// #filter:frei0r_3dflippo
//
// Name   : 3dflippo
// Author : c.e. prelz AS FLUIDO <fluido@fluido.as>
// Version: 1.0.1
//
// Frame rotation in 3d-space
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_addition_alpha
//
// Name   : addition_alpha
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform an RGB[A] addition_alpha operation of the pixel sources.
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_addition
//
// Name   : addition
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform an RGB[A] addition operation of the pixel sources.
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_alpha0ps
//
// Name   : alpha0ps
// Author : Marko Cebokli
// Version: 1.0.4
//
// Display and manipulation of the alpha channel
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_alphaatop
//
// Name   : alphaatop
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// the alpha ATOP operation
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_alphagrad
//
// Name   : alphagrad
// Author : Marko Cebokli
// Version: 1.0.2
//
// Fills alpha channel with a gradient
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_alphainjection
//
// Name   : Alpha Injection
// Author : Richard Spindler
// Version: 1.0.9
//
// Averages Input 1 and uses this as Alpha Channel on Input 2
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_alphain
//
// Name   : alphain
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// the alpha IN operation
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_alphaout
//
// Name   : alphaout
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// the alpha OUT operation
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_alphaover
//
// Name   : alphaover
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// the alpha OVER operation
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_alphaspot
//
// Name   : alphaspot
// Author : Marko Cebokli
// Version: 1.0.1
//
// Draws simple shapes into the alpha channel
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_alphaxor
//
// Name   : alphaxor
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// the alpha XOR operation
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_balanc0r
//
// Name   : White Balance
// Author : Dan Dennedy
// Version: 1.0.3
//
// Adjust the white balance / color temperature
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_baltan
//
// Name   : Baltan
// Author : Kentaro, Jaromil
// Version: 1.3.1
//
// delayed alpha smoothed blit of time
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_bgsubtract0r
//
// Name   : BgSubtract0r
// Author : Samuel Mimram
// Version: 1.0.2
//
// Bluescreen the background of a static video.
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_blend
//
// Name   : blend
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform a blend operation between two sources
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_bluescreen0r
//
// Name   : bluescreen0r
// Author : Hedde Bosman
// Version: 1.0.3
//
// Color to alpha (blit SRCALPHA)
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_brightness
//
// Name   : Brightness
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Adjusts the brightness of a source image
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_B
//
// Name   : B
// Author : Richard Spindler
// Version: 1.0.9
//
// Extracts Blue from Image
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_burn
//
// Name   : burn
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform an RGB[A] dodge operation between the pixel sources, using the generalised algorithm:
// D = saturation of 255 or depletion of 0, of ((255 - A) * 256) / (B + 1)
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_bw0r
//
// Name   : bw0r
// Author : coma@gephex.org
// Version: 1.0.9
//
// Turns image black/white.
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_c0rners
//
// Name   : c0rners
// Author : Marko Cebokli
// Version: 1.0.2
//
// Four corners geometry engine
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_cairoaffineblend
//
// Name   : cairoaffineblend
// Author : Janne Liljeblad
// Version: 1.0.9
//
// Composites second input on first input applying user-defined transformation, opacity and blend mode
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_cairoblend
//
// Name   : cairoblend
// Author : Janne Liljeblad
// Version: 1.0.9
//
// Composites second input on the first input with user-defined blend mode and opacity.
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_cairogradient
//
// Name   : cairogradient
// Author : Janne Liljeblad
// Version: 1.0.9
//
// Draws a gradient on top of image. Filter is given gradient start and end points, colors and opacities.
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_cairoimagegrid
//
// Name   : cairoimagegrid
// Author : Janne Liljeblad
// Version: 1.0.9
//
// Draws a grid of input images.
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_cartoon
//
// Name   : Cartoon
// Author : Dries Pruimboom, Jaromil
// Version: 1.2.2
//
// Cartoonify video, do a form of edge detect
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_cluster
//
// Name   : K-Means Clustering
// Author : binarymillenium
// Version: 1.0.1
//
// Clusters of a source image by color and spatial distance
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_colgate
//
// Name   : White Balance (LMS space)
// Author : Steinar H. Gunderson
// Version: 1.0.1
//
// Do simple color correction, in a physically meaningful way
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_coloradj_RGB
//
// Name   : coloradj_RGB
// Author : Marko Cebokli
// Version: 1.0.2
//
// Simple color adjustment
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_colordistance
//
// Name   : Color Distance
// Author : Richard Spindler
// Version: 1.0.2
//
// Calculates the distance between the selected color and the current pixel and uses that value as new pixel value
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_colorhalftone
//
// Name   : colorhalftone
// Author : Janne Liljeblad
// Version: 1.0.9
//
// Filters image to resemble a halftone print in which tones are represented as variable sized dots
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_colorize
//
// Name   : colorize
// Author : Janne Liljeblad
// Version: 1.0.1
//
// Colorizes image to selected hue, saturation and lightness
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_color_only
//
// Name   : color_only
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform a conversion to color only of the source input1 using the hue and saturation values of input2.
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_colortap
//
// Name   : colortap
// Author : Janne Liljeblad
// Version: 1.0.9
//
// Applies a pre-made color effect to image
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_composition
//
// Name   : Composition
// Author : Richard Spindler
// Version: 1.0.9
//
// Composites Image 2 onto Image 1 according to its Alpha Channel
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_contrast0r
//
// Name   : Contrast0r
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Adjusts the contrast of a source image
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_curves
//
// Name   : Curves
// Author : Maksim Golovkin, Till Theato
// Version: 1.0.4
//
// Adjust luminance or color channel intensity with curve level mapping
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_d90stairsteppingfix
//
// Name   : Nikon D90 Stairstepping fix
// Author : Simon A. Eugster (Granjow)
// Version: 1.0.2
//
// Removes the Stairstepping from Nikon D90 videos (720p only) by interpolation
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_darken
//
// Name   : darken
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform a darken operation between two sources (minimum value of both sources).
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_defish0r
//
// Name   : Defish0r
// Author : Marko Cebokli
// Version: 1.0.3
//
// Non rectilinear lens mappings
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_delay0r
//
// Name   : delay0r
// Author : Martin Bayer
// Version: 1.0.2
//
// video delay
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_delaygrab
//
// Name   : Delaygrab
// Author : Bill Spinhover, Andreas Schiffler, Jaromil
// Version: 1.3.1
//
// delayed frame blitting mapped on a time bitmap
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_difference
//
// Name   : difference
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform an RGB[A] difference operation between the pixel sources.
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_distort0r
//
// Name   : Distort0r
// Author : Gephex crew
// Version: 1.0.10
//
// Plasma
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_dither
//
// Name   : dither
// Author : Janne Liljeblad
// Version: 1.0.1
//
// Dithers the image and reduces the number of available colors
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_divide
//
// Name   : divide
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform an RGB[A] divide operation between the pixel sources: input1 is the numerator, input2 the denominator
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_dodge
//
// Name   : dodge
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform an RGB[A] dodge operation between the pixel sources, using the generalised algorithm:
// D = saturation of 255 or (A * 256) / (256 - B)
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_edgeglow
//
// Name   : Edgeglow
// Author : Salsaman
// Version: 1.0.2
//
// Edgeglow filter
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_emboss
//
// Name   : emboss
// Author : Janne Liljeblad
// Version: 1.0.1
//
// Creates embossed relief image of source image
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_equaliz0r
//
// Name   : Equaliz0r
// Author : Jean-Sebastien Senecal (Drone)
// Version: 1.0.2
//
// Equalizes the intensity histograms
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_facebl0r
//
// Name   : FaceBl0r
// Author : ZioKernel, Biilly, Jilt, Jaromil, ddennedy
// Version: 1.1.1
//
// automatic face blur
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_facedetect
//
// Name   : opencvfacedetect
// Author : binarymillenium, ddennedy
// Version: 1.2.0
//
// detect faces and draw shapes on them
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_flippo
//
// Name   : Flippo
// Author : Carlo Emilio, Jean-Sebastien Senecal
// Version: 1.0.1
//
// Flipping in x and y axis
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_gamma
//
// Name   : Gamma
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Adjusts the gamma value of a source image
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_glitch0r
//
// Name   : Glitch0r
// Author : IDENT Software
// Version: 1.0.1
//
// Adds glitches and block shifting
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_glow
//
// Name   : Glow
// Author : Richard Spindler
// Version: 1.0.1
//
// Creates a Glamorous Glow
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_grain_extract
//
// Name   : grain_extract
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform an RGB[A] grain-extract operation between the pixel sources.
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_grain_merge
//
// Name   : grain_merge
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform an RGB[A] grain-merge operation between the pixel sources.
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_G
//
// Name   : G
// Author : Richard Spindler
// Version: 1.0.9
//
// Extracts Green from Image
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_hardlight
//
// Name   : hardlight
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform an RGB[A] hardlight operation between the pixel sources
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_hqdn3d
//
// Name   : hqdn3d
// Author : Marko Cebokli, Daniel Moreno
// Version: 1.0.1
//
// High quality 3D denoiser from Mplayer
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_hueshift0r
//
// Name   : Hueshift0r
// Author : Jean-Sebastien Senecal
// Version: 1.0.3
//
// Shifts the hue of a source image
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_hue
//
// Name   : hue
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform a conversion to hue only of the source input1 using the hue of input2.
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_IIRblur
//
// Name   : IIR blur
// Author : Marko Cebokli
// Version: 1.0.1
//
// Three types of fast IIR blurring
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_invert0r
//
// Name   : Invert0r
// Author : Gephex crew
// Version: 1.0.9
//
// Inverts all colors of a source image
//
// Generates images as r8g8b8a8
//
// #input:frei0r_ising0r:
//
// Name   : Ising0r
// Author : Gephex crew
// Version: 1.0.9
//
// Generates ising noise
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_keyspillm0pup
//
// Name   : keyspillm0pup
// Author : Marko Cebokli
// Version: 1.0.3
//
// Reduces the visibility of key color spill in chroma keying
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_lenscorrection
//
// Name   : Lens Correction
// Author : Richard Spindler
// Version: 1.0.2
//
// Allows compensation of lens distortion
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_letterb0xed
//
// Name   : LetterB0xed
// Author : Richard Spindler
// Version: 1.0.1
//
// Adds Black Borders at top and bottom for Cinema Look
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_levels
//
// Name   : Levels
// Author : Maksim Golovkin
// Version: 1.0.3
//
// Adjust luminance or color channel intensity
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_lighten
//
// Name   : lighten
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform a lighten operation between two sources (maximum value of both sources).
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_lightgraffiti
//
// Name   : Light Graffiti
// Author : Simon A. Eugster (Granjow)
// Version: 1.0.3
//
// Creates light graffitis from a video by keeping the brightest spots.
//
// Generates images as r8g8b8a8
//
// #input:frei0r_lissajous0r:
//
// Name   : Lissajous0r
// Author : Martin Bayer
// Version: 1.0.3
//
// Generates Lissajous0r images
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_luminance
//
// Name   : Luminance
// Author : Richard Spindler
// Version: 1.0.2
//
// Creates a luminance map of the image
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_mask0mate
//
// Name   : Mask0Mate
// Author : Richard Spindler
// Version: 1.0.1
//
// Creates an square alpha-channel mask
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_medians
//
// Name   : Medians
// Author : Marko Cebokli
// Version: 1.0.1
//
// Implements several median-type filters
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_multiply
//
// Name   : multiply
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform an RGB[A] multiply operation between the pixel sources.
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_ndvi
//
// Name   : NDVI filter
// Author : Brian Matherly
// Version: 1.0.2
//
// This filter creates a false image from a visible + infrared source.
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_nervous
//
// Name   : Nervous
// Author : Tannenbaum, Kentaro, Jaromil
// Version: 1.3.1
//
// flushes frames in time in a nervous way
//
// Generates images as b8g8r8a8
//
// #input:frei0r_nois0r:
//
// Name   : Nois0r
// Author : Martin Bayer
// Version: 1.0.3
//
// Generates white noise images
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_nosync0r
//
// Name   : nosync0r
// Author : Martin Bayer
// Version: 1.0.2
//
// broken tv
//
// Generates images as b8g8r8a8
//
// #input:frei0r_onecol0r:
//
// Name   : onecol0r
// Author : Martin Bayer
// Version: 1.0.3
//
// image with just one color
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_overlay
//
// Name   : overlay
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform an RGB[A] overlay operation between the pixel sources, using the generalised algorithm:
// D =  A * (B + (2 * B) * (255 - A))
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #input:frei0r_partik0l:
//
// Name   : Partik0l
// Author : Jaromil
// Version: 1.0.3
//
// Particles generated on prime number sinusoidal blossoming
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_perspective
//
// Name   : Perspective
// Author : Richard Spindler
// Version: 1.0.1
//
// Distorts the image for a pseudo perspective
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_pixeliz0r
//
// Name   : pixeliz0r
// Author : Gephex crew
// Version: 1.1.0
//
// Pixelize input image.
//
// Generates images as b8g8r8a8
//
// #input:frei0r_plasma:
//
// Name   : Plasma
// Author : Jaromil
// Version: 1.0.3
//
// Demo scene 8bit plasma
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_posterize
//
// Name   : posterize
// Author : Janne Liljeblad
// Version: 1.0.1
//
// Posterizes image by reducing the number of colors used in image
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_pr0be
//
// Name   : pr0be
// Author : Marko Cebokli
// Version: 1.0.1
//
// Measure video values
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_pr0file
//
// Name   : pr0file
// Author : Marko Cebokli
// Version: 1.0.2
//
// 2D video oscilloscope
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_primaries
//
// Name   : primaries
// Author : Hedde Bosman
// Version: 1.0.2
//
// Reduce image to primary colors
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_rgbnoise
//
// Name   : rgbnoise
// Author : Janne Liljeblad
// Version: 1.0.9
//
// Adds RGB noise to image.
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_rgbparade
//
// Name   : RGB-Parade
// Author : Albert Frisch
// Version: 1.0.2
//
// Displays a histogram of R, G and B of the video-data
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_RGB
//
// Name   : RGB
// Author : Richard Spindler
// Version: 1.0.9
//
// Averages each Input and uses each as R, G or B channel of the Output
//
// Generates images as r8g8b8a8
//
// Requires 3 connected slots.
//
// #filter:frei0r_rgbsplit0r
//
// Name   : rgbsplit0r
// Author : IDENT Software
// Version: 1.0.1
//
// RGB splitting and shifting
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_R
//
// Name   : R
// Author : Richard Spindler
// Version: 1.0.9
//
// Extracts Red from Image
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_saturat0r
//
// Name   : Saturat0r
// Author : Jean-Sebastien Senecal
// Version: 1.0.3
//
// Adjusts the saturation of a source image
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_saturation
//
// Name   : saturation
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform a conversion to saturation only of the source input1 using the saturation level of input2.
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_scale0tilt
//
// Name   : Scale0Tilt
// Author : Richard Spindler
// Version: 1.0.1
//
// Scales, Tilts and Crops an Image
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_scanline0r
//
// Name   : scanline0r
// Author : Martin Bayer
// Version: 1.0.2
//
// interlaced blak lines
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_screen
//
// Name   : screen
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform an RGB[A] screen operation between the pixel sources, using the generalised algorithm:
// D = 255 - (255 - A) * (255 - B)
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_select0r
//
// Name   : select0r
// Author : Marko Cebokli
// Version: 1.0.5
//
// Color based alpha selection
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_sharpness
//
// Name   : Sharpness
// Author : Marko Cebokli, Remi Guyomarch
// Version: 1.0.2
//
// Unsharp masking (port from Mplayer)
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_sigmoidaltransfer
//
// Name   : sigmoidaltransfer
// Author : Janne Liljeblad
// Version: 1.0.9
//
// Desaturates image and creates a particular look that could be called Stamp, Newspaper or Photocopy
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_sobel
//
// Name   : Sobel
// Author : Jean-Sebastien Senecal (Drone)
// Version: 1.0.2
//
// Sobel filter
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_softglow
//
// Name   : softglow
// Author : Janne Liljeblad
// Version: 1.0.9
//
// Does softglow effect on highlights
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_softlight
//
// Name   : softlight
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform an RGB[A] softlight operation between the pixel sources.
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_sopsat
//
// Name   : SOP/Sat
// Author : Simon A. Eugster (Granjow)
// Version: 1.0.3
//
// Slope/Offset/Power and Saturation color corrections according to the ASC CDL (Color Decision List)
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_spillsupress
//
// Name   : spillsupress
// Author : Janne Liljeblad
// Version: 1.0.1
//
// Remove green or blue spill light from subjects shot in front of green or blue screen
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_squareblur
//
// Name   : Squareblur
// Author : Drone
// Version: 1.0.1
//
// Variable-size square blur
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_subtract
//
// Name   : subtract
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform an RGB[A] subtract operation of the pixel source input2 from input1.
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_tehroxx0r
//
// Name   : TehRoxx0r
// Author : Coma
// Version: 1.0.9
//
// Something videowall-ish
//
// Generates images as b8g8r8a8
//
// #input:frei0r_test_pat_B:
//
// Name   : test_pat_B
// Author : Marko Cebokli
// Version: 1.0.1
//
// Generates test card lookalikes
//
// Generates images as r8g8b8a8
//
// #input:frei0r_test_pat_C:
//
// Name   : test_pat_C
// Author : Marko Cebokli
// Version: 1.0.1
//
// Generates cross sections of color spaces
//
// Generates images as r8g8b8a8
//
// #input:frei0r_test_pat_G:
//
// Name   : test_pat_G
// Author : Marko Cebokli
// Version: 1.0.2
//
// Generates geometry test pattern images
//
// Generates images as r8g8b8a8
//
// #input:frei0r_test_pat_I:
//
// Name   : test_pat_I
// Author : Marko Cebokli
// Version: 1.0.2
//
// Generates spatial impulse and step test patterns
//
// Generates images as r8g8b8a8
//
// #input:frei0r_test_pat_L:
//
// Name   : test_pat_L
// Author : Marko Cebokli
// Version: 1.0.1
//
// Generates linearity checking patterns
//
// Generates images as r8g8b8a8
//
// #input:frei0r_test_pat_R:
//
// Name   : test_pat_R
// Author : Marko Cebokli
// Version: 1.0.2
//
// Generates resolution test patterns
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_threelay0r
//
// Name   : threelay0r
// Author : Hedde Bosman
// Version: 1.0.2
//
// dynamic 3 level thresholding
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_three_point_balance
//
// Name   : 3 point color balance
// Author : Maksim Golovkin
// Version: 1.0.1
//
// Adjust color balance with 3 color points
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_threshold0r
//
// Name   : Threshold0r
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Thresholds a source image
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_timeout
//
// Name   : Timeout indicator
// Author : Simon A. Eugster
// Version: 1.0.2
//
// Timeout indicators e.g. for slides.
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_tint0r
//
// Name   : Tint0r
// Author : Maksim Golovkin
// Version: 1.0.1
//
// Tint a source image with specified color
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_transparency
//
// Name   : Transparency
// Author : Richard Spindler
// Version: 1.0.9
//
// Tunes the alpha channel.
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_twolay0r
//
// Name   : Twolay0r
// Author : Martin Bayer
// Version: 1.0.2
//
// dynamic thresholding
//
// Generates images as b8g8r8a8
//
// #filter:frei0r_uvmap
//
// Name   : UV Map
// Author : Richard Spindler
// Version: 1.0.9
//
// Uses Input 1 as UV Map to distort Input 2
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_value
//
// Name   : value
// Author : Jean-Sebastien Senecal
// Version: 1.0.2
//
// Perform a conversion to value only of the source input1 using the value of input2.
//
// Generates images as r8g8b8a8
//
// Requires 2 connected slots.
//
// #filter:frei0r_vectorscope
//
// Name   : Vectorscope
// Author : Albert Frisch
// Version: 1.0.2
//
// Displays the vectorscope of the video-data
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_vertigo
//
// Name   : Vertigo
// Author : Fukuchi Kentarou
// Version: 1.1.1
//
// alpha blending with zoomed and rotated images
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_vignette
//
// Name   : Vignette
// Author : Simon A. Eugster (Granjow)
// Version: 1.0.2
//
// Lens vignetting effect, applies natural vignetting
//
// Generates images as r8g8b8a8
//
// #filter:frei0r_xfade0r
//
// Name   : xfade0r
// Author : Martin Bayer
// Version: 1.0.2
//
// a simple xfader
//
// Generates images as b8g8r8a8
//
// Requires 2 connected slots.
//
