// frei0r - A frei0r plugin to ml.
//
// Copyright (C) 2009 Charles Yates
// Released under the LGPL.

extern "C" {
#include <frei0r.h>
}

#include <openmedialib/ml/openmedialib_plugin.hpp>
#include <openpluginlib/pl/pcos/observer.hpp>

#include <boost/algorithm/string.hpp>

#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
#include <sstream>

namespace cl = olib::opencorelib;
namespace cls = olib::opencorelib::str_util;
namespace pl = olib::openpluginlib;
namespace ml = olib::openmedialib::ml;
namespace pcos = olib::openpluginlib::pcos;

namespace olib { namespace openmedialib { namespace ml { namespace frei0r {

// pcos property observer
template < typename C > class fn_observer : public pcos::observer
{
	public:
		fn_observer( C *instance, void ( C::*fn )( pcos::isubject * ) )
			: instance_( instance )
			, fn_( fn )
		{ }

		virtual void updated( pcos::isubject *subject )
		{ ( instance_->*fn_ )( subject ); }

	private:
		C *instance_;
		void ( C::*fn_ )( pcos::isubject * );
};

// Wrapper for the requested plugin
class plugin_wrapper
{
	public:
		typedef int ( *f0r_init )();
		typedef void ( *f0r_deinit )();
		typedef void ( *f0r_get_plugin_info )(f0r_plugin_info_t* info);
		typedef void ( *f0r_get_param_info )(f0r_param_info_t* info, int param_index);
		typedef f0r_instance_t ( *f0r_construct )(unsigned int width, unsigned int height);
		typedef void ( *f0r_destruct )(f0r_instance_t instance);
		typedef void ( *f0r_set_param_value )(f0r_instance_t instance, f0r_param_t param, int param_index);
		typedef void ( *f0r_get_param_value )(f0r_instance_t instance, f0r_param_t param, int param_index);
		typedef void ( *f0r_update )(f0r_instance_t instance, double time, const uint32_t* inframe, uint32_t* outframe);
		typedef void ( *f0r_update2 )(f0r_instance_t instance, double time, const uint32_t* inframe1, const uint32_t* inframe2, const uint32_t* inframe3, uint32_t* outframe);

		plugin_wrapper( )
		: addr_( 0 )
		, complete_( false )
		{ }

		~plugin_wrapper( )
		{
			if ( complete_ ) deinit_( );
			if ( addr_ ) dlclose( addr_ );
		}

		bool load( const std::string &plugin )
		{
			// Open the plugin
			addr_ = dlopen( plugin.c_str( ), RTLD_NOW );

			// Resolve symbols
			if ( addr_ )
			{
				init_ = ( f0r_init )dlsym( addr_, "f0r_init" );
				deinit_ = ( f0r_deinit )dlsym( addr_, "f0r_deinit" );
				get_plugin_info = ( f0r_get_plugin_info )dlsym( addr_, "f0r_get_plugin_info" );
				get_param_info = ( f0r_get_param_info )dlsym( addr_, "f0r_get_param_info" );
				construct = ( f0r_construct )dlsym( addr_, "f0r_construct" );
				destruct = ( f0r_destruct )dlsym( addr_, "f0r_destruct" );
				set_param_value = ( f0r_set_param_value )dlsym( addr_, "f0r_set_param_value" );
				get_param_value = ( f0r_get_param_value )dlsym( addr_, "f0r_get_param_value" );
				update = ( f0r_update )dlsym( addr_, "f0r_update" );
				update2 = ( f0r_update2 )dlsym( addr_, "f0r_update2" );
				complete_ = init_ && deinit_ && get_plugin_info && get_param_info && construct && destruct && set_param_value && get_param_value && ( update || update2 );
				if ( !complete_ )
					std::cerr << "failed to resolve " << plugin << std::endl;
			}
			else
			{
				std::cerr << "failed to load " << plugin << std::endl;
			}

			// Initialise the plugin
			if ( complete_ )
			{
				complete_ = init_( );
				if ( !complete_ )
					std::cerr << "failed to init " << plugin << std::endl;
			}

			return complete_;
		}

		const bool complete( ) const
		{ return complete_; }

		f0r_get_plugin_info get_plugin_info;
		f0r_get_param_info get_param_info;
		f0r_construct construct;
		f0r_destruct destruct;
		f0r_set_param_value set_param_value;
		f0r_get_param_value get_param_value;
		f0r_update update;
		f0r_update2 update2;

	private:
		void *addr_;
		bool complete_;
		f0r_init init_;
		f0r_deinit deinit_;
};

static std::string basename( const std::wstring &spec )
{
	std::string base = cls::to_string( spec );

	// Chop out all the cruft first
	if ( base.find( "frei0r_doc_" ) == 0 )
		base = base.substr( 11 );

	if ( base.find( "frei" ) == 0 )
		base = base.substr( 7 );

	if ( base.find( ":" ) != std::string::npos )
		base = base.substr( 0, base.size( ) - 1 );

	return base;
}

// Hunt the path for a filter which matches the spec
static std::string find( const std::wstring &spec )
{
	std::string result = "";
	std::string base = basename( spec );

	// Set up the search path
	std::vector< std::string > dirs;
	dirs.push_back( std::string( getenv( "HOME" ) ) + "/.frei0r-1/lib/" );
	dirs.push_back( "/usr/local/lib/frei0r-1/" );
	dirs.push_back( "/usr/lib/frei0r-1/" );

	// Search the path
	for( std::vector< std::string >::iterator iter = dirs.begin( ); iter != dirs.end( ); iter ++ )
	{
		std::string temp = *iter + base + ".so";
		if ( boost::filesystem::exists( temp ) )
		{
			result = temp;
			break;
		}
	}

	if ( result == "" )
		std::cerr << "unable to find a plugin for " << base << std::endl;

	return result;
}

// Map plugin type to slot count
static int slots( f0r_plugin_info_t &info )
{
	int result = -1;

	switch( info.plugin_type )
	{
		case F0R_PLUGIN_TYPE_SOURCE:
			result = 0;
			break;

		case F0R_PLUGIN_TYPE_FILTER:
			result = 1;
			break;

		case F0R_PLUGIN_TYPE_MIXER2:
			result = 2;
			break;

		case F0R_PLUGIN_TYPE_MIXER3:
			result = 3;
			break;
	}

	return result;
}

// Map colour_model to aml picture format
static std::wstring pf( f0r_plugin_info_t &info )
{
	std::wstring result = std::wstring( L"" );

	switch( info.color_model )
	{
		case F0R_COLOR_MODEL_BGRA8888:
			result = std::wstring( L"b8g8r8a8" );
			break;

		case F0R_COLOR_MODEL_RGBA8888:
			result = std::wstring( L"r8g8b8a8" );
			break;

		case F0R_COLOR_MODEL_PACKED32:
			// TODO: allow options for this
			result = std::wstring( L"b8g8r8a8" );
			break;
	}

	return result;
}

// Map plugin params to aml properties
static bool convert( plugin_wrapper &wrapper, f0r_instance_t instance, std::vector< pl::pcos::property > &props, int num_params )
{
	bool result = true;

	for ( int i = 0; i < num_params; i ++ )
	{
		f0r_param_info param;
		wrapper.get_param_info( &param, i );

		std::string prop_name( param.name );
		prop_name = "f_" + prop_name;

		// Yicky naming used in some of these params..
		boost::to_lower( prop_name );
		boost::replace_all( prop_name, " ", "_" );
		boost::replace_all( prop_name, "(", "" );
		boost::replace_all( prop_name, ")", "" );
		boost::replace_all( prop_name, "'", "" );

		switch( param.type )
		{
			case F0R_PARAM_BOOL:
				{
					f0r_param_bool value;
					wrapper.get_param_value( instance, ( f0r_param_t )&value, i );
					pl::pcos::property prop( pl::pcos::key::from_string( prop_name.c_str( ) ) );
					props.push_back( prop = double( value ) );
				}
				break;

			case F0R_PARAM_DOUBLE:
				{
					f0r_param_double value;
					wrapper.get_param_value( instance, ( f0r_param_t )&value, i );
					pl::pcos::property prop( pl::pcos::key::from_string( prop_name.c_str( ) ) );
					props.push_back( prop = double( value ) );
				}
				break;

			case F0R_PARAM_COLOR:
				{
					f0r_param_color_t value;
					wrapper.get_param_value( instance, ( f0r_param_t )&value, i );
					std::ostringstream stream;
					stream << value.r << " " << value.g << " " << value.b;
					pl::pcos::property prop( pl::pcos::key::from_string( prop_name.c_str( ) ) );
					props.push_back( prop = cls::to_wstring( stream.str( ) ) );
				}
				break;

			case F0R_PARAM_POSITION:
				{
					f0r_param_position_t value;
					wrapper.get_param_value( instance, ( f0r_param_t )&value, i );
					std::ostringstream stream;
					stream << value.x << " " << value.y;
					pl::pcos::property prop( pl::pcos::key::from_string( prop_name.c_str( ) ) );
					props.push_back( prop = cls::to_wstring( stream.str( ) ) );
				}
				break;

			case F0R_PARAM_STRING:
				{
					f0r_param_string value;
					wrapper.get_param_value( instance, ( f0r_param_t )&value, i );
					pl::pcos::property prop( pl::pcos::key::from_string( prop_name.c_str( ) ) );
					std::string temp( value );
					props.push_back( prop = cls::to_wstring( temp ) );
				}
				break;
		}
	}

	return result;
}

static bool refresh( plugin_wrapper &wrapper, f0r_instance_t instance, std::vector< pl::pcos::property > &props )
{
	bool result = true;

	for ( int i = 0; i < int( props.size( ) ); i ++ )
	{
		f0r_param_info param;
		wrapper.get_param_info( &param, i );
		switch( param.type )
		{
			case F0R_PARAM_BOOL:
				{
					f0r_param_bool value = f0r_param_bool( props[ i ].value< double >( ) );
					wrapper.set_param_value( instance, ( f0r_param_t )&value, i );
				}
				break;

			case F0R_PARAM_DOUBLE:
				{
					f0r_param_double value = props[ i ].value< double >( );
					wrapper.set_param_value( instance, ( f0r_param_t )&value, i );
				}
				break;

			case F0R_PARAM_COLOR:
				{
					f0r_param_color value;
					std::istringstream stream( cls::to_string( props[ i ].value< std::wstring >( ) ) );
					stream >> value.r >> value.g >> value.b;
					wrapper.set_param_value( instance, ( f0r_param_t )&value, i );
				}
				break;

			case F0R_PARAM_POSITION:
				{
					f0r_param_position value;
					std::istringstream stream;
					stream.str( cls::to_string( props[ i ].value< std::wstring >( ) ) );
					stream >> value.x >> value.y;
					wrapper.set_param_value( instance, ( f0r_param_t )&value, i );
				}
				break;

			case F0R_PARAM_STRING:
				{
					std::string str( cls::to_string( props[ i ].value< std::wstring >( ) ) );
					wrapper.set_param_value( instance, ( f0r_param_t )str.c_str( ), i );
				}
				break;
		}
	}

	return result;
}

static void document( const std::wstring &spec, plugin_wrapper &wrapper, f0r_plugin_info_t &info, const pl::pcos::property_container &properties )
{
	if ( wrapper.complete( ) && slots( info ) != -1 )
	{
		std::string base = basename( spec );

		switch( slots( info ) )
		{
			case 0:
				std::cout << "// #input:frei0r_" << base << ":" << std::endl;
				break;

			case 1:
			case 2:
			case 3:
				std::cout << "// #filter:frei0r_" << base << std::endl;
				break;
		}

		std::string detail( info.explanation );
		boost::replace_all( detail, "\n", "\n// " );

		std::cout << "//" << std::endl;
		std::cout << "// Name   : " << info.name << std::endl;
		std::cout << "// Author : " << info.author << std::endl;
		std::cout << "// Version: " << info.frei0r_version << "." << info.major_version << "." << info.minor_version << std::endl;
		std::cout << "//" << std::endl;
		std::cout << "// " << detail << std::endl;
		std::cout << "//" << std::endl;
		std::cout << "// Generates images as " << cls::to_string( pf( info ) ) << std::endl;
		std::cout << "//" << std::endl;

		if ( slots( info ) > 1 )
		{
			std::cout << "// Requires " << slots( info ) << " connected slots." << std::endl;
			std::cout << "//" << std::endl;
		}
	}

}

#define const_frei0r_input const_cast< frei0r_input * >

// Input wrapper for source types
class ML_PLUGIN_DECLSPEC frei0r_input : public input_type
{
	public:
		typedef fn_observer< frei0r_input > observer;

		// Constructor and destructor
		frei0r_input( const std::wstring &spec ) 
			: input_type( ) 
			, spec_( spec )
			, pf_( std::wstring( L"b8g8r8a8" ) )
			, instance_( 0 )
			, width_( 0 )
			, height_( 0 )
			, prop_type_( pl::pcos::key::from_string( "frei0r_type" ) )
			, prop_out_( pcos::key::from_string( "out" ) )
			, prop_fps_num_( pcos::key::from_string( "fps_num" ) )
			, prop_fps_den_( pcos::key::from_string( "fps_den" ) )
			, prop_width_( pcos::key::from_string( "width" ) )
			, prop_height_( pcos::key::from_string( "height" ) )
			, prop_sar_num_( pcos::key::from_string( "sar_num" ) )
			, prop_sar_den_( pcos::key::from_string( "sar_den" ) )
			, changed_( false )
			, obs_prop_( new observer( const_frei0r_input( this ), &frei0r_input::updated ) )
		{ 
			properties( ).append( prop_type_ = 1 );
			std::string path = find( spec );

			if ( path != "" && wrapper_.load( path ) )
			{
				f0r_plugin_info_t info;
				wrapper_.get_plugin_info( &info );

				// Determine the type of the plugin
				prop_type_ = slots( info );

				if ( prop_type_.value< int >( ) == 0 || spec.find( L"frei0r_doc_" ) == 0 )
				{
					// Only provide profile props for generators
					if ( prop_type_.value< int >( ) == 0 )
					{
						properties( ).append( prop_out_ = 250 );
						properties( ).append( prop_fps_num_ = 25 );
						properties( ).append( prop_fps_den_ = 1 );
						properties( ).append( prop_width_ = 720 );
						properties( ).append( prop_height_ = 576 );
						properties( ).append( prop_sar_num_ = 59 );
						properties( ).append( prop_sar_den_ = 54 );
					}
					else
					{
						prop_out_ = 0;
					}

					// Determine the picture format of this generator
					pf_ = pf( info );

					// Create a dummy instance in order to obtain params
					instance( 720, 576 );

					// Map plugin values to aml properties
					convert( wrapper_, instance_, props_, info.num_params );

					// Expose the properties
					for ( std::vector< pl::pcos::property >::iterator iter = props_.begin( ); iter != props_.end( ); iter ++ )
					{
						properties( ).append( *iter );
						( *iter ).set_always_notify( true );
						( *iter ).attach( obs_prop_ );
					}

					// Provide doc generation of loaded plugin
					if ( spec.find( L"frei0r_doc_" ) == 0 )
					{
						document( spec_, wrapper_, info, properties( ) );
						prop_out_ = 0;
					}
				}
				else
				{
					prop_out_ = 0;
				}
			}
			else
			{
				prop_out_ = 0;
			}
		}

		virtual ~frei0r_input( ) 
		{ 
			if ( instance_ )
				wrapper_.destruct( instance_ );
		}

		// Indicates if the input will enforce a packet decode
		virtual bool requires_image( ) const { return true; }

		// Basic information
		virtual std::wstring get_uri( ) const { return spec_; }

		// Audio/Visual
		virtual int get_frames( ) const { return prop_out_.value< int >( ); }
		virtual bool is_seekable( ) const { return true; }

		// Visual
		virtual void get_fps( int &num, int &den ) const { num = prop_fps_num_.value< int >( ); den = prop_fps_den_.value< int >( ); }
		virtual void get_sar( int &num, int &den ) const { num = prop_sar_num_.value< int >( ); den = prop_sar_den_.value< int >( ); }
		virtual int get_video_streams( ) const { return 1; }
		virtual int get_width( ) const { return prop_width_.value< int >( ); }
		virtual int get_height( ) const { return prop_height_.value< int >( ); }

		// Audio
		virtual int get_audio_streams( ) const { return 0; }

		virtual bool determine_complete( ) const { return true; }

		void updated( pcos::isubject * ) { changed_ = true; }

	protected:
		// Fetch method
		ml::fetch_status do_fetch( frame_type_ptr &result )
		{
			// Construct a frame and populate with basic information
			result = frame_type_ptr( new frame_type( ) );

			int num, den;

			get_sar( num, den );
			result->set_sar( num, den );

			get_fps( num, den );
			result->set_fps( num, den );

			result->set_pts( get_position( ) * float( den ) / float( num ) );
			result->set_duration( float( den ) / float( num ) );
			result->set_position( get_position( ) );

			// Generate an image
			int width = get_width( );
			int height = get_height( );

			// Make sure we have a correct instance to work with
			instance( width, height );

			// Pass properties to frei0r instance
			if ( changed_ )
			{
				refresh( wrapper_, instance_, props_ );
				changed_ = false;
			}

			// Allocate an image
			ml::image_type_ptr image = ml::image::allocate( cls::to_t_string( pf_ ), width, height );

			// Call the frieor update
			wrapper_.update( instance_, result->get_duration( ) * get_position( ), ( const boost::uint32_t * )0, ( boost::uint32_t * )image->ptr( 0 ) );

			// Place image on the result
			result->set_image( boost::move( image ) );

			return ml::fetch_ok;
		}

		f0r_instance_t instance( int width, int height )
		{
			// Check for dimension changes
			if ( instance_ && width != width_ && height != height_ )
			{
				wrapper_.destruct( instance_ );
				instance_ = 0;
			}

			// Construct our instance if we don't have one
			if ( !instance_ )
			{
				instance_ = wrapper_.construct( width, height );
				width_ = width;
				height_ = height;
			}

			return instance_;
		}

	private:
		std::wstring spec_;
		plugin_wrapper wrapper_;
		std::wstring pf_;
		f0r_instance_t instance_;
		int width_, height_;
		pcos::property prop_type_;
		pcos::property prop_out_;
		pcos::property prop_fps_num_;
		pcos::property prop_fps_den_;
		pcos::property prop_width_;
		pcos::property prop_height_;
		pcos::property prop_sar_num_;
		pcos::property prop_sar_den_;
		std::vector < pl::pcos::property > props_;
		bool changed_;
		std::shared_ptr< pcos::observer > obs_prop_;
};

#define const_frei0r_filter const_cast< frei0r_filter * >

class ML_PLUGIN_DECLSPEC frei0r_filter : public filter_type
{
	public:
		typedef fn_observer< frei0r_filter > observer;

		frei0r_filter( const std::wstring &spec )
		: spec_( spec )
		, pf_( std::wstring( L"b8g8r8a8" ) )
		, instance_( 0 )
		, width_( 0 )
		, height_( 0 )
		, prop_enable_( pl::pcos::key::from_string( "enable" ) )
		, prop_slots_( pl::pcos::key::from_string( "frei0r_type" ) )
		, prop_distributable_( pl::pcos::key::from_string( "distributable" ) )
		, changed_( false )
		, obs_prop_( new observer( const_frei0r_filter( this ), &frei0r_filter::updated ) )
		{
			properties( ).append( prop_enable_ = 1 );
			properties( ).append( prop_slots_ = 1 );
			properties( ).append( prop_distributable_ = 0 );

			std::string path = find( spec );

			if ( path != "" && wrapper_.load( path ) )
			{
				wrapper_.get_plugin_info( &info_ );

				if ( slots( info_ ) >= 1 )
				{
					prop_slots_ = slots( info_ );
					pf_ = pf( info_ );

					// We need an instance to populate default values, but annoyingly, we don't know anything about our input yet...
					instance( 720, 576 );

					// Map plugin values to aml properties
					convert( wrapper_, instance_, props_, info_.num_params );

					// Expose the properties
					for ( std::vector< pl::pcos::property >::iterator iter = props_.begin( ); iter != props_.end( ); iter ++ )
					{
						properties( ).append( *iter );
						( *iter ).set_always_notify( true );
						( *iter ).attach( obs_prop_ );
					}
				}
			}
		}

		~frei0r_filter( )
		{
			if ( instance_ )
				wrapper_.destruct( instance_ );
		}

		// Indicates if the input will enforce a packet decode
		virtual bool requires_image( ) const { return true; }

		virtual std::wstring get_uri( ) const { return spec_; }

		virtual size_t slot_count( ) const { return prop_slots_.value< int >( ); }

		void updated( pcos::isubject * ) { changed_ = true; }

		virtual distributable_status is_distributable( ) { return prop_distributable_.value< int >( ) ? distributable_should : distributable_no; }

	protected:
		ml::fetch_status do_fetch( frame_type_ptr &result )
		{
			if ( prop_enable_.value< int >( ) )
			{
				switch( prop_slots_.value< int >( ) )
				{
					case 1:
						AR_TRY_FETCH( type1( result ) );
						break;
	
					case 2:
						AR_TRY_FETCH( type2( result ) );
						break;
	
					case 3:
						AR_TRY_FETCH( type3( result ) );
						break;
				}
			}
			else
			{
				AR_TRY_FETCH( fetch_from_slot( result ) );
			}

			return ml::fetch_ok;
		}

		ml::fetch_status type1( frame_type_ptr &result )
		{
			AR_TRY_FETCH( fetch_from_slot( result ) );

			if ( wrapper_.complete( ) && result && result->get_image( ) )
			{
				// Make sure we we have an instance of the right dimensions
				instance( result->width( ), result->height( ) );

				// Pass properties to frei0r instance
				if ( changed_ )
				{
					refresh( wrapper_, instance_, props_ );
					changed_ = false;
				}

				// Convert to derived picture format
				result = frame_convert( result, cls::to_t_string( pf_ ) );

				// Allocate a result image
				ml::image_type_ptr image = ml::image::allocate( result->get_image( ) );

				// Apply the effect
				wrapper_.update( instance_, result->get_duration( ) * get_position( ), ( const boost::uint32_t * )result->get_image( )->ptr( 0 ), ( boost::uint32_t * )image->ptr( 0 ) );

				// Set the result
				result->set_image( boost::move( image ) );
			}

			return ml::fetch_ok;
		}

		ml::fetch_status type2( frame_type_ptr &result )
		{
			ml::frame_type_ptr frame0, frame1;
			AR_TRY_FETCH( fetch_from_slot( frame0, 0 ) );
			AR_TRY_FETCH( fetch_from_slot( frame1, 1 ) );

			if ( wrapper_.complete( ) && frame0 && frame1 && frame0->get_image( ) && frame1->get_image( ) )
			{
				// Assign the result frame
				result = boost::move( frame0 );

				// Make sure we we have an instance of the right dimensions
				instance( result->width( ), result->height( ) );

				// Pass properties to frei0r instance
				if ( changed_ )
				{
					refresh( wrapper_, instance_, props_ );
					changed_ = false;
				}

				// Convert to derived picture format
				frame0 = boost::move( frame_convert( frame0, cls::to_t_string( pf_ ) ) );
				frame1 = boost::move( frame_convert( frame1, cls::to_t_string( pf_ ) ) );

				// Allocate a result image
				ml::image_type_ptr image = ml::image::allocate( result->get_image( ) );

				// Apply the effect
				wrapper_.update2( instance_, result->get_duration( ) * get_position( ), 
								  ( const boost::uint32_t * )frame0->get_image( )->ptr( 0 ), 
								  ( const boost::uint32_t * )frame1->get_image( )->ptr( 0 ), 
								  0, 
								  ( boost::uint32_t * )image->ptr( 0 ) );

				// Set the result
				result->set_image( boost::move( image ) );
			}
			else if ( frame0 && frame0->get_image( ) )
			{
				result = boost::move( frame0 );
			}
			else
			{
				result = boost::move( frame1 );
			}

			return ml::fetch_ok;
		}

		ml::fetch_status type3( frame_type_ptr &result )
		{
			ml::frame_type_ptr frame0, frame1, frame2;
			AR_TRY_FETCH( fetch_from_slot( frame0, 0 ) );
			AR_TRY_FETCH( fetch_from_slot( frame1, 1 ) );
			AR_TRY_FETCH( fetch_from_slot( frame2, 2 ) );

			if ( wrapper_.complete( ) && frame0 && frame1 && frame2 && frame0->get_image( ) && frame1->get_image( ) && frame2->get_image( ) )
			{
				// Assign the result frame
				result = boost::move( frame0 );

				// Make sure we we have an instance of the right dimensions
				instance( result->width( ), result->height( ) );

				// Pass properties to frei0r instance
				if ( changed_ )
				{
					refresh( wrapper_, instance_, props_ );
					changed_ = false;
				}

				// Convert to derived picture format
				frame0 = boost::move( frame_convert( frame0, cls::to_t_string( pf_ ) ) );
				frame1 = boost::move( frame_convert( frame1, cls::to_t_string( pf_ ) ) );
				frame2 = boost::move( frame_convert( frame2, cls::to_t_string( pf_ ) ) );

				// Allocate a result image
				ml::image_type_ptr image = ml::image::allocate( result->get_image( ) );

				// Apply the effect
				wrapper_.update2( instance_, result->get_duration( ) * get_position( ), 
								  ( const boost::uint32_t * )frame0->get_image( )->ptr( 0 ), 
								  ( const boost::uint32_t * )frame1->get_image( )->ptr( 0 ), 
								  ( const boost::uint32_t * )frame2->get_image( )->ptr( 0 ), 
								  ( boost::uint32_t * )image->ptr( 0 ) );

				// Set the result
				result->set_image( boost::move( image ) );
			}
			else if ( frame0 && frame0->get_image( ) )
			{
				result = boost::move( frame0 );
			}
			else if ( frame1 && frame1->get_image( ) )
			{
				result = boost::move( frame1 );
			}
			else
			{
				result = boost::move( frame2 );
			}

			return ml::fetch_ok;
		}

		f0r_instance_t instance( int width, int height )
		{
			// Check for dimension changes
			if ( instance_ && width != width_ && height != height_ )
			{
				wrapper_.destruct( instance_ );
				instance_ = 0;
			}

			// Construct our instance if we don't have one
			if ( !instance_ )
			{
				instance_ = wrapper_.construct( width, height );
				width_ = width;
				height_ = height;
			}

			return instance_;
		}

	private:
		std::wstring spec_;
		plugin_wrapper wrapper_;
		f0r_plugin_info_t info_;
		std::wstring pf_;
		f0r_instance_t instance_;
		int width_, height_;
		std::vector < pl::pcos::property > props_;
		pl::pcos::property prop_enable_;
		pl::pcos::property prop_slots_;
		pl::pcos::property prop_distributable_;
		bool changed_;
		std::shared_ptr< pcos::observer > obs_prop_;
};

//
// Plugin object
//

class ML_PLUGIN_DECLSPEC plugin : public openmedialib_plugin
{
public:

	virtual input_type_ptr input( const io::request &request )
	{
		const std::wstring spec = cl::str_util::to_wstring( request.uri( ) );
		return input_type_ptr( new frei0r_input( spec ) );
	}

	virtual store_type_ptr store( const io::request &, const frame_type_ptr & )
	{
		return store_type_ptr( );
	}

	virtual filter_type_ptr filter( const std::wstring &spec )
	{
		return filter_type_ptr( new frei0r_filter( spec ) );
	}
};

} } } }

//
// Access methods for openpluginlib
//

extern "C"
{
	ML_PLUGIN_DECLSPEC bool openplugin_init( void )
	{
		return true;
	}

	ML_PLUGIN_DECLSPEC bool openplugin_uninit( void )
	{
		return true;
	}
	
	ML_PLUGIN_DECLSPEC bool openplugin_create_plugin( const char*, pl::openplugin** plug )
	{
		*plug = new ml::frei0r::plugin;
		return true;
	}
	
	ML_PLUGIN_DECLSPEC void openplugin_destroy_plugin( pl::openplugin* plug )
	{ 
		delete static_cast< ml::frei0r::plugin * >( plug ); 
	}
}
