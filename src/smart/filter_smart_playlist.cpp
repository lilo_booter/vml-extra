// Smart Filters
//
// Copyright (C) 2018 Charles Yates
// Released under the LGPL.
//
// #filter:smart_playlist
//
// Provides a simplified wrapper for filter:playlist.
//
// <input1>
// <input2>
// filter:smart_playlist image=640x360 audio=48000@2 fps=25:1 slots=2
//
// Would form a playlist with 2 entries, resampled, rescaled and regulated at 
// the requested frame rate.

class ML_PLUGIN_DECLSPEC filter_smart_playlist : public filter_smart, public ml::filter_playlist_type
{
	public:
		filter_smart_playlist( )
		: prop_slots_( pl::pcos::key::from_string( "slots" ) )
		, prop_decode_( pl::pcos::key::from_string( "decode" ) )
		, prop_audio_( pl::pcos::key::from_string( "audio" ) )
		, prop_image_( pl::pcos::key::from_string( "image" ) )
		, prop_fps_( pl::pcos::key::from_string( "fps" ) )
		{   
			properties( ).append( prop_slots_ = 1 );
			properties( ).append( prop_decode_ = 0 );
			properties( ).append( prop_audio_ = std::wstring( L"48000@2" ) );
			properties( ).append( prop_image_ = std::wstring( L"1920x1080@1:1" ) );
			properties( ).append( prop_fps_ = std::wstring( L"25:1" ) );
			follow< filter_smart >( prop_slots_, this, &filter_smart::detach );
			follow< filter_smart >( prop_audio_, this, &filter_smart::detach );
			follow< filter_smart >( prop_image_, this, &filter_smart::detach );
			follow< filter_smart >( prop_fps_, this, &filter_smart::detach );
		}

		virtual std::wstring get_uri( ) const { return L"smart_playlist"; }

		virtual size_t slot_count( ) const { return size_t( prop_slots_.value< int >( ) ); }

	protected:
		virtual ml::input_type_ptr create_graph( )
		{
			ml::stack stack;

			const slots_type slots = fetch_slots( );
			const size_t count = slots.size( );

			const int decode = prop_decode_.value< int >( );
			const smart_audio audio( prop_audio_.value< std::wstring >( ), false );
			const smart_image image( prop_image_.value< std::wstring >( ), false );
			const smart_fps fps( prop_fps_.value< std::wstring >( ) );

			int offset = 0;

			for ( size_t slot = 0; slot < count; slot ++ )
			{
				int frame_count = 0;

				stack
				<< slots[ slot ]
				<< filter::decode( decode )
				<< filter::deinterlace( )
				<< filter::fps( fps )
				<< filter::resample( audio )
				<< filter::locked_audio( audio, offset )
				<< query::get_frames( )
				>> frame_count
				;

				offset += frame_count;
			}

			stack 
			<< filter::playlist( static_cast< int >( count ) )
			<< filter::rescale( image )
			;

			return stack.pop( );
		}

	private:
		pl::pcos::property prop_slots_;
		pl::pcos::property prop_decode_;
		pl::pcos::property prop_audio_;
		pl::pcos::property prop_image_;
		pl::pcos::property prop_fps_;
};

