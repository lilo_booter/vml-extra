// Smart Filters
//
// Copyright (C) 2018 Charles Yates
// Released under the LGPL.
//
// #filter:smart_fps
//
// Ensure the input is provided at a framerate of 25:1:
//
// <input>
// filter:smart_fps fps=25
//
// Note that it is invalid to use this filter without stipulating the fps you
// actually require.
//
// General form of the fps property is:
//
// fps=num[:den]
//
// where both num and den are integers.
//
// When den is not specified, it is assumed to be 1.

class ML_PLUGIN_DECLSPEC filter_smart_fps : public filter_smart
{
	public:
		filter_smart_fps( )
		: prop_fps_( pl::pcos::key::from_string( "fps" ) )
		{
			properties( ).append( prop_fps_ = std::wstring( L"" ) );
			follow< filter_smart >( prop_fps_, this, &filter_smart::detach );
		}

		std::wstring get_uri( ) const { return L"smart_fps"; }

		ml::distributable_status is_distributable( ) { return ml::distributable_no; }

	protected:
		ml::input_type_ptr create_graph( )
		{
			ml::stack stack;

			smart_fps fps( prop_fps_.value< std::wstring >( ) );

			stack
			<< fetch_slot( )
			<< boost::format( "filter:frame_rate fps_num=%d fps_den=%d" ) % fps.num % fps.den
			;

			return stack.pop( );
		}

	private:
		pl::pcos::property prop_fps_;
};


