// Smart Filters
//
// Copyright (C) 2020 Charles Yates
// Released under the LGPL.
//
// #filter:smart_stats
//
// Provides information about each frame fetched as a collections of frame
// properties.
//
// The most important one is 'frame_stats' which consists of a number of bits
// of information.
//
// The amount of information reported is configurable by way of the 'flags'
// property. Flags are treated as a bit pattern and specific bits can be
// turned on or off as follows:
//
// 0x01 - frame and fps
// 0x02 - timecode
// 0x04 - time to fetch
// 0x08 - total time passed
// 0x10 - virtual memory
// 0x20 - resident memory
// 0x40 - number of threads
//
// By default, all bits are set.

using namespace std::chrono;

#ifdef OLIB_ON_LINUX
void mem_usage( int64_t &vm_usage, int64_t &resident_set, int &numthreads )
{
	using namespace std;
	ifstream stat_stream( "/proc/self/stat", ios_base::in );
	string pid, comm, state, ppid, pgrp, session, tty_nr;
	string tpgid, flags, minflt, cminflt, majflt, cmajflt;
	string utime, stime, cutime, cstime, priority, nice;
	string O, itrealvalue, starttime;
	unsigned long vsize;
	long rss;
	stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
	>> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
	>> utime >> stime >> cutime >> cstime >> priority >> nice
	>> numthreads >> O >> starttime >> vsize >> rss;
	stat_stream.close();
	long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // for x86-64 is configured to use 2MB pages
	vm_usage = vsize / ( 1024 * 1024 );
	resident_set = rss * page_size_kb / 1024;
}
#elif OLIB_ON_WINDOWS
void mem_usage( int64_t &vm_usage, int64_t &resident_set, int &numthreads )
{
	MEMORYSTATUSEX memInfo;
	memInfo.dwLength = sizeof( MEMORYSTATUSEX );
	GlobalMemoryStatusEx( &memInfo );

	PROCESS_MEMORY_COUNTERS_EX pmc;
	GetProcessMemoryInfo( GetCurrentProcess( ), ( PROCESS_MEMORY_COUNTERS * )&pmc, sizeof( pmc ) );

	vm_usage = pmc.PrivateUsage / ( 1024 * 1024 );
	resident_set = pmc.WorkingSetSize / ( 1024 * 1024 );

	DWORD const  id = GetCurrentProcessId();

	HANDLE const  snapshot = CreateToolhelp32Snapshot( TH32CS_SNAPALL, 0 );
	PROCESSENTRY32 entry = { 0 };
	entry.dwSize = sizeof( entry );
	BOOL  ret = true;
	ret = Process32First( snapshot, &entry );
	while( ret && entry.th32ProcessID != id )
		ret = Process32Next( snapshot, &entry );
	CloseHandle( snapshot );

	numthreads = ret ? entry.cntThreads : -1;
}
#endif

class filter_smart_stats : public ml::filter_simple
{
	public:
		typedef boost::format bf;

		filter_smart_stats( )
		: prop_flags_( pl::pcos::key::from_string( "flags" ) )
		, prop_cerr_( pl::pcos::key::from_string( "cerr" ) )
		{
			properties( ).append( prop_flags_ = int( 0xff ) );
			properties( ).append( prop_cerr_ = 0 );
		}

		virtual ml::distributable_status is_distributable( ) { return ml::distributable_no; }

		virtual std::wstring get_uri( ) const { return L"smart_stats"; }

	protected:
		ml::fetch_status do_fetch( ml::frame_type_ptr &result )
		{
			// We'll defer initialisation of time state until the first fetch to avoid factoring
			// in delays between the ctor and now
			if ( !started_ )
			{
				started_ = true;
				start_time_point_ = steady_clock::now( );
				last_time_point_ = steady_clock::now( );
			}

			// We'll time how long it takes to fetch each frame
			auto now = steady_clock::now( );

			// Fetch the frame using normal error/eof handling (may want an eof event here though)
			AR_TRY_FETCH( fetch_from_slot( result ) );

			// Check if stats have already been collected and bail if need be
			auto report = pl::pcos::value< std::string >( result->properties( ), pl::pcos::key::from_string( "frame_stats" ), "" );
			if ( report != "" ) return ml::fetch_ok;

			// We calculate the difference between the current time and the time prior to the fetch
			const duration< double > time_to_fetch = steady_clock::now( ) - now;

			// We count the number of frames since our last time point
			frames_ ++;

			// To avoid wildly fluctuating fps readings while iterating over a graph with high throughput, 
			// refresh every second
			const duration< double > since = steady_clock::now( ) - last_time_point_;
			if ( since.count( ) >= 1.0 )
			{
				const duration< double > running_time = steady_clock::now( ) - last_time_point_;
				fps_ = double( frames_ ) / running_time.count( );
				last_time_point_ = steady_clock::now( );
				frames_ = 0;
			}

			// Report it - we need to get the std::ostream from somewhere else though
			auto in_fps = double( result->get_fps_num( ) ) / result->get_fps_den( );

			// Display timecode (should be property triggered) and should reflect original fps
			const int frame_count = pl::pcos::value< int >( result->properties( ), ml::keys::source_timecode, result->get_position( ) );
			const cl::rational_time fps = cl::rational_time( result->get_fps_num( ), result->get_fps_den( ) );
			const int drop_frame = is_dropframe_fps( fps ) ? pl::pcos::value< int >( result->properties( ), ml::keys::source_timecode_dropframe, 0 ) : 0;
			const cl::time_code tc = cl::frames_to_time_code( frame_count, fps, drop_frame != 0, true );

			// Display total time of playout so far
			const duration< double > total = steady_clock::now( ) - start_time_point_;

#if defined( OLIB_ON_LINUX ) || defined( OLIB_ON_WINDOWS )
			// Get some interesting OS info
			int64_t vm, rss;
			int numthreads;
			mem_usage( vm, rss, numthreads );
			result->properties( ).append( pl::pcos::property( pl::pcos::key::from_string( "frame_stats_vm" ) ) = int( vm ) );
			result->properties( ).append( pl::pcos::property( pl::pcos::key::from_string( "frame_stats_rss" ) ) = int( rss ) );
			result->properties( ).append( pl::pcos::property( pl::pcos::key::from_string( "frame_stats_threads" ) ) = numthreads );
#endif

			// Pass derived values on frame
			result->properties( ).append( pl::pcos::property( pl::pcos::key::from_string( "frame_stats_in_fps" ) ) = in_fps );
			result->properties( ).append( pl::pcos::property( pl::pcos::key::from_string( "frame_stats_fps" ) ) = fps_ );
			result->properties( ).append( pl::pcos::property( pl::pcos::key::from_string( "frame_stats_time" ) ) = time_to_fetch.count( ) );
			result->properties( ).append( pl::pcos::property( pl::pcos::key::from_string( "frame_stats_timecode" ) ) = cl::str_util::to_wstring( tc.to_string( ) ) );

			// Output all the requested stats
			int flags = prop_flags_.value< int >( );
			std::ostringstream stream;
			stream
			<< ( flags & 0x01 ? bf( "frame: %8d%c%-8d in: %7.2ffps out: %7.2ffps " ) % get_position( ) % ( is_complete( ) ? '/' : '\\' ) % get_frames( ) % in_fps % fps_ : bf( ) )
			<< ( flags & 0x02 ? bf( "timecode: %s " ) % cl::str_util::to_string( tc.to_string( ) ) : bf( ) )
			<< ( flags & 0x04 ? bf( "took: %2.5fs " ) % time_to_fetch.count( ) : bf( ) )
			<< ( flags & 0x08 ? bf( "total: %8.2fs " ) % total.count( ) : bf( ) )
#if defined( OLIB_ON_LINUX ) || defined( OLIB_ON_WINDOWS )
			<< ( flags & 0x10 ? bf( "virtual: %5dMB " ) % vm : bf( ) )
			<< ( flags & 0x20 ? bf( "resident: %5dMB " ) % rss : bf( ) )
			<< ( flags & 0x40 ? bf( "threads: %4d " ) % numthreads : bf( ) )
#endif
			;

			// Output accumulated stats as a string
			if ( prop_cerr_.value< int >( ) == 0 )
				result->properties( ).append( pl::pcos::property( pl::pcos::key::from_string( "frame_stats" ) ) = stream.str( ) );
			else
				std::cerr << stream.str( ) << "\r";

			// TODO: check other clocks?

			return ml::fetch_ok;
		}

	private:
		bool is_dropframe_fps( const cl::rational_time &fps )
		{
			return fps.denominator( ) == 1001 ? fps.numerator( ) == 30000 || fps.numerator( ) == 60000 : false;
		}

		pl::pcos::property prop_flags_;
		pl::pcos::property prop_cerr_;
		boost::optional< bool > started_;
		steady_clock::time_point start_time_point_;
		steady_clock::time_point last_time_point_;
		double fps_ = 0.0;
		int frames_ = 0;
};
