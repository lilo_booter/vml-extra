
class filter_smart_catchup : public ml::filter_simple
{
	public:
		filter_smart_catchup( )
		: prop_skip_( pl::pcos::key::from_string( "skip" ) )
		{
			properties( ).append( prop_skip_ = 50 );
		}

		virtual ~filter_smart_catchup( )
		{
		}

		virtual ml::distributable_status is_distributable( ) { return ml::distributable_no; }

		virtual std::wstring get_uri( ) const { return L"smart_catchup"; }

	protected:
		ml::fetch_status do_fetch( ml::frame_type_ptr &result )
		{
			while( skip != prop_skip_.value< int >( ) )
			{
				ml::fetch_status status = fetch_slot( )->fetch( result, get_position( ) + skip );
				if ( status != ml::fetch_ok ) return status;
				skip ++;
			}

			ml::fetch_status status = fetch_slot( )->fetch( result, get_position( ) + skip );
			if ( status != ml::fetch_ok ) return status;
			result = result->shallow( );
			result->set_position( output ++ );

			return ml::fetch_ok;
		}

	private:
		pl::pcos::property prop_skip_;
		int skip = 0;
		int output = 0;
};

