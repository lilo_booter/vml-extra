// Smart Filters
//
// Copyright (C) 2020 Charles Yates
// Released under the LGPL.
//
// #filter:smart_cut
//
// Create multiple cuts of a single input
//
// <input>
// filter:smart_cut list=1000:2000,3000:5000,1000*10,6000:-1@2.5
//
// Provides frames 1000 through 1099, then 3000 through 4999, then 1000
// repeated 10 times, followed by 6000 through to the end of the input at a
// speed of 2.5 (the latter is suggested for use with progressive material
// and will also silence audio).

namespace cut {

	// Provides a means to indicate modifcations required to frame
	enum hint_type
	{
		do_nothing = 0x0,
		do_silence = 0x1,
		do_field_swap = 0x2,
		do_reverse_audio = 0x4,
	};

	// Cut implementations must implement this interface
	class interface
	{
		public:
			virtual ~interface( ) { }
			virtual int get_frames( const ml::input_type_ptr & ) const = 0;
			virtual int calculate_position_for( const ml::input_type_ptr &input, int position, int &hint ) const = 0;
	};

	// Handles the range of frames of the form in:out and in:out@speed - negative numbers are treated
	// as relative to the end of the input, so 0:-1 would provide all frames in the original order,
	// while -1:0 would reverse the input
	class range : public interface
	{
		public:
			// Ctor just takes in/out/speed values as specified
			range( int in, int out, double speed = 1.0 ) : in_( in ), out_( out ), speed_( speed ) { }

			// This is basically the same as filter:clip, except for the 1 frame reverse special case and
			// introduction of speed
			virtual int get_frames( const ml::input_type_ptr &input ) const
			{
				const int frames = input->get_frames( );
				ARENFORCE_MSG( frames != std::numeric_limits< int >::max( ) || ( in_ >= 0 || out_ >= 0 ), "filter:smart_cut can't accept end relative in or out points for unknown sized inputs" );
				ARENFORCE_MSG( speed_ > 0.0, "Invalid speed of %f specified" )( speed_ );
				const int in = in_ >= 0 ? in_ : frames + in_;
				const int out = out_ >= 0 ? out_ : frames + out_ + 1;
				ARENFORCE_MSG( in >= 0 && in < frames, "In point %d out of range of available frames %d" )( in )( frames );
				ARENFORCE_MSG( out >= 0 && out <= frames, "Out point %d out of range of available frames %d" )( out )( frames );
				const int frames_in_cut = out - in;
				return static_cast< int >( ( frames_in_cut < 0 ? - frames_in_cut + 1 : frames_in_cut ) / speed_ );
			}

			// Calculate position require in the input based on in/out for this range
			virtual int calculate_position_for( const ml::input_type_ptr &input, int position, int &hint ) const
			{
				const int frames = input->get_frames( );
				const int in = in_ >= 0 ? in_ : frames + in_;
				const int out = out_ >= 0 ? out_ : frames + out_ + 1;
				if ( out < in ) hint = do_field_swap | do_reverse_audio;
				if ( speed_ != 1.0 ) hint |= do_silence;
				return static_cast< int >( in < out ? in + position * speed_ : in - position * speed_ );
			}

		private:
			int in_;
			int out_;
			double speed_;
	};

	// This provides the ability to hold/repeat a frame
	class frame : public interface
	{
		public:
			// Ctor can verify that the repeat count is sane
			frame( int in, int repeat = 1 ) : in_( in ), repeat_( repeat )
			{ ARENFORCE_MSG( repeat_ > 0, "Invalid repeat count of frame %d of %d" )( in )( repeat ); }

			// The length of the cut is just the repeat count
			virtual int get_frames( const ml::input_type_ptr &input ) const { return repeat_; }

			// Obtain the frame position
			virtual int calculate_position_for( const ml::input_type_ptr &input, int position, int &hint ) const
			{
				// Since we're repeating, audio should be removed and repopulated
				hint = do_silence;

				// Ensure that the requested frame position is sane
				const int frames = input->get_frames( );
				if ( in_ >= 0 )
				{
					ARENFORCE_MSG( in_ < frames, "Invalid frame of %d requested when only %d are available" )( in_ )( frames );
					return in_;
				}
				else
				{
					ARENFORCE_MSG( std::abs( in_ ) <= frames, "Invalid frame of %d requested when only %d are available" )( in_ )( frames );
					return frames + in_;
				}
			}

		private:
			int in_;
			int repeat_;
	};

	// Avoids the map usage having to be new/delete aware
	typedef std::shared_ptr< interface > interface_ptr;

	// Parses the token provided and generates the cut interface required
	interface_ptr parse_token( const std::string &token )
	{
		int in = 0;
		char op = 0;
		int out = 1;
		char at = 0;
		double speed = 1.0;

		std::istringstream istr( token );
		istr >> in >> op >> out >> at >> speed;

		if ( istr.eof( ) )
		{
			if ( op == 0 )
				return interface_ptr( new frame( in ) );
			else if ( op == '*' )
				return interface_ptr( new frame( in, out ) );
			else if ( op == ':' && at == '@' )
				return interface_ptr( new range( in, out, speed ) );
			else if ( op == ':' )
				return interface_ptr( new range( in, out ) );
		}

		ARENFORCE_MSG( false, "Unrecognised token '%s'" )( token );

		return interface_ptr( );
	}

	// The map provides a frame start position to the cut object
	typedef std::map< int, interface_ptr > map;

	// This really should be defined somewhere
	static pl::pcos::key key_audio_reversed = pl::pcos::key::from_string( "audio_reversed" );
};

// Just provides a list property which is re-evaluated on a sync or fetch if changed
class ML_PLUGIN_DECLSPEC filter_smart_cut : public ml::filter_simple
{
	public:
		filter_smart_cut( )
		: prop_list_( pl::pcos::key::from_string( "list" ) )
		{ properties( ).append( prop_list_ = std::string( "0:-1" ) ); }

		std::wstring get_uri( ) const { return L"smart_cut"; }

		ml::distributable_status is_distributable( ) { return ml::distributable_could; }

		int get_frames( ) const { return frame_count_; }

	protected:
		ml::fetch_status do_fetch( ml::frame_type_ptr &result )
		{
			// Parse the list if changed
			parse_list( prop_list_.value< std::string >( ) );

			// Return eof if out of range
			if ( get_position( ) >= get_frames( ) ) return ml::fetch_eof;

			// Map absolute position to position in input
			int hint = cut::do_nothing;
			int position = calculate_position_for( get_position( ), hint );

			// Diagnostics
			if ( debug_level( ) ) std::cerr << get_position( ) << " = " << position << std::endl;

			// Fetch the required frame
			ml::fetch_status status = fetch_slot( )->fetch( result, position );
			if ( status != ml::fetch_ok ) return status;

			// Update it as required
			result = result->shallow( );
			result->set_position( get_position( ) );

			// Nuke audio if required
			if ( hint & cut::do_silence )
				result->set_audio( ml::audio_type_ptr( ) );

			// Switch field order if required
			if ( hint & cut::do_field_swap && result->field_order( ) != ml::image::progressive )
			{
				ml::image_type_ptr image = result->get_image( )->shallow( );
				image->set_field_order( result->field_order( ) == ml::image::top_field_first ? ml::image::bottom_field_first : ml::image::top_field_first );
				result->set_image( std::move( image ) );
			}

			// Reverse audio if required
			if ( hint & cut::do_reverse_audio && result->has_audio( ) )
			{
				int reversed = pl::pcos::value< int >( result->properties( ), cut::key_audio_reversed, 0 );
				if ( reversed == 0 ) result->set_audio( ml::audio::reverse( result->get_audio( ) ) );
				pl::pcos::assign< int >( result->properties( ), cut::key_audio_reversed, 0 );
			}

			return ml::fetch_ok;
		}

		// Do some basic sanity checks and attempt to parse th elists property
		virtual void sync_frames( )
		{
			ARENFORCE_MSG( is_seekable( ), "filter:smart_cut only works with a seekable input" );
			parse_list( prop_list_.value< std::string >( ) );
		}

	private:
		// Parse the list - basically, just populate the map object
		void parse_list( const std::string &input )
		{
			auto list = input != "" ? input : std::string( "0:-1" );

			// Only parse if the value has changed
			if ( list != parsed_list_ )
			{
				// Ugly parsing - just use substr to reduce the list
				std::string remainder = list;
				if ( debug_level( ) ) std::cerr << "parsing " << remainder << std::endl;

				// Reset the state
				map_.clear( );
				frame_count_ = 0;

				while( remainder != "" )
				{
					// Extract a token and reduce remainder
					auto comma = remainder.find( ',' );
					auto token = remainder.substr( 0, comma );
					remainder = comma != std::string::npos ? remainder.substr( comma + 1 ) : "";
					if ( debug_level( ) ) std::cerr << "cut  : " << token << std::endl;

					// Generate the cut object
					map_[ frame_count_ ] = cut::parse_token( token );

					// Ensure it's valid and increment the frame count accordingly
					int count = map_[ frame_count_ ]->get_frames( fetch_slot( ) );
					ARENFORCE_MSG( count != std::numeric_limits< int >::max( ), "All cuts must be a known size" );
					frame_count_ += count;
					if ( debug_level( ) ) std::cerr << "cut  : has " << count << " totalling " << frame_count_ << std::endl;
				}

				// Update the parsed list value accordingly
				parsed_list_ = list;
			}
		}

		// Locate the required frame position
		int calculate_position_for( int position, int &hint ) const
		{
			if ( map_.begin( ) != map_.end( ) )
			{
				auto iter = -- map_.upper_bound( position );
				return iter->second->calculate_position_for( fetch_slot( ), position - iter->first, hint );
			}
			return 0;
		}

	private:
		pl::pcos::property prop_list_;
		std::string parsed_list_;
		int frame_count_ = std::numeric_limits< int >::max( );
		cut::map map_;
};
