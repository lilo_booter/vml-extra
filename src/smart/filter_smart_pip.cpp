// Smart Filters
//
// Copyright (C) 2018 Charles Yates
// Released under the LGPL.
//
// #filter:smart_pip
//
// Provides "picture in picture" support.
//
// <graph1> 
// <graph2> 
// filter:smart_pip geometry=<x>,<y>,<w>[,<h>] in=<duration> out=<duration>
//
// Composites <graph2> on <graph1> at the geometry specified. 
//
// The default geometry defines a square in the top right hand corner.
//
// The temporaral placement of the PIP is expressed relative to <graph1>, and 
// by default <graph2> is repeated or cut to match the duration of the in/out
// points.
//
// Hence:
//
// <graph1>
// file.png
// filter:smart_pip
//
// Will composite the png file over every frame in <graph1>.
//
// Unlike normal PIP, the default behaviour is to mix the audio of both graphs
// when present.

class ML_PLUGIN_DECLSPEC filter_stretch : public filter_simple
{
	public:
		// This is a private, non-exposed filter for the time being - it may prove 
		// useful in other scenarios.

		// Basic usage:
		//
		// <overlay> <background> filter_stretch in=<int> out=<int>
		//
		// The resultant graph is the overlay input 'stretched' or 'shrunk' to 
		// guarantee it provides the minimum of 'out - in' frames or the available 
		// background frames. The only requirement for the background is to determine 
		// the number of frames available (and this can thus be used to evaluate 
		// negative values since they are relative to the background).
		//
		// Note that the first frame requested is required to correlate with the in point 
		// of the filter, hence we will receive that as a request for position 0 here.
		// This is achieved by the placement of a downstream filter:offset with the 
		// correct in point.

		filter_stretch( )
		: prop_in_( pl::pcos::key::from_string( "in" ) )
		, prop_out_( pl::pcos::key::from_string( "out" ) )
		{
			properties( ).append( prop_in_ = 0 );
			properties( ).append( prop_out_ = -1 );
		}

		virtual std::wstring get_uri( ) const { return L"smart_internal_stretch"; }

		virtual ml::distributable_status is_distributable( ) { return ml::distributable_should; }

		virtual size_t slot_count( ) const { return 2; }

		virtual int get_frames( ) const { return evaluate( ); }

	protected:
		// The main access point to the filter
		ml::fetch_status do_fetch( ml::frame_type_ptr &result )
		{
			// Ensure that we can obtain a frame for the duration of the input by requesting
			// the modulo of position and the frames in the overlay.
			ml::input_type_ptr input = fetch_slot( );
			ARENFORCE_MSG( !!input && input->get_frames( ) > 0, "No connected input to stretch" );
			int position = get_position( ) % input->get_frames( );
		   	AR_TRY_FETCH( input->fetch( result, position ) );
			return ml::fetch_ok;
		}

		int evaluate( ) const
		{
			// Determines the duration of the overlay. 
			int in = prop_in_.value< int >( );
			int out = prop_out_.value< int >( );
			int available = fetch_slot( 1 )->get_frames( );
			if ( in < 0 )
				in = available + in + 1;
			if ( out < 0 )
				out = available + out + 1;
			int result = out - in + 1;
			if ( result > available )
				result = available;
			return result;
		}

		pl::pcos::property prop_in_;
		pl::pcos::property prop_out_;
};

ml::filter_type_ptr stretch( const smart_time &in, const smart_time &out )
{
	ml::filter_type_ptr result = ml::filter_type_ptr( new filter_stretch );
	result->property( "in" ) = in.value;
	result->property( "out" ) = out.value;
	return result;
}

class ML_PLUGIN_DECLSPEC filter_smart_pip : public filter_smart
{
	public:
		filter_smart_pip( )
		: prop_geometry_( pl::pcos::key::from_string( "geometry" ) )
		, prop_in_( pl::pcos::key::from_string( "in" ) )
		, prop_out_( pl::pcos::key::from_string( "out" ) )
		{
			properties( ).append( prop_geometry_ = std::wstring( L"0.8,0.1,0.15" ) );
			properties( ).append( prop_in_ = std::wstring( L"0" ) );
			properties( ).append( prop_out_ = std::wstring( L"-1" ) );
			follow< filter_smart >( prop_geometry_, this, &filter_smart::detach );
			follow< filter_smart >( prop_in_, this, &filter_smart::detach );
			follow< filter_smart >( prop_out_, this, &filter_smart::detach );
		}

		virtual std::wstring get_uri( ) const { return L"smart_pip"; }

		virtual size_t slot_count( ) const { return 2; }

	protected:
		virtual ml::input_type_ptr create_graph( const ml::frame_type_ptr &frame )
		{
			// Parse frame contents
			const smart_fps fps( frame );
			const smart_image image( frame );
			const smart_audio audio( frame );

			// Parse properties
			const smart_geometry geometry( prop_geometry_.value< std::wstring >( ) );
			const smart_time in( prop_in_.value< std::wstring >( ), fps );
			const smart_time out( prop_out_.value< std::wstring >( ), fps );

			// Derive offset in point
			const int offset = in.value < 0 ? fetch_slot( )->get_frames( ) + in.value : in.value;

			// Create the graph. This is a little bit complex - the use of this filter
			// requires that we are connected on two slots - the first slot is the video,
			// the second provides the overlay. Here we are going to create an internal 
			// graph which ensures that the overlay is composited according to the geometry 
			// and is active for the specified range of frames.
			ml::stack stack;

			stack
			// The first slot becomes the background of our internal graph
			<< fetch_slot( 0 )
			// The second slot is the overlay which we will modify:
			<< fetch_slot( 1 )
			// * Ensure that its fps matches that of the background
			<< filter::fps( fps )
			// * Introduce the stretch filter - this takes a reference to the background
			//   in order to ensure it knows the frame count (see filter_stretch above)
			<< fetch_slot( 0 )
			<< stretch( in, out )
			// * Modify the geometry of our stretched overlay
			<< filter::geometry( geometry )
			// * Ensure that the audio of the overlay (if any) is aligned with the background
			<< filter::resample( audio )
			// * Honour the in point of the filter (see filter_stretch above)
			<< boost::format( "filter:offset in=%d" ) % offset
			// Composite the overlay on to the background while ensuring the length of the
			// graph is derived from the first slot
			<< "filter:compositor track=1"
			;

			// Attach the graph such that fetch requests from the outer smart_pip are 
			// obtained from the derived graph
			return stack.pop( );
		}

	private:
		pl::pcos::property prop_geometry_;
		pl::pcos::property prop_in_;
		pl::pcos::property prop_out_;
};

