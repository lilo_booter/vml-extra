// Smart Filters
//
// Copyright (C) 2023 Charles Yates
// Released under the LGPL.
//
// #filter:smart_offset
//
// Combined compositor offset and locked audio functionality.
//
// Usage is as per the filter:offset with the exception of the placement
// of filter:locked_audio.
//
// For example:
//
// colour: silence: filter:muxer
// <input1> ... filter:smart_offset in=50
// <input2> ... filter:smart_offset in=102
// filter:compositor slots=3
//
// With the assumption that each of the inputs match the fps, audio sampling
// frequency and channel count of the background, the smart_offset filter will
// ensure that the sample count is always constant for any requested frame N.

class locked_audio
{
	public:
		void set( const ml::input_type_ptr &input, int in )
		{
			input_ = input;
			in_ = in;
			expected_ = 0;
			direction_ = 1;
			reseat_->clear( );
			cache_.clear( );
			last_frame_.reset( );
		}

		void debug( int level )
		{
			debug_ = level;
		}

		ml::fetch_status fetch( ml::frame_type_ptr &result, int position )
		{
			// Handle requests before the offset
			if ( position < in_ ) return ml::fetch_error;

			// Handle dupes
			if ( last( result, position ) ) return ml::fetch_ok;

			// Determine direction of requests
			direction_ = direction( position );

			// Handle unexpected requested
			if ( position != expected_ ) restart( );

			// We'll obtain the result frame immediately
			AR_TRY_FETCH( obtain( result, position ) );

			// Ensure audio matches reseat content
			normalise_audio( result );

			// If we have no audio, we're done here
			if ( !result->has_audio( ) ) return ml::fetch_ok;

			// Required number of samples for this absolute position
			const int samples_out = expected_samples_in_frame( result );

			// Actual samples in frame
			const int samples_in = result->samples( );

			// If we have an exact match and the reseat is clear
			if ( samples_in == samples_out && reseat_->size( ) == 0 ) return ml::fetch_ok;

			// Provide some debug info when we need to reseat
			if ( debug_ )
				std::cerr << position << ": in = " << samples_in << " + " << reseat_->size( ) << " out = " << samples_out << std::endl;

			// Append new samples to reseater
			reseat_->append( result->get_audio( ) );

			// Populate reseater until samples available
			if( position + direction_ >= 0 && !reseat_->has( samples_out ) )
			{
				ml::frame_type_ptr temp;

				// Obtain next frame and handle error and eof cases
				auto status = obtain( temp, position + direction_ );
				if ( status == ml::fetch_error ) return ml::fetch_error;

				if ( status == ml::fetch_ok )
				{
					// Ensure we have the audio type
					normalise_audio( result );

					// Append to reseater
					reseat_->append( temp->get_audio( ) );
				}
			}

			// Retrieve the audio from the reseater
			auto audio = reseat_->retrieve( samples_out, true );
			result->set_audio( audio );

			// Indicate the direction of the audio
			pl::pcos::assign< int >( result->properties( ), ml::keys::audio_reversed, direction_ >= 0 ? 0 : 1 );

			// Set up for the next request
			last_frame_ = std::move( result->shallow( ) );
			expected_ = position + direction_;

			return ml::fetch_ok;
		}

	private:
		int expected_samples_in_frame( ml::frame_type_ptr &frame ) const
		{
			// Obtain relevant info from frame
			const auto frequency = reseat_->begin( ) != reseat_->end( ) ? ( *reseat_->begin( ) )->frequency( ) : frame->frequency( );
			const auto fps_num = frame->get_fps_num( );
			const auto fps_den = frame->get_fps_den( );

			// Required number of samples for this absolute position
			return samples_in_frame( frame->get_position( ), frequency, fps_num, fps_den );
		}

		// Caclcuate the number of samples which should be present in the frame at the specified position
		int samples_in_frame( int position, int frequency, int fps_num, int fps_den ) const
		{
			const int64_t first_sample_of_pos = ml::audio::samples_to_frame( position, frequency, fps_num, fps_den );
			const int64_t first_sample_of_next = ml::audio::samples_to_frame( position + 1, frequency, fps_num, fps_den );
			return static_cast< int >( first_sample_of_next - first_sample_of_pos );
		}

		// Handle last frame if repeatedly requested
		bool last( ml::frame_type_ptr &frame, int position ) const
		{
			bool result = last_frame_ && last_frame_->get_position( ) == position;
			if ( result ) frame = std::move( last_frame_->shallow( ) );
			return result;
		}

		// Determine if direction has changed
		int direction( int position ) const
		{
			int result = direction_;
			if ( position != expected_ )
				result = position == expected_ - 2 ? -1 : 1;
			return result;
		}

		// Clear cache and reseater
		void restart( )
		{
			reseat_->clear( );
			cache_.clear( );
		}

		// Cache and rertieve the requested frame
		ml::fetch_status obtain( ml::frame_type_ptr &result, int position )
		{
			auto iter = cache_.find( position );

			if ( iter == cache_.end( ) )
			{
				input_->seek( position - in_ );
				AR_TRY_FETCH( input_->fetch( result ) );
				result->set_position( position );
				cache_[ position ] = result->shallow( );
			}
			else
			{
				result = iter->second->shallow( );
			}

			maintain_cache( );

			return ml::fetch_ok;
		}

		// Normalise audio
		void normalise_audio( ml::frame_type_ptr& frame ) const
		{
			// Make sure we have audio of the right type
			auto iter = reseat_->begin( );
			if ( iter != reseat_->end( ) )
			{
				if ( frame->has_audio( ) )
				{
					auto audio = frame->get_audio( );
					if ( audio->id( ) != ( *iter )->id( ) )
						audio = ml::audio::coerce( ( *iter )->id( ), audio );
					frame->set_audio( audio, true );
				}
				else
				{
					int samples = expected_samples_in_frame( frame );
					auto audio = ml::audio::allocate( *iter, -1, -1, samples, true );
					frame->set_audio( audio );
				}
			}

			// Make sure audio is in the order of playout
			ml::audio_type_ptr audio = frame->get_audio( );
			if ( audio )
			{
				int reversed = pl::pcos::value< int >( frame->properties( ), ml::keys::audio_reversed, 0 );
				if ( ( direction_ < 0 && reversed == 0 ) || ( direction_ > 0 && reversed == 1 ) )
				{
					pl::pcos::assign< int >( frame->properties( ), ml::keys::audio_reversed, reversed ? 0 : 1 );
					frame->set_audio( ml::audio::reverse( audio ) );
				}
			}
		}

		// Ensures we keep a minimal cache
		void maintain_cache( )
		{
			if ( cache_.size( ) > 5 )
				cache_.erase( direction_ > 0 ? cache_.begin( ) : -- cache_.end( ) );
		}

		ml::input_type_ptr input_;
		int in_;
		int expected_ = -1;
		int direction_ = 1;
		ml::frame_type_ptr last_frame_;
		ml::audio::reseat_ptr reseat_ = ml::audio::create_reseat( );
		std::map< int, ml::frame_type_ptr > cache_;
		int debug_ = 0;
};

class filter_smart_offset : public ml::filter_offset_type
{
	public:
		filter_smart_offset( )
		: prop_in_( pl::pcos::key::from_string( "in" ) )
		, obs_in_( new fn_observer< filter_smart_offset >( const_cast< filter_smart_offset * >( this ), &filter_smart_offset::changed ) )
		{
			properties( ).append( prop_in_ = 0 );
		}

		// Used for serialization/deserialization
		virtual std::wstring get_uri( ) const override { return L"smart_offset"; }

		// As with all audio, we have to be stateful here
		virtual ml::distributable_status is_distributable( ) override { return ml::distributable_no; }

		// Returns the current offset
		virtual int get_offset( ) const override { return prop_in_.value< int >( ); }

		// Ensure we register a reconnect as a change
		virtual void after_slot_change( int ) override { changed( ); }

	protected:
		// Fetch the requested frame ensuring that audio is correctly sized
		ml::fetch_status do_fetch( ml::frame_type_ptr &result ) override
		{
			audio_.debug( debug_level( ) );
			return audio_.fetch( result, get_position( ) );
		}

		// Invoked when changes are detected
		void changed( )
		{
			audio_.set( fetch_slot( 0 ), prop_in_.value< int >( ) );
		}

	private:
		pl::pcos::property prop_in_;
		pl::pcos::observer_ptr obs_in_;
		locked_audio audio_;
		bool changed_ = true;
};
