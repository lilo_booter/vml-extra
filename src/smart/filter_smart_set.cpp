// Smart Filters
//
// Copyright (C) 2018 Charles Yates
// Released under the LGPL.
//
// #filter:smart_set
//
// Modifies settings on components:
//
// image={name=value}[,{name=value}]*
//
// Where name can be:
//
// * field_order : progressive, tff or bff
// * sar : rational pair of ints
// * ar : rational pair of ints
//
// audio={name=value}[,{name=value}]*
//
// Where name can be:
//
// * volume : float
// * volume_db : float
//
// props={name=value}[,{name=value}]*
// video_props={name=value}[,{name=value}]*
//
// Contents of properties are assigned to frame and video_packet as requested.
// Types of existing properties are preserved.

namespace smart_set 
{
	// Types which are used here
	typedef boost::rational< int > rational;

	// Updates the field order
	void field_order( const ml::frame_type_ptr &frame, const ml::image::field_order_flags &flag )
	{
		frame->get_image( )->set_field_order( flag );
	}

	// Updates the sar
	void sar( const ml::frame_type_ptr &frame, const rational &sar )
	{
		frame->set_sar( sar.numerator( ), sar.denominator( ) );
	}

	// Updates the ar by way of calculating the requested sar
	void ar( const ml::frame_type_ptr &frame, const rational &ar )
	{
		sar( frame, rational( frame->height( ), frame->width( ) ) * ar );
	}

	// Modifies the volume on the audio
	void volume( const ml::frame_type_ptr &frame, const float &level )
	{
		frame->set_audio( ml::audio::volume( frame->get_audio( ), level, level ) );
	}

	// Modifies the volume on the audio by decibel
	void volume_db( const ml::frame_type_ptr &frame, const float &db )
	{
		volume( frame, std::pow( 2.0f, db / 10.0f ) );
	}

	// Convert std::wstring to various types
	template< class T >
	bool convert( T &result, const std::wstring &value )
	{
		try
		{
			result = boost::lexical_cast< T >( value );
			return true;
		}
		catch( ... )
		{
			return false;
		}
	}

	// Convert std::wstring to field_order
	bool convert( ml::image::field_order_flags &flag, const std::wstring &order )
	{
		bool result = true;

		if ( order == L"bff" )
			flag = ml::image::bottom_field_first;
		else if ( order == L"tff" )
			flag = ml::image::top_field_first;
		else if ( order == L"progressive" )
			flag = ml::image::progressive;
		else
			result = false;

		return result;
	}

	// Convert std::wstring to a rational
	bool convert( rational &rat, const std::wstring &value )
	{
		int num, den;
		wchar_t colon;
		std::wistringstream iss( value );
		iss >> num >> colon >> den;
		rat.assign( num, den );
		return iss.eof( );
	}
}

class ML_PLUGIN_DECLSPEC filter_smart_set : public filter_smart
{
	public:
		filter_smart_set( )
		: prop_image_( pl::pcos::key::from_string( "image" ) )
		, prop_audio_( pl::pcos::key::from_string( "audio" ) )
		, prop_props_( pl::pcos::key::from_string( "props" ) )
		, prop_video_props_( pl::pcos::key::from_string( "video_props" ) )
		{
			properties( ).append( prop_image_ = ml::profile_map( ) );
			properties( ).append( prop_audio_ = ml::profile_map( ) );
			properties( ).append( prop_props_ = ml::profile_map( ) );
			properties( ).append( prop_video_props_ = ml::profile_map( ) );
		}

		virtual std::wstring get_uri( ) const { return L"smart_set"; }

	protected:
		// We have no graph to insert
		virtual ml::input_type_ptr create_graph( )
		{
			return ml::input_type_ptr( );
		}

		// We're going to override the do_fetch here since we have no internal graph
		ml::fetch_status do_fetch( ml::frame_type_ptr &result )
		{
			AR_TRY_FETCH( fetch_from_slot( result ) );

			// We're going to allow arbitrary updates here
			result = result->shallow( );

			// Deal with properties
			update_properties( result, prop_props_ );
			update_properties( result->get_video_packet( ), prop_video_props_ );

			// Deal with known settings
			update_image( result, prop_image_ );
			update_audio( result, prop_audio_ );

			return ml::fetch_ok;
		}

		// Update properties from property map
		template< class T >
		void update_properties( const T &object, pl::pcos::property &property ) const
		{
			if ( !object ) return;

			const pl::pcos::property_container props = object->properties( );
			const ml::profile_map map = property.value< ml::profile_map >( );

			for ( ml::profile_map::const_iterator iter = map.begin( ); iter != map.end( ); iter ++ )
			{
				const pl::pcos::key key = pl::pcos::key::from_string( cls::to_string( iter->first ).c_str( ) );
				pl::pcos::property property = props.get_property_with_key( key );
				if ( property.valid( ) )
					property.set_from_string( iter->second.c_str( ) );
				else
					pl::pcos::assign( props, key, iter->second );
			}
		}

		// Update image from property
		void update_image( const ml::frame_type_ptr &result, pl::pcos::property &property ) const
		{
			ml::profile_map map = property.value< ml::profile_map >( );
			if ( map.size( ) == 0 || !result->has_image( ) ) return;

			update( result, map, L"field_order", smart_set::field_order );
			update( result, map, L"sar", smart_set::sar );
			update( result, map, L"ar", smart_set::ar );

			ARENFORCE_MSG( map.size( ) == 0, "Invalid options in image: %s" )( contents( map ) );
		}

		// Update image from property
		void update_audio( const ml::frame_type_ptr &result, pl::pcos::property &property ) const
		{
			ml::profile_map map = property.value< ml::profile_map >( );
			if ( map.size( ) == 0 || !result->has_audio( ) ) return;

			update( result, map, L"volume", smart_set::volume );
			update( result, map, L"volume_db", smart_set::volume_db );

			ARENFORCE_MSG( map.size( ) == 0, "Invalid options in audio: %s" )( contents( map ) );
		}

		// Obtains value from map, converts and sets on the frame
		template < class T >
		void update( const ml::frame_type_ptr &result, ml::profile_map &map, const std::wstring &key, void ( *setter )( const ml::frame_type_ptr &result, const T & ) ) const
		{
			T setting;
			if ( obtain( map, key, setting ) )
				setter( result, setting );
		}

		// Templated convenience method - fetches value from key, and involves a convert method to map from
		// the std::wstring value to the type of the requested setting. Convert methods defined below
		template< class T >
		bool obtain( ml::profile_map &map, const std::wstring &key, T &setting ) const
		{
			std::wstring value;
			bool result = ml::consume_optional( map, key, value );
			if ( result ) { ARENFORCE_MSG( smart_set::convert( setting, value ), "Invalid assignment of %s=%s" )( key )( value ); }
			return result;
		}

		// Obtain printable contents of map
		std::wstring contents( const ml::profile_map &map ) const
		{
			std::wstring result;
			for ( ml::profile_map::const_iterator iter = map.begin( ); iter != map.end( ); iter ++ )
				result += iter->first + L"=" + iter->second + L" ";
			return result;
		}

	private:
		pl::pcos::property prop_image_;
		pl::pcos::property prop_audio_;
		pl::pcos::property prop_props_;
		pl::pcos::property prop_video_props_;
};
