// Smart Filters
//
// Copyright (C) 2018 Charles Yates
// Released under the LGPL.
//
// #filter:smart_clip
//
// Clip by time or frames.
//
// <input>
// filter:smart_clip in=10000ms out=-1000
//
// Clip the first 10s and last 1000 frames from the input.
//
// General form of the in and out properties are:
//
// in=offset[unit] out=offset[unit]
//
// offsets are signed doubles and units are ms, s or unspecified for frames.
//
// When neither are specified they default to 0 and -1 respectively.

class ML_PLUGIN_DECLSPEC filter_smart_clip : public filter_smart
{
	public:
		filter_smart_clip( )
		: prop_in_( pl::pcos::key::from_string( "in" ) )
		, prop_out_( pl::pcos::key::from_string( "out" ) )
		, fps_num_( 0 )
		, fps_den_( 0 )
		{
			properties( ).append( prop_in_ = std::wstring( L"0" ) );
			properties( ).append( prop_out_ = std::wstring( L"-1" ) );
			follow< filter_smart_clip >( prop_in_, this, &filter_smart_clip::refresh );
			follow< filter_smart_clip >( prop_out_, this, &filter_smart_clip::refresh );
		}

		std::wstring get_uri( ) const { return L"smart_clip"; }

		ml::distributable_status is_distributable( ) { return ml::distributable_could; }

	protected:
		ml::input_type_ptr create_graph( const frame_type_ptr &frame )
		{
			// Initialise from the frame
			frame->get_fps( fps_num_, fps_den_ );

			// We'll be connecting to this input
			ml::input_type_ptr input = fetch_slot( );

			// Log, create, update and attach
			SMART_DEBUG( SMART_ID << " creating filter:clip" );
			ml::filter_type_ptr clip = create_filter( L"clip" );
			update( clip );
			clip->connect( input );

			// Attach it to the smart filter
			return clip;
		}

	private:
		void refresh( )
		{
			update( attachment( ) );
		}

		void update( const ml::input_type_ptr &clip )
		{
			if ( !clip || fps_den_ == 0 ) return;
			const int in = smart_time( prop_in_.value< std::wstring >( ), fps_num_, fps_den_ ).value;
			const int out = smart_time( prop_out_.value< std::wstring >( ), fps_num_, fps_den_ ).value;
			SMART_DEBUG( SMART_ID << " updating filter:clip in=" << in << " out=" << out );
			clip->property( "in" ) = in;
			clip->property( "out" ) = out;
		}

		pl::pcos::property prop_in_;
		pl::pcos::property prop_out_;
		int fps_num_;
		int fps_den_;
};

