// Smart Filters
//
// Copyright (C) 2018 Charles Yates
// Released under the LGPL.
//
// GENERAL INFORMATION:
//
// The filter:smart_ family are designed to provide simpler interfaces and more
// efficient use of resources.
//
// For example:
//
// <input>
// filter:smart_fps fps=25:1
//
// Would insert a filter:frame_rate fps_num=25 fps_den=1 in the graph, but only
// if the input provides a framerate other that 25:1.

#ifdef OLIB_ON_WINDOWS
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <psapi.h>
#include <tlhelp32.h>
#endif

#include <chrono>
#include <thread>
#include <mutex>
#include <condition_variable>

#include <opencorelib/cl/core.hpp>
#include <opencorelib/cl/enforce_defines.hpp>
#include <openmedialib/ml/openmedialib_plugin.hpp>
#include <openmedialib/ml/stack.hpp>
#include <openmedialib/ml/encoding_profile.hpp>
#include <openmedialib/ml/keys.hpp>
#include <openmedialib/ml/audio_utilities.hpp>
#include <ml/field_regulate.hpp>
#include <ml/fps_regulate.hpp>
#include "plugin_smart.hpp"

#include <boost/optional.hpp>

namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;
namespace ml = olib::openmedialib::ml;
namespace cls = olib::opencorelib::str_util;

namespace olib { namespace openmedialib { namespace ml {

namespace pl = olib::openpluginlib;

} } }


namespace olib { namespace openmedialib { namespace ml { namespace smart {

#include "smart_parsers.cpp"
#include "smart_generators.cpp"

#include "filter_smart_alpha_double.cpp"
#include "filter_smart_alpha_halve.cpp"
#include "filter_smart_catchup.cpp"
#include "filter_smart_clip.cpp"
#include "filter_smart_cut.cpp"
#include "filter_smart_fps.cpp"
#include "filter_smart_mosaic.cpp"
#include "filter_smart_muxer.cpp"
#include "filter_smart_offset.cpp"
#include "filter_smart_pip.cpp"
#include "filter_smart_playlist.cpp"
#include "filter_smart_repair.cpp"
#include "filter_smart_resample.cpp"
#include "filter_smart_rescale.cpp"
#include "filter_smart_set.cpp"
#include "filter_smart_sleep.cpp"
#include "filter_smart_stats.cpp"
#include "filter_smart_timeline.cpp"

#include "input_smart_background.cpp"
#include "input_smart_pusher.cpp"

#include "store_smart.cpp"
#include "store_smart_puller.cpp"

//
// Plugin object
//

class ML_PLUGIN_DECLSPEC plugin : public openmedialib_plugin
{
public:
	virtual ml::filter_type_ptr filter( const std::wstring &spec )
	{
		if ( spec == L"smart_alpha_double" )
			return ml::filter_type_ptr( new filter_smart_alpha_double( ) );
		else if ( spec == L"smart_alpha_halve" )
			return ml::filter_type_ptr( new filter_smart_alpha_halve( ) );
		else if ( spec == L"smart_catchup" )
			return ml::filter_type_ptr( new filter_smart_catchup( ) );
		else if ( spec == L"smart_clip" )
			return ml::filter_type_ptr( new filter_smart_clip( ) );
		else if ( spec == L"smart_cut" )
			return ml::filter_type_ptr( new filter_smart_cut( ) );
		else if ( spec == L"smart_fps" )
			return ml::filter_type_ptr( new filter_smart_fps( ) );
		else if ( spec == L"smart_mosaic" )
			return ml::filter_type_ptr( new filter_smart_mosaic( ) );
		else if ( spec == L"smart_muxer" )
			return ml::filter_type_ptr( new filter_smart_muxer( ) );
		else if ( spec == L"smart_offset" )
			return ml::filter_type_ptr( new filter_smart_offset( ) );
		else if ( spec == L"smart_pip" )
			return ml::filter_type_ptr( new filter_smart_pip( ) );
		else if ( spec == L"smart_playlist" )
			return ml::filter_type_ptr( new filter_smart_playlist( ) );
		else if ( spec == L"smart_repair" )
			return ml::filter_type_ptr( new filter_smart_repair( ) );
		else if ( spec == L"smart_resample" )
			return ml::filter_type_ptr( new filter_smart_resample( ) );
		else if ( spec == L"smart_rescale" )
			return ml::filter_type_ptr( new filter_smart_rescale( ) );
		else if ( spec == L"smart_set" )
			return ml::filter_type_ptr( new filter_smart_set( ) );
		else if ( spec == L"smart_sleep" )
			return ml::filter_type_ptr( new filter_smart_sleep( ) );
		else if ( spec == L"smart_stats" )
			return ml::filter_type_ptr( new filter_smart_stats( ) );
		else if ( spec == L"smart_timeline" )
			return ml::filter_type_ptr( new filter_smart_timeline( ) );

		else if ( spec == L"smart_internal_merge_alpha" )
			return ml::filter_type_ptr( new filter_merge_alpha( ) );
		else if ( spec == L"smart_internal_stretch" )
			return ml::filter_type_ptr( new filter_stretch( ) );

		ARENFORCE( "Unknown smart filter request: %s" )( spec );
		return ml::filter_type_ptr( );
	}

	virtual ml::input_type_ptr input( const ml::io::request &request )
	{
		const std::wstring spec = cl::str_util::to_wstring( request.uri( ) );

		if ( spec == L"smart_background:" )
			return ml::input_type_ptr( new input_smart_background( ) );
		if ( spec == L"smart_pusher:" )
			return ml::input_type_ptr( new input_smart_pusher( ) );

		ARENFORCE( "Unknown smart input request: %s" )( spec );
		return ml::input_type_ptr( );
	}

	virtual ml::store_type_ptr store( const io::request &request, const frame_type_ptr &frame )
	{
		const std::wstring spec = cl::str_util::to_wstring( request.uri( ) );
		if ( spec == L"smart_puller:" )
			return ml::store_type_ptr( new store_smart_puller( ) );

		ARENFORCE_MSG( !!frame, "Can't initialise a smart: store without a sample input frame" );
		ARENFORCE_MSG( spec.find( L"smart:" ) == 0, "Invalid smart store specification %s" )( spec );
		const std::wstring store = spec.substr( 6 );
		ARENFORCE_MSG( store != L"", "Invalid store specification %s" )( spec );
		return ml::store_type_ptr( new store_smart( store, frame ) );
	}
};

} } } }

//
// Access methods for openpluginlib
//

extern "C"
{
	ML_PLUGIN_DECLSPEC bool openplugin_init( void )
	{
		return true;
	}

	ML_PLUGIN_DECLSPEC bool openplugin_uninit( void )
	{
		return true;
	}

	ML_PLUGIN_DECLSPEC bool openplugin_create_plugin( const char*, pl::openplugin** plug )
	{
		*plug = new ml::smart::plugin;
		return true;
	}

	ML_PLUGIN_DECLSPEC void openplugin_destroy_plugin( pl::openplugin* plug )
	{
		delete static_cast< ml::smart::plugin * >( plug );
	}
}

