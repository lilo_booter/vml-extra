namespace input {

// input::colour( image, fps )
//
// Creates a colour: input of the requested form

inline bwf colour( const smart_image &im, const smart_fps &fps )
{
	return bwf( L"colour: width=%d height=%d sar_num=%d sar_den=%d fps_num=%d fps_den=%d interlace=%d r=%d g=%d b=%d a=%d" ) 
				% im.width % im.height % im.sar_num % im.sar_den % fps.num % fps.den % int( im.fo ) % im.r % im.g % im.b % im.a;
}


// input::silence( audio, fps )
//
// Create a silence: input of the requested form

inline bwf silence( const smart_audio &aud, const smart_fps &fps )
{
	return bwf( L"silence: af=%S channels=%d fps_den=%d fps_num=%d frequency=%d profile=%S" )
				% aud.af % aud.channels % fps.den % fps.num % aud.frequency % aud.profile;
}


// input::background( image, audio, fps )
//
// Provides a colour: silence: filter:muxer input of the requested form

inline bwf background( const smart_image &im, const smart_audio &aud, const smart_fps &fps )
{
	return bwf( L"%S %S filter:muxer" ) % colour( im, fps ) % silence( aud, fps );
}


}

namespace filter {

// filter::decode( value, threads )
//
// Attempts to coerce the input on the stack to packet streaming mode and decodes

inline bwf decode( int value = -1, int threads = 4 )
{
	return value != 0 ? bwf( L"?packet_stream=%d filter:decode inner_threads=%d" ) % value % threads : bwf( );
}



// filter::colour_space( image )
//
// Provides a means to change the colour space to that of the image

inline bwf colour_space( const smart_image &im )
{
	return bwf( L"filter:swscale pf=%S" ) % im.pf;
}



// filter::fps( fps )
//
// Provides a means to convert the frame rate

inline bwf fps( const smart_fps &fps )
{
	return bwf( L"filter:frame_rate fps_num=%d fps_den=%d" ) % fps.num % fps.den;
}



// filter::geometry( geometry )
//
// Stipulates the geometry a downstream compositor should use

inline bsf geometry( const smart_geometry &g )
{
	return bsf( "filter:lerp @@x=%g @@y=%g @@w=%g @@h=%g" ) % g.x % g.y % g.w % g.h;
}



// filter::mix( lower, upper, frames )
//
// Stipulates the geometry a downstream compositor should use

inline bsf mix( double lower, double upper, int frames )
{
	return bsf( "filter:lerp @@mix=%g:%g:%d:%d" ) % lower % upper % 0 % frames;
}



// filter::resample( audio )
//
// Change the audio as requested

inline bwf resample( const smart_audio &a )
{
	return bwf( L"filter:resampler frequency=%d channels=%d af=%S" ) % a.frequency % a.channels % a.af;
}



// filter::rescale( image )
//
// Rescale the image as required

inline bwf rescale( const smart_image &im )
{
	return im.width * im.height > 0 ? 
		   bwf( L"filter:swscale width=%d height=%d sar_num=%d sar_den=%d interp=%d pf=%S" ) 
				% im.width % im.height % im.sar_num % im.sar_den % im.interp % im.pf : 
		   bwf( );
}



// filter::deinterlace( )
//
// Attempt to deinterlace

inline std::string deinterlace( )
{
	return "filter:avvideo_yadif options=mode=send_field";
}



// filter::deinterlace( )
//
// Attempt to deinterlace

inline std::string deinterlace( const smart_image &image )
{
	return image.fo == ml::image::progressive ? deinterlace( ) : "";
}



// filter::visualise( )
//
// Apply the audio visualise filter

inline bsf visualise( int type = 1 )
{
	return type >= 0 ? bsf( "filter:visualise type=%d" ) % type : bsf( );
}



// filter::offset( offset )
//
// Introduces a compositor offset

inline bwf offset( int offset )
{
	return bwf( L"filter:offset in=%d" ) % offset;
}



// filter::locked_audio( audio, offset )
//
// Lock audio as required

inline bwf locked_audio( const smart_audio &a, int offset )
{
	return bwf( L"filter:locked_audio profile=%S offset=%d" ) % a.profile % offset;
}



// filter::playlist( slots )
//
// Create a playlist with the specified number of slots

inline bwf playlist( int slots )
{
	return slots >= 0 ? bwf( L"filter:playlist slots=%d" ) % slots : bwf( );
}



// filter::compositor( slots )
//
// Create a compositor with the specified number of slots

inline bwf compositor( int slots )
{
	return slots >= 0 ? bwf( L"filter:compositor slots=%d" ) % slots : bwf( );
}

}


namespace query {

// query::get_frames( )
//
// Pushes the number of frames on the input on the top of the stack to the push.
//
// int frames;
// stack 
// << query::get_frames( ) 
// >> frames;
//
// or:
//
// int frames = stack.push( query::get_frames( ) ).pop( );

inline const char *get_frames( )
{
	return "length?";
}

}
