// Smart Input
//
// Copyright (C) 2018 Charles Yates
// Released under the LGPL.
//
// #input:smart_background:
//
// Provides a background of the requested type - both video and audio are 
// included in the output (when requested).

class ML_PLUGIN_DECLSPEC input_smart_background : public input_smart
{
	public:
		input_smart_background( )
		: prop_audio_( pl::pcos::key::from_string( "audio" ) )
		, prop_image_( pl::pcos::key::from_string( "image" ) )
		, prop_fps_( pl::pcos::key::from_string( "fps" ) )
		{
			properties( ).append( prop_audio_ = std::wstring( L"48000@2" ) );
			properties( ).append( prop_image_ = std::wstring( L"1920x1080" ) );
			properties( ).append( prop_fps_ = std::wstring( L"25:1" ) );
			follow< input_smart >( prop_audio_, this, &input_smart::detach );
			follow< input_smart >( prop_image_, this, &input_smart::detach );
			follow< input_smart >( prop_fps_, this, &input_smart::detach );
		}

		virtual std::wstring get_uri( ) const { return L"smart_background:"; }

		virtual bool is_seekable( ) const { return true; }

		virtual bool determine_complete( ) const { return true; }

	protected:
		virtual ml::input_type_ptr create_graph( )
		{
			// We'll use a stack for simplicity and count the number of components
			ml::stack stack;
			int components = 0;

			// Parse the properties
			smart_fps fps( prop_fps_.value< std::wstring >( ) );

			// If image is requested, handle that first
			if ( prop_image_.value< std::wstring >( ) != L"" )
			{
				smart_image im( prop_image_.value< std::wstring >( ) );
				stack << input::colour( im, fps );
				components ++;
			}

			// Then check on audio
			if ( prop_audio_.value< std::wstring >( ) != L"" )
			{
				smart_audio aud( prop_audio_.value< std::wstring >( ) );
				stack << input::silence( aud, fps );
				components ++;
			}

			// Make sure we have something
			ARENFORCE_MSG( components > 0, "Don't have anything to create here" );

			// If we have two components, attach them with a muxer
			if ( components == 2 )
				stack << "filter:muxer";

			// And attach this to our input
			return stack.pop( );
		}

	private:
		pl::pcos::property prop_audio_;
		pl::pcos::property prop_image_;
		pl::pcos::property prop_fps_;
};
