// Smart Filters
//
// Copyright (C) 2020 Charles Yates
// Released under the LGPL.
//
// #filter:smart_repair
//
// Attempts to populate fetched frame with the last image and alpha obtained
// if the incoming frame defines neither. This allows frame rate filters to
// output generated frames with null images, such that a downstream rescale
// filter will only scale unique images:
//
// smart_pusher: fps=50:1 image=640x360
// filter:smart_rescale
// filter:distributor threads=8 insert_sequential=1
// filter:smart_repair
//
// Note that this filter cannot be distributed and must read sequntially from
// upstream.

class filter_smart_repair : public ml::filter_simple
{
	public:
		filter_smart_repair( )
		{ }

		virtual ~filter_smart_repair( )
		{ }

		virtual ml::distributable_status is_distributable( ) { return ml::distributable_no; }

		virtual bool is_seekable( ) const { return false; }

		virtual std::wstring get_uri( ) const { return L"smart_repair"; }

	protected:
		ml::fetch_status do_fetch( ml::frame_type_ptr &result )
		{
			AR_TRY_FETCH( fetch_from_slot( result ) );

			if ( result->has_image( ) )
			{
				last_usable_frame_ = result->shallow( );
			}
			else
			{
				if ( last_usable_frame_ && last_usable_frame_->has_image( ) )
					result->set_image( last_usable_frame_->get_image( )->shallow( ) );
				if ( last_usable_frame_ && last_usable_frame_->has_alpha( ) )
					result->set_alpha( last_usable_frame_->get_alpha( )->shallow( ) );
			}

			return ml::fetch_ok;
		}

	private:
		ml::frame_type_ptr last_usable_frame_;
};

