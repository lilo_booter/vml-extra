// Smart Filters
//
// Copyright (C) 2018 Charles Yates
// Released under the LGPL.
//
// #filter:smart_rescale
//
// Provides a simplified wrapper for filter:swscale.
//
// <input>
// filter:smart_rescale image=640x360
//
// Would provide a 640x360 with square samples of type yuv420p.
//
// General form of the image property is:
//
// image=WxH[@N:D[:pf]]
//
// where N and D default to 1 and pf defaults to yuv420p.

class ML_PLUGIN_DECLSPEC filter_smart_rescale : public filter_simple
{
	public:
		filter_smart_rescale( )
		: prop_image_( key_image )
		, ro_( new ml::image::rescale_object( ) )
		{
			properties( ).append( prop_image_ = std::wstring( L"" ) );
		}

		virtual std::wstring get_uri( ) const { return L"smart_rescale"; }

		virtual ml::distributable_status is_distributable( ) { return ml::distributable_should; }

	protected:
		virtual ml::fetch_status do_fetch( ml::frame_type_ptr &result )
		{
			ml::fetch_status status = ml::fetch_ok;
			AR_TRY_FETCH( fetch_from_slot( result ) );

			if ( result->has_image( ) )
			{
				// Check if we know what we're doing
				initialise( );

				if ( !is_match( result, shape_ ) )
				{
					// Annoying - shape correctly handles all changing input values apart from sar
					shape_.src_sar_num = result->get_sar_num( );
					shape_.src_sar_den = result->get_sar_den( );

					// Rescale the image
					result = frame_rescale( ro_, result, shape_ );
				}
			}

			return status;
		}

	private:
		// Determint the shape settings
		void initialise( )
		{
			if ( initialised == false )
			{
				const std::wstring prop_value = prop_image_.value< std::wstring >( );
				const std::wstring image = prop_value == L"" ? find_image_spec( fetch_slot( ) ) : prop_value;
				shape_ = convert( smart_image( image ) );
				initialised = true;
			}
		}

		// We'll seek up stream for an 'image' property rather than forcing duplication on
		// graph authoring process.
		std::wstring find_image_spec( const ml::input_type_ptr &input ) const
		{
			std::wstring result;
			auto prop = input->properties( ).get_property_with_key( key_image );
			if ( prop.valid( ) && prop.is_a< std::wstring >( ) )
				result = prop.value< std::wstring >( );
			for( size_t i = 0; result == L"" && i < input->slot_count( ); i ++ )
				result = find_image_spec( input->fetch_slot( i ) );
			return result;
		}

		bool is_match( const ml::frame_type_ptr &frame, ml::image::geometry &shape ) const
		{
			// Check image dimensions and picture format equivalence
			bool result =
				frame->width( ) == shape.width &&
				frame->height( ) == shape.height &&
				frame->get_sar_num( ) == shape.sar_num &&
				frame->get_sar_den( ) == shape.sar_den &&
				ml::image::string_to_MLPF( frame->pf( ) ) == shape.pf;

			// If the image matched so far, but we want a progressive image and our input is interlaced,
			// then we will scale here. If our output should be interlaced and our input is progressive
			// but otherwise matches, then it is fine to do nothing (assuming upstream frame rate regulation
			// and field handing).
			if ( result && shape.field_order == ml::image::progressive && frame->field_order( ) != ml::image::progressive )
				result = false;

			return result;
		}

		const pl::pcos::key key_image = pl::pcos::key::from_string( "image" );
		pl::pcos::property prop_image_;
		bool initialised = false;
		ml::rescale_object_ptr ro_;
		ml::image::geometry shape_;
};
