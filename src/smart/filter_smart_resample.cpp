// Smart Filters
//
// Copyright (C) 2018 Charles Yates
// Released under the LGPL.
//
// #filter:smart_resample
//
// Ensures that all audio obtained has a fixed frequency, channel count and
// audio format.
//
// <input>
// filter:smart_resample audio=48000@2
//
// Would provide an output of stereo 48khz regardless of whether audio is 
// present in the input.
//
// General form of the audio property is:
//
// audio=frequency@channels[:audio_format]
//
// where audio_format defaults to pcm16.

class ML_PLUGIN_DECLSPEC filter_smart_resample : public filter_smart
{
	public:
		filter_smart_resample( )
		: prop_audio_( pl::pcos::key::from_string( "audio" ) )
		{
			properties( ).append( prop_audio_ = std::wstring( L"" ) );
			follow< filter_smart >( prop_audio_, this, &filter_smart::detach );
		}

		std::wstring get_uri( ) const { return L"smart_resample"; }

		ml::distributable_status is_distributable( ) { return ml::distributable_no; }

	protected:
		ml::input_type_ptr create_graph( )
		{
			ARENFORCE_MSG( prop_audio_.value< std::wstring >( ) != L"", "filter:smart_resample needs an audio assignment" );

			// Determine frequency, channels and af from resample property
			smart_audio audio( prop_audio_.value< std::wstring >( ) );

			// This is the input we will connect to
			ml::input_type_ptr input = fetch_slot( );

			// In case we receive no audio, conform here
			SMART_DEBUG( SMART_ID << " creating filter:conform image=-1 audio=1 frequency=" << audio.frequency << " channels=" << audio.channels );
			ml::filter_type_ptr conform = create_filter( L"conform" );
			conform->property( "image" ) = -1;
			conform->property( "audio" ) = 1;
			conform->property( "frequency" ) = audio.frequency;
			conform->property( "channels" ) = audio.channels;
			conform->connect( input );

			// Create the resampler
			SMART_DEBUG( SMART_ID << " creating filter:resampler frequency=" << audio.frequency << " channels=" << audio.channels << " af=" << cls::to_string( audio.af ) );
			ml::filter_type_ptr resampler = create_filter( L"resampler" );
			resampler->property( "frequency" ) = audio.frequency;
			resampler->property( "channels" ) = audio.channels;
			resampler->property( "af" ) = audio.af;
			resampler->connect( conform );

			// Attach it to the the smart filter
			return resampler;
		}

	private:
		pl::pcos::property prop_audio_;
};
