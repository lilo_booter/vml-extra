// Smart Filters
//
// Copyright (C) 2018 Charles Yates
// Released under the LGPL.
//
// GENERAL INFORMATION:
//
// The filter:smart_ family are designed to provide simpler interfaces and more
// efficient use of resources.
//
// For example:
//
// <input>
// filter:smart_fps fps=25:1
//
// Would insert a filter:frame_rate fps_num=25 fps_den=1 in the graph, but only
// if the input provides a framerate other that 25:1.
//
// Rationale:
//
// I thought it was funny.

#pragma once

#include <opencorelib/cl/core.hpp>
#include <opencorelib/cl/enforce_defines.hpp>
#include <openpluginlib/pl/pcos/observer.hpp>
#include <openmedialib/ml/openmedialib_plugin.hpp>
#include <openmedialib/ml/stack.hpp>

#include <ml/stack_operators.hpp>

namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;
namespace ml = olib::openmedialib::ml;
namespace cls = olib::opencorelib::str_util;

namespace olib { namespace openmedialib { namespace ml { namespace smart {

typedef boost::format bsf;
typedef boost::wformat bwf;

// pcos property observer
template < typename C > 
class fn_observer : public pl::pcos::observer
{
	public:
		fn_observer( C *instance, void ( C::*fn )( ) )
		: instance_( instance )
		, fn_( fn )
		{ }

		virtual void updated( pl::pcos::isubject * )
		{ ( instance_->*fn_ )( ); }

	private:
		C *instance_;
		void ( C::*fn_ )( );
};

struct property_compare
{
   bool operator( ) ( const pl::pcos::property &lhs, const pl::pcos::property &rhs ) const
   {
	   return lhs.get_key( ) < rhs.get_key( );
   }
};

struct head_type
{
	head_type( )
	: assigned( false )
	{ }

	head_type( const ml::input_type_ptr &input_ )
	: assigned( true )
	, input( input_ )
	{ }

	bool assigned;
	ml::input_type_ptr input;
};

#ifdef WIN32
#define __FILE_SEP__					'\\'
#else
#define __FILE_SEP__					'/'
#endif

#define __FILENAME__ 					( strrchr( __FILE__, __FILE_SEP__ ) ? strrchr( __FILE__, __FILE_SEP__ ) + 1 : __FILE__ )
#define __DEBUG_INFO__					" <" << __FILENAME__ << ":" << __LINE__ << ">"
#define SMART_DEBUG( msg ) 				do { if ( debug_level( ) ) std::cerr << msg << __DEBUG_INFO__ << std::endl; } while( false )
#define SMART_DEBUG_LEVEL( n, msg ) 	do { if ( debug_level( ) >= n ) std::cerr << msg << __DEBUG_INFO__ << std::endl; } while( false )
#define SMART_ID						cls::to_string( get_uri( ) )

class ML_PLUGIN_DECLSPEC follower
{
	protected:
		// Follower logic
		typedef std::shared_ptr< pl::pcos::observer > observer_ptr;
		typedef std::map< pl::pcos::property, observer_ptr, property_compare > following_type;

		template < typename C >
		void follow( pl::pcos::property &property, C *instance, void ( C::*follower )( ) )
		{
			observer_ptr observer( new fn_observer< C >( const_cast< C * >( instance ), follower ) );
			following_[ property ] = observer;
			property.attach( observer );
		}

	private:
		following_type following_;
};

class ML_PLUGIN_DECLSPEC describe_type
{
	public:
		std::string describe( const ml::input_type_ptr &graph ) const
		{
			std::string result;
			if ( !!graph )
			{
				ml::filter_type_ptr aml = ml::create_filter( L"aml" );
				aml->property( "filename" ) = std::wstring( L"@" );
				aml->connect( graph );
				result = aml->property( "stdout" ).value< std::string >( );
			}
			return result;
		}
};

class ML_PLUGIN_DECLSPEC filter_smart : public ml::filter_simple, protected follower, protected describe_type
{
	public:
		typedef std::vector< ml::input_type_ptr > slots_type;

		virtual ~filter_smart( ) { }

		// Optimistically, we'll make all smart filters distributable by default
		virtual ml::distributable_status is_distributable( ) { return ml::distributable_should; }

		// If we have sync'd, we should know the number of frames avaialble, otherwise 
		// optimistically assume we have at least 1 frame on the input (if this is wrong
		// it'll fail at the point of the sync)
		virtual int get_frames( ) const
		{
			return attached( ) ? head( )->get_frames( ) : 1;
		}

		// Keep track of all upstream graphs (this can of course drift if upstream 
		// changes in a manner which we aren't directly connected to - amldaemon provides
		// a funky work around by reconnecting each downstream filter to force this)
		virtual void after_slot_change( int slot )
		{
			ml::input_type_ptr input = fetch_slot( slot );

			// Get a full graph dump of input
			std::string graph = describe( input );

			// If it's the same as before, do nothing, otherwise reset
			if ( graph != upstream_[ slot ] )
			{
				if ( !!input )
					SMART_DEBUG_LEVEL( 10, SMART_ID << " slot " << slot << " now attached to " << cls::to_string( input->get_uri( ) ) );
				else
					SMART_DEBUG_LEVEL( 10, SMART_ID << " slot " << slot << " connected to nothing" );

				head_ = head_type( );
				upstream_[ slot ] = graph;
			}
		}

		// Provides a means for smart filter instances to force a rebuild of the graph
		// (this is typically used by followed properties and activated when their values
		// are changed)
		void detach( )
		{
			SMART_DEBUG_LEVEL( 10, SMART_ID << " detaching" );
			head_ = head_type( );
		}

		// Provides a means for instances to recover the current graph (thus allowing
		// followed properties to modify the existing graph rather than forcing a 
		// recreation)
		inline ml::input_type_ptr attachment( ) const
		{
			return head_.input;
		}

	protected:
		// Fetch a frame from the input
		ml::fetch_status do_fetch( frame_type_ptr &frame )
		{
			if ( !attached( ) ) attach( create_graph( ) );
			return head( )->fetch( frame, get_position( ) );
		}

		// Create the graph if necessary and sync the subgraph
		void sync_frames( )
		{
			if ( !attached( ) ) attach( create_graph( ) );
			head( )->sync( fetch_slot( ) );
		}

		// Default create_graph - attempts to fetch a frame from upstream (first slot 
		// only) which is passed to the following virtual method. If no frame is required
		// or the first slot is unsifficient, implementations can override this method 
		// instead.
		virtual ml::input_type_ptr create_graph( )
		{
			// Ensure we have a valid input of some sort
			if ( !fetch_slot( ) ) return ml::input_type_ptr( );
			if ( fetch_slot( )->get_frames( ) <= 0 ) fetch_slot( )->sync( );
			if ( fetch_slot( )->get_frames( ) <= 0 ) return ml::input_type_ptr( );

			// Fetch a frame and create the graph
			ml::frame_type_ptr frame;
			ARENFORCE_MSG( fetch_from_slot( frame ) == fetch_ok, "Unable to obtain frame" );
			return create_graph( frame );
		}

		// See create_graph above - override one of the two methods and return the graph
		// to read from.
		virtual ml::input_type_ptr create_graph( const frame_type_ptr &frame )
		{
			ARENFORCE_MSG( false, "%s has no ability to create a graph" )( SMART_ID );
			return ml::input_type_ptr( );
		}

		slots_type fetch_slots( ) const
		{
			slots_type slots;
			ml::stack stack;

			for ( size_t slot = 0; slot < slot_count( ); slot ++ )
				fetch_slots( stack, fetch_slot( int( slot ) ) );

			int depth;
			stack << "depth?" >> depth;

			for ( int index = depth - 1; index >= 0; index -- )
				slots.push_back( ( stack << index << "roll" ).pop( ) );

			return slots;
		}

	private:
		// Determine if the input is a playlist or not
		inline bool is_playlist( const ml::input_type_ptr &input ) const
		{
			return !!input && ( input->get_uri( ) == L"playlist" || input->get_uri( ) == L"smart_playlist" );
		}

		inline bool is_string( const ml::input_type_ptr &input ) const
		{
			return !!input && input->get_frames( ) <= 0;
		}

		void fetch_slots( ml::stack &stack, const ml::input_type_ptr &input ) const
		{
			if ( is_playlist( input ) )
				for( size_t slot = 0; slot < input->slot_count( ); slot ++ )
					fetch_slots( stack, input->fetch_slot( slot ) );
			else if ( is_string( input ) )
				stack.command( input );
			else if ( !!input )
				stack << input;
		}

		// Return the internal graph or the first slot if none has been specified
		inline ml::input_type_ptr head( ) const
		{
			ml::input_type_ptr possible = attachment( );
			return !!possible ? possible : fetch_slot( );
		}

		// Determine if we have attached an internal graph or not
		inline bool attached( ) const
		{
			return head_.assigned;
		}

		// Attach the internal graph
		void attach( const ml::input_type_ptr &input ) const
		{
			// Give some info as to what is going on
			if ( !!input )
				SMART_DEBUG_LEVEL( 10, SMART_ID << " attaching to " << cls::to_string( input->get_uri( ) ) );
			else
				SMART_DEBUG_LEVEL( 10, SMART_ID << " attaching to nothing" );

			// If we have no input specified, default to incoming slot
			if ( !!input )
				head_ = head_type( input );
			else
				head_ = head_type( fetch_slot( ) );

			// Overkill debugging
			SMART_DEBUG_LEVEL( 20, SMART_ID << " full graph is " << boost::replace_all_copy( describe( head( ) ), "\n", " " ) );
		}

		mutable head_type head_;
		std::map< int, std::string > upstream_;
};

class ML_PLUGIN_DECLSPEC input_smart : public ml::input_type, protected follower, protected describe_type
{
	public:
		virtual ~input_smart( ) { }

		// If we have sync'd, we should know the number of frames avaialble, otherwise 
		// optimistically assume we have at least 1 frame on the input (if this is wrong
		// it'll fail at the point of the sync)
		virtual int get_frames( ) const
		{
			return attached( ) ? head( )->get_frames( ) : 0;
		}

		// Provides a means for smart filter instances to force a rebuild of the graph
		// (this is typically used by followed properties and activated when their values
		// are changed)
		void detach( )
		{
			SMART_DEBUG_LEVEL( 10, SMART_ID << " detaching" );
			head_ = head_type( );
		}

		// Provides a means for instances to recover the current graph (thus allowing
		// followed properties to modify the existing graph rather than forcing a 
		// recreation)
		inline ml::input_type_ptr attachment( ) const
		{
			return head_.input;
		}

	protected:
		// Fetch a frame from the input
		ml::fetch_status do_fetch( frame_type_ptr &frame )
		{
			if ( !attached( ) ) attach( create_graph( ) );
			head( )->seek( get_position( ) );
			return head( )->fetch( frame );
		}

		// Create the graph if necessary and sync the subgraph
		void sync_frames( )
		{
			if ( !attached( ) ) attach( create_graph( ) );
			head( )->sync( );
		}

		// Virtual method to construct the graph
		virtual ml::input_type_ptr create_graph( )
		{
			ARENFORCE_MSG( false, "%s has no ability to create a graph" )( SMART_ID );
			return ml::input_type_ptr( );
		}

	private:
		// Return the internal graph or the first slot if none has been specified
		inline ml::input_type_ptr head( ) const
		{
			ml::input_type_ptr possible = attachment( );
			return attachment( );
		}

		// Determine if we have attached an internal graph or not
		inline bool attached( ) const
		{
			return head_.assigned;
		}

		// Attach the internal graph
		void attach( const ml::input_type_ptr &input ) const
		{
			// Give some info as to what is going on
			if ( !!input )
				SMART_DEBUG_LEVEL( 10, SMART_ID << " attaching to " << cls::to_string( input->get_uri( ) ) );
			else
				SMART_DEBUG_LEVEL( 10, SMART_ID << " attaching to nothing" );

			// If we have no input specified, default to incoming slot
			if ( !!input )
				head_ = head_type( input );

			// Overkill debugging
			SMART_DEBUG_LEVEL( 20, SMART_ID << " full graph is " << boost::replace_all_copy( describe( head( ) ), "\n", " " ) );
		}

		mutable head_type head_;
		std::map< int, std::string > upstream_;
};

} } } }
