// Smart Puller
//
// Copyright (C) 2020 Charles Yates
// Released under the LGPL.
//
// #store:smart_puller:
//
// Provides a thread safe pulling store which should be used with a
// input:smart_pusher: input.

class store_smart_puller : public ml::store_type
{
	public:
		store_smart_puller( )
		: prop_input_( pl::pcos::key::from_string( "graph" ) )
		{
			properties( ).append( prop_input_ = ml::input_type_ptr( ) );
		}

		virtual ~store_smart_puller( )
		{
			if ( pusher_ )
				pusher_->properties( ).get_property_with_string( "done" ) = 1;
			if ( thread.joinable( ) ) thread.join( );
		}

		virtual bool init( )
		{
			ml::input_type_ptr input = prop_input_.value< ml::input_type_ptr >( );
			ARENFORCE_MSG( !!input, "No input graph provided" );

			ml::input_type_ptr &search = input;
			while( !pusher_ && !!search )
			{
				if ( search->get_uri( ) == L"smart_pusher:" )
					pusher_ = search;
				else
					search = search->fetch_slot( );
			}

			ARENFORCE_MSG( !!pusher_, "No smart_pusher: found in provided graph" );

			thread = std::thread( [ this ]( ) { run( ); } );

			return true;
		}

		virtual bool push( frame_type_ptr frame )
		{
			return !terminated_ ? pusher_->push( std::move( frame ) ) : false;
		}

		virtual void complete( )
		{
			pusher_->push( ml::frame_type_ptr( ) );
		}

	private:
		void run( )
		{
			std::unique_lock< std::mutex > lock( mutex );

			ml::input_type_ptr input = prop_input_.value< ml::input_type_ptr >( );
			input->sync( );

			int position = 0;

			do
			{
				ml::frame_type_ptr frame;
				ml::fetch_status status = input->fetch( frame, position ++ );
				if ( status != ml::fetch_ok || !frame || frame->in_error( ) ) terminated_ = true;
				condition.notify_one( );
			}
			while( !terminated_ );

			pusher_->properties( ).get_property_with_string( "done" ) = 1;
		}

		pl::pcos::property prop_input_;

		std::thread thread;
		std::mutex mutex;
		std::condition_variable condition;

		ml::input_type_ptr pusher_;

		bool terminated_ = false;
};
