// Smart Filters
//
// Copyright (C) 2018 Charles Yates
// Released under the LGPL.
//
// #filter:smart_timeline
//
// Provides a custom graph for playing multiple inputs in a timeline with
// custom transitions.
//
// Largely wraps filter:compositor.
//
// <input1>
// <input2>
// filter:smart_timeline slots=2 transition=1s
//
// Would play input1 and input2 sequentially.

class ML_PLUGIN_DECLSPEC filter_smart_timeline : public filter_smart
{
	public:
		filter_smart_timeline( )
		: prop_slots_( pl::pcos::key::from_string( "slots" ) )
		, prop_decode_( pl::pcos::key::from_string( "decode" ) )
		, prop_threads_( pl::pcos::key::from_string( "threads" ) )
		, prop_audio_( pl::pcos::key::from_string( "audio" ) )
		, prop_image_( pl::pcos::key::from_string( "image" ) )
		, prop_fps_( pl::pcos::key::from_string( "fps" ) )
		, prop_transition_( pl::pcos::key::from_string( "transition" ) )
		{   
			properties( ).append( prop_slots_ = 1 );
			properties( ).append( prop_decode_ = 0 );
			properties( ).append( prop_threads_ = 4 );
			properties( ).append( prop_audio_ = std::wstring( L"48000@2" ) );
			properties( ).append( prop_image_ = std::wstring( L"1920x1080" ) );
			properties( ).append( prop_fps_ = std::wstring( L"25:1" ) );
			properties( ).append( prop_transition_ = std::wstring( L"1s" ) );
			follow< filter_smart >( prop_slots_, this, &filter_smart::detach );
			follow< filter_smart >( prop_audio_, this, &filter_smart::detach );
			follow< filter_smart >( prop_image_, this, &filter_smart::detach );
			follow< filter_smart >( prop_fps_, this, &filter_smart::detach );
			follow< filter_smart >( prop_transition_, this, &filter_smart::detach );
		}

		virtual std::wstring get_uri( ) const { return L"smart_timeline"; }

		virtual size_t slot_count( ) const { return size_t( prop_slots_.value< int >( ) ); }

	protected:
		virtual ml::input_type_ptr create_graph( )
		{
			// Unpack all the inputs
			slots_type slots = fetch_slots( );
			const size_t count = slots.size( );

			// Extract the properties
			const int decode = prop_decode_.value< int >( );
			const int threads = prop_threads_.value< int >( );

			// Parse the properties - invalid inputs will be rejected here
			const smart_fps fps( prop_fps_.value< std::wstring >( ) );
			const smart_audio audio( prop_audio_.value< std::wstring >( ) );

			// Default duration of transition
			const smart_time transition( prop_transition_.value< std::wstring >( ), fps );

			// We want an opaque output here
			smart_image image( prop_image_.value< std::wstring >( ) );
			image.a = 255;

			// We'll use a stack to simplify the graph building
			ml::stack stack;

			// Push the background
			stack 
			<< input::background( image, audio, fps )
			;

			// Track the offset
			int offset = 0;

			// Handle each slot by creating a graph for each
			for ( size_t slot = 0; slot < slots.size( ); slot ++ )
			{
				int clip_length = 0;

				stack 
				<< slots[ slot ]
				<< filter::decode( decode, threads )
				<< filter::deinterlace( image )
				<< filter::fps( fps )
				<< filter::visualise( )
				<< filter::resample( audio )
				<< query::get_frames( )
				>> clip_length
				<< filter::mix( 0, 1, transition.value )
				<< filter::offset( offset )
				<< filter::locked_audio( audio, 0 )
				;

				offset += clip_length - transition.value;
			}

			// Composite all of them together (with the background)
			stack 
			<< filter::compositor( static_cast< int >( count ) + 1 )
			;

			return stack.pop( );
		}

	private:
		pl::pcos::property prop_slots_;
		pl::pcos::property prop_decode_;
		pl::pcos::property prop_threads_;
		pl::pcos::property prop_audio_;
		pl::pcos::property prop_image_;
		pl::pcos::property prop_fps_;
		pl::pcos::property prop_transition_;
};

