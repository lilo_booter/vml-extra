// Smart Pusher
//
// Copyright (C) 2020 Charles Yates
// Released under the LGPL.
//
// #input:smart_pusher:
//
// Provides a thread safe pusher which can be used with a smart_puller: store.
//
// The general usage is as follows:
//
// Start by constructing your smart_pusher: graph which has a form of:
//
// smart_pusher: fps=<fps> <scaling-filters> <encode-filters> <store-filters>
//
// For example, the following graph will produce an mp4 of 640x360 at 25fps for
// any sequence of progressive video frames you push at it:
//
// smart_pusher: fps=25:1 image=640x360 audio=48000:2:pcm16
// filter:smart_rescale
// filter:distributor threads=8 insert_sequential=1
// filter:smart_repair
// filter:resampler frequency=48000 channel=2
// filter:avencode_lgop video="codec=libx264,g=25,aml_codec_id=h264/h264"
// filter:avencode_audio audio="codec=aac"
// filter:store store="avmux:out.mp4"
//
// Note that the smart_pusher: doesn't scale the images directly - instead, this
// is done by the filter:smart_rescale (which, by default, will look upstream for
// the image property assigned to the smart_pusher:).
//
// To avoid rescaling images which have been duplicated in the frame rate conversion,
// smart_pusher: will provide those frame with null images. These will be passed
// through the rescale and distributor, and the filter:repair will track the last
// image carrying frame and populate the missing images as required.
//
// NOTE: Do not sync or fetch from this graph directly as it will block until frames
// have been pushed into it.
//
// Instead, create a smart_puller: store and assign the created graph to its
// graph property:
//
// auto store = ml::create_store( "smart_puller:", ml::frame_type_ptr( ) );
// store.property( "graph ) = graph;
//
// Then you can use it as a normal store:
//
// ml::input_type_ptr input = ...;
// input->sync( );
// for ( int i = 0; i < input->get_frames( ); i ++ )
// {
//     ml::frame_type_ptr frame;
//     if ( input->fetch( frame, i ) != ml::fetch_ok ) break;
//     store->push( frame );
// }
// store->complete( );
//
// IMAGE SCALING:
//
// There are two conflicting considerations concerning image scaling:
//
// * if you push a single image at, say, 1 fps, that image will be repeated for
//   each frame (ie: 25 times in the example above). If the dimensions/pf don't
//   match the swscale, then it will get rescaled 25 times. To avoid this, we
//   could scale the image in the input graph.
// * if a drop in fps for video is triggered, the demonstrated approach above will
//   provide a net gain as we won't scale the dropped frames, whereas scaling it in
//   the input graph will provide a net loss
//
// We have resolved the issue in this mechanism by simply avoiding duplication of
// images until after the scaling process (using null images to denote the
// duplication). This allows efficient rescaling to be carried out in the pusher's
// subgraph.
//
// AUDIO SAMPLING:
//
// Additionally, audio has a similar issue, but in this case, it's only partially
// solved by the pusher:
//
// * by default, all audio must be of the same frequency and channel count as the
//   first frame. Changes to frequency will cause an exception, though there will be a
//   mix down or up to match channels, and audio format is similarly changed as
//   required (pcm16, pcm24, float etc).
//
// This conforming to the first frame isn't ideal, and we can overried that using the
// audio property:
//
// smart_pusher: fps=25:1 image=640x360@1:1 audio=48000:2:float
//
// Changes to frequency will still be rejected, so the mininimum requirement for the
// input graph is to resample if mixed frequency inputs are involved.
//
// INTERLACE:
//
// Interlaced material can be pushed into the pusher with very few considerations.
//
// The pusher will correctly handle pitch shifting, pausing, switching direction of
// play, changing resolution, frame rates and field order.
//
// Since scaling is downstream of the pusher, a filter:interlace will suffice for the
// general case of 50p/59.94p to 50i/59.94i conversions.
//
// TODO:
//
// * Interlace passthrough/generation
// * Audio resampling
// * Various optimisations
// * Provide a means to use this in a filter

class ML_PLUGIN_DECLSPEC input_smart_pusher : public ml::input_type
{
	public:
		typedef std::shared_ptr < ml::fps_regulate > fps_regulate_ptr;

		input_smart_pusher( )
		: prop_fps_( pl::pcos::key::from_string( "fps" ) )
		, prop_image_( pl::pcos::key::from_string( "image" ) )
		, prop_audio_( pl::pcos::key::from_string( "audio" ) )
		, prop_deinterlace_( pl::pcos::key::from_string( "deinterlace" ) )
		, prop_done_( pl::pcos::key::from_string( "done" ) )
		, obs_done_( new fn_observer< input_smart_pusher >( const_cast< input_smart_pusher * >( this ), &input_smart_pusher::done ) )
		{
			properties( ).append( prop_fps_ = std::wstring( L"" ) );
			properties( ).append( prop_image_ = std::wstring( L"" ) );
			properties( ).append( prop_audio_ = std::wstring( L"" ) );
			properties( ).append( prop_deinterlace_ = std::wstring( L"auto" ) );
			properties( ).append( prop_done_ = 0 );
			prop_done_.attach( obs_done_ );
			prop_done_.set_always_notify( true );
		}

		virtual ~input_smart_pusher( )
		{
			deinterlacer_.flush( );
		}

		// Basic information
		virtual std::wstring get_uri( ) const { return L"smart_pusher:"; }
		virtual int get_frames( ) const { return INT_MAX; }
		virtual bool is_seekable( ) const { return false; }
		virtual bool determine_complete( ) const { return false; }

		virtual bool initialize( )
		{
			// Parse properties
			fps_ = parse< smart_fps >( prop_fps_.value< std::wstring >( ), fps_ );
			image_ = parse< smart_image >( prop_image_.value< std::wstring >( ), image_ );
			audio_ = parse< smart_audio >( prop_audio_.value< std::wstring >( ), audio_ );

			// Create the regulator
			regulator_ = fps_regulate_ptr( new ml::fps_regulate( fps_.num, fps_.den, debug_level( ) ) );
			regulator_->set_shape( convert( image_ ) );

			// Setting 0 here will disable audio altogether
			if ( audio_.frequency != 0 && audio_.channels != 0 )
				regulator_->set_audio( audio_.frequency, audio_.channels, ml::audio::af_to_id( cls::to_t_string( audio_.af ) ) );

			// Handle the deinterlace property
			const auto deinterlace = prop_deinterlace_.value< std::wstring >( );
			const bool is_auto = deinterlace == L"auto";
			const double fps = ( double( fps_.num ) / fps_.den );
			const int pixels = image_.width * image_.height;
			if ( deinterlace == L"off" )
				deinterlacer_.set_type( regulate::off );
			else if ( is_auto && pixels == 0 )
				deinterlacer_.set_type( regulate::off );
			else if ( deinterlace == L"field" )
				deinterlacer_.set_type( regulate::field );
			else if ( deinterlace == L"frame" )
				deinterlacer_.set_type( regulate::frame );
			else if ( deinterlace == L"fast-frame" )
				deinterlacer_.set_type( regulate::fast_frame );
			else if ( is_auto && fps <= 30.0 && pixels <= 640 * 480  )
				deinterlacer_.set_type( regulate::fast_frame );
			else if ( is_auto && fps <= 30.0 )
				deinterlacer_.set_type( regulate::frame );
			else if ( is_auto )
				deinterlacer_.set_type( regulate::field );
			else
				ARENFORCE_MSG( false, "Unrecognised deinterlace setting of %s" )( deinterlace );

			// If the deinterlacer is active and our eventual output is interlaced, we can
			// let it know precisely what we are going to output now to avoid unnecessary
			// processing ie: if the pusher's image type is interlaced of a specific field
			// order, dimension and fps, do nothing
			if ( deinterlacer_.get_type( ) != regulate::off )
				deinterlacer_.set_target( fps_.num, fps_.den, convert( image_ ) );

			return true;
		}

		// Push method
		virtual bool push( ml::frame_type_ptr frame )
		{
			// FIXME: We probably don't need the lock all the time here - perhaps
			// switching to atomic values for the eof and error bools will help
			std::unique_lock< std::mutex > lock( mutex );

			try
			{
				// We'll block here if there any other pending frames
				while ( !eof_ && !error_ && regulator_->has_frame( ) )
					condition.wait( lock );

				if ( deinterlacer_.get_type( ) != regulate::off )
				{
					// Prepare the frame and queue the audio
					if ( frame )
					{
						frame = regulator_->prepare( frame );
						regulator_->push_audio( frame );
					}
					else
					{
						eof_ = true;
					}

					// Give the frame to the deinterlacer (null frame causes a flush)
					deinterlacer_.push( boost::move( frame ) );

					// Wait until we have at least one frame here
					deinterlacer_.wait( );

					// We'll now retrieve all pending frames and forward to regulator
					while( !deinterlacer_.empty( ) )
					{
						// Move deinterlaced frames to the regulator
						regulator_->push_video( deinterlacer_.fetch( ) );

						// Let another thread know of the change in state
						condition.notify_one( );
					}
				}
				else if ( !eof_ && !error_ && frame )
				{
					// Prepare the frame and queue the audio
					frame = regulator_->prepare( frame );
					regulator_->push_audio( frame );
					regulator_->push_video( frame );
				}
				else if ( !frame )
				{
					// A null frame is received at eof
					eof_ = !frame;
				}
			}
			catch( ... )
			{
				// If regulate throws, we'll put ourselves in an error state
				error_ = true;
				condition.notify_all( );
				throw;
			}

			// State as probably changed
			condition.notify_one( );

			// Indicates if additional frames can be pushed
			return !eof_ && !error_;
		}

		void done( )
		{
			std::unique_lock< std::mutex > lock( mutex );

			// This is invoked by the done property observer and can be used by
			// the puller store in case of errors occuring downstream
			if ( prop_done_.value< int >( ) > 1 )
				error_ = true;
			else
				eof_ = true;

			deinterlacer_.flush( );

			condition.notify_one( );
		}

	protected:
		ml::fetch_status do_fetch( frame_type_ptr &result )
		{
			std::unique_lock< std::mutex > lock( mutex );

			ml::fetch_status status = ml::fetch_ok;

			// Provide last frame if required (only kept if previous fetch was ok)
			if( last_frame_ && last_frame_->get_position( ) == get_position( ) )
			{
				result = last_frame_->shallow( );
				return ml::fetch_ok;
			}

			// Wait until we're either in error or we have a full frame to fetch
			while ( !eof_ && !error_ && !regulator_->has_frame( ) )
				condition.wait( lock );

			// Fetch from the regulator
			if ( !error_ )
			{
				// If we know we're at eof and we don't have a full frame pending,
				// force the fetch now - this will pad audio with silence if necessary
				// or just return a null frame if there really is nothing there
				bool force = eof_ && !regulator_->has_frame( );
				result = regulator_->pop( force );

				// If we have no result and eof is set, indicate eof, otherwise indicate error
				if ( !result && eof_ )
					status = ml::fetch_eof;
				else if ( !result )
					status = ml::fetch_error;

				// Hold the last_frame if status is ok
				if ( status == ml::fetch_ok )
					last_frame_ = result->shallow( );
				else
					last_frame_.reset( );

				// More sanity tests
				if ( status == ml::fetch_ok )
					verify( result );
			}
			else
			{
				status = ml::fetch_error;
			}

			// If we're in error on this frame, we're done
			if ( status == ml::fetch_error )
				error_ = true;

			// Signal the condition to unblock the push
			condition.notify_one( );

			return status;
		}

	private:
		template < typename C >
		C parse( const std::wstring &input, const C &fallback )
		{
			return input != L"" ? C( input ) : fallback;
		}

		inline void verify( const ml::frame_type_ptr &result )
		{
			try
			{
				ARENFORCE_MSG( result, "Fetch status is ok, but no frame is provied for %d" )( get_position( ) );
				ARENFORCE_MSG( result->get_position( ) == get_position( ), "Did not receive frame %d from regulator" )( get_position( ) );
			}
			catch( ... )
			{
				error_ = true;
				throw;
			}
		}

		std::mutex mutex;
		std::condition_variable condition;

		bool eof_ = false;
		bool error_ = false;

		pl::pcos::property prop_fps_;
		pl::pcos::property prop_image_;
		pl::pcos::property prop_audio_;
		pl::pcos::property prop_deinterlace_;
		pl::pcos::property prop_done_;
		std::shared_ptr< pl::pcos::observer > obs_done_;

		smart_fps fps_;
		smart_image image_ = smart_image( std::wstring( L"" ), false );
		smart_audio audio_ = smart_audio( std::wstring( L"" ), false );

		fps_regulate_ptr regulator_;
		field_regulate deinterlacer_;

		ml::frame_type_ptr last_frame_;
};
