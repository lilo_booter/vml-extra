// Smart Filters
//
// Copyright (C) 2018 Charles Yates
// Released under the LGPL.
//
// #filter:smart_mosaic
//
// Provides a custom graph for playing multiple inputs at the same time.
// Largely wraps filter:compositor.
//
// <input1>
// <input2>
// filter:smart_mosaic image=1920x1080 audio=48000@2 fps=25:1 slots=2
//
// Would play input1 and input2 side by side.

class arrangement
{
	public:
		virtual ~arrangement( ) { }
		virtual const smart_geometry &calculate( const size_t slot ) = 0;
};

typedef std::shared_ptr< arrangement > arrangement_ptr;

class smart_hgrid : public arrangement
{
	public:
		smart_hgrid( size_t slots )
		: slots_( slots )
		, rows_( 0 )
		, cols_( 0 )
		{
			init( rows_, cols_ );
		}

		virtual const smart_geometry &calculate( const size_t slot )
		{
			geometry.x = ( slot % rows_ ) * geometry.w;
			geometry.y = ( slot / rows_ ) * geometry.h;
			geometry.w = 1.0 / rows_;
			geometry.h = 1.0 / cols_;
			return geometry;
		}

	protected:
		void init( int &rows, int &cols )
		{
			ARENFORCE_MSG( slots_ > 0, "Cannot composite 0 inputs" );

			// Start by simply calculating the square root of the number of inputs
			int target = int( slots_ );
			double grid = std::sqrt( double( target ) );

			// Default number of rows and columns is simply the rounded value
			rows = int( grid + 0.5 );
			cols = rows;

			// Small correction to the rows if we've gone under here
			if ( cols * rows < target ) rows_ ++;

			// Obviously, if we have an exact square, we don't need to do anything else, but
			// to make better use of the space, see if we can reduce the number of columns
			while( ( cols - 1 ) * ( rows + 1 ) >= target )
			{
				rows ++;
				cols --;
			}
		}

		smart_geometry geometry;
		const size_t slots_;
		int rows_;
		int cols_;
};

class smart_vgrid : public smart_hgrid
{
	public:
		smart_vgrid( size_t slots )
		: smart_hgrid( slots )
		{
			init( cols_, rows_ );
		}
};

class ML_PLUGIN_DECLSPEC filter_smart_mosaic : public filter_smart
{
	public:
		filter_smart_mosaic( )
		: prop_slots_( pl::pcos::key::from_string( "slots" ) )
		, prop_decode_( pl::pcos::key::from_string( "decode" ) )
		, prop_threads_( pl::pcos::key::from_string( "threads" ) )
		, prop_audio_( pl::pcos::key::from_string( "audio" ) )
		, prop_image_( pl::pcos::key::from_string( "image" ) )
		, prop_fps_( pl::pcos::key::from_string( "fps" ) )
		, prop_arrangement_( pl::pcos::key::from_string( "arrangement" ) )
		{   
			properties( ).append( prop_slots_ = 1 );
			properties( ).append( prop_decode_ = 0 );
			properties( ).append( prop_threads_ = 4 );
			properties( ).append( prop_audio_ = std::wstring( L"48000@2" ) );
			properties( ).append( prop_image_ = std::wstring( L"1920x1080" ) );
			properties( ).append( prop_fps_ = std::wstring( L"25:1" ) );
			properties( ).append( prop_arrangement_ = std::wstring( L"hgrid" ) );
			follow< filter_smart >( prop_slots_, this, &filter_smart::detach );
			follow< filter_smart >( prop_audio_, this, &filter_smart::detach );
			follow< filter_smart >( prop_image_, this, &filter_smart::detach );
			follow< filter_smart >( prop_fps_, this, &filter_smart::detach );
			follow< filter_smart >( prop_arrangement_, this, &filter_smart::detach );
		}

		virtual std::wstring get_uri( ) const { return L"smart_mosaic"; }

		virtual size_t slot_count( ) const { return size_t( prop_slots_.value< int >( ) ); }

	protected:
		virtual ml::input_type_ptr create_graph( )
		{
			// Unpack all the inputs
			slots_type slots = fetch_slots( );
			const size_t count = slots.size( );

			// Extract the properties
			const int decode = prop_decode_.value< int >( );
			const int threads = prop_threads_.value< int >( );
			const std::wstring arrangement = prop_arrangement_.value< std::wstring >( );

			// Parse the properties - invalid inputs will be rejected here
			const smart_fps fps( prop_fps_.value< std::wstring >( ) );
			const smart_image image( prop_image_.value< std::wstring >( ) );
			const smart_audio audio( prop_audio_.value< std::wstring >( ) );

			// Calculate dimensions of each entry in the mosaic
			arrangement_ptr layout( obtain( count, arrangement ) );
			ARENFORCE_MSG( !!layout, "Unable to map %s to a known layout" )( arrangement );

			// We'll use a stack to simplify the graph building
			ml::stack stack;

			// Push the background
			stack 
			<< input::background( image, audio, fps )
			;

			// Handle each slot by creating a graph for each
			for ( size_t slot = 0; slot < count; slot ++ )
			{
				stack 
				<< slots[ slot ]
				<< filter::decode( decode, threads )
				<< filter::deinterlace( image )
				<< filter::fps( fps )
				<< filter::visualise( )
				<< filter::resample( audio )
				<< filter::geometry( layout->calculate( slot ) )
				<< filter::locked_audio( audio, 0 )
				;
			}

			// Composite all of them together (with the background)
			stack 
			<< filter::compositor( static_cast< int >( count ) + 1 )
			;

			return stack.pop( );
		}

		arrangement_ptr obtain( const size_t &count, const std::wstring &arrangement )
		{
			if ( arrangement == L"hgrid" )
				return arrangement_ptr( new smart_hgrid( count ) );
			else if ( arrangement == L"vgrid" )
				return arrangement_ptr( new smart_vgrid( count ) );
			return arrangement_ptr( );
		}

	private:
		pl::pcos::property prop_slots_;
		pl::pcos::property prop_decode_;
		pl::pcos::property prop_threads_;
		pl::pcos::property prop_audio_;
		pl::pcos::property prop_image_;
		pl::pcos::property prop_fps_;
		pl::pcos::property prop_arrangement_;
};

