// Smart Filters
//
// Copyright (C) 2018 Charles Yates
// Released under the LGPL.
//
// #filter:smart_muxer
//
// Provides a simplified use of the filter:muxer such that the fps of the second
// (audio) slot is automatically converted to that of the the first (video).
//
// <video> <audio> filter:smart_muxer
//
// Replaces the audio from the first graph with that of the second.

class ML_PLUGIN_DECLSPEC filter_smart_muxer : public filter_smart
{
	public:
		virtual std::wstring get_uri( ) const { return L"smart_muxer"; }

		virtual size_t slot_count( ) const { return 2; }

	protected:
		virtual ml::input_type_ptr create_graph( const ml::frame_type_ptr &frame )
		{
			// Parse frame contents
			const smart_fps fps( frame );

			// Create the graph.
			ml::stack stack;

			stack
			<< fetch_slot( 0 )
			<< fetch_slot( 1 )
			<< filter::fps( fps )
			<< "filter:muxer"
			;

			return stack.pop( );
		}
};

