typedef std::map< std::wstring, std::wstring > audio_lookup_type;

audio_lookup_type audio_lookup = boost::assign::map_list_of
	( L"48khz", L"48000" )
	( L"44.1khz", L"44100" )
;

struct smart_audio
{
	smart_audio( const std::wstring &input, bool strict = true )
	: frequency( 0 )
	, channels( 2 )
	, af( L"pcm16" )
	, profile( L"dv" )
	{
		// Determine what we should parse
		const audio_lookup_type::const_iterator iter = audio_lookup.find( input );
		const std::wstring &audio = iter != audio_lookup.end( ) ? ( *iter ).second : input;

		// Parse the audio specification
		wchar_t colon;
		std::wistringstream iss( audio );
		iss >> frequency >> colon >> channels >> colon >> af;

		// Sanity check
		if ( strict )
		{
			ARENFORCE_MSG( iss.eof( ), "Invalid audio specification '%s'" )( audio );
			ARENFORCE_MSG( frequency > 0 && channels > 0, "Invalid audio of %d:%d from %s" )( frequency )( channels )( audio );
			ml::audio::af_to_id( cls::to_t_string( af ) );
		}
	}

	smart_audio( const ml::frame_type_ptr &frame )
	{
		bool audio = frame->has_audio( );
		frequency = audio ? frame->frequency( ) : 0;
		channels = audio ? frame->channels( ) : 0;
		af = audio ? cls::to_wstring( frame->get_audio( )->af( ) ) : L"pcm16";
		profile = L"dv";
	}

	int frequency;
	int channels;
	std::wstring af;
	std::wstring profile;
};

typedef std::map< std::wstring, std::wstring > fps_lookup_type;

fps_lookup_type fps_lookup = boost::assign::map_list_of
	( L"pal", L"25" )
	( L"ntsc", L"30000:1001" )
	( L"movie", L"24000:1001" )
	( L"24p", L"24000:1001" )
	( L"50i", L"25" )
	( L"50p", L"50" )
	( L"60i", L"30000:1001" )
	( L"60p", L"60000:1001" )
;

struct smart_fps
{
	smart_fps( )
	: num( 0 )
	, den( 1 )
	{ }

	smart_fps( const std::wstring &input )
	: num( 0 )
	, den( 1 )
	{
		// Determine what we should parse
		const fps_lookup_type::const_iterator iter = fps_lookup.find( input );
		const std::wstring &fps = iter != fps_lookup.end( ) ? ( *iter ).second : input;

		// Parse the fps specification
		wchar_t colon;
		std::wistringstream iss( fps );
		iss >> num >> colon >> den;

		// Sanity check
		ARENFORCE_MSG( iss.eof( ), "Invalid fps specification '%s'" )( fps );
		ARENFORCE_MSG( num > 0 && den > 0, "Invalid fps of %d:%d" )( num )( den );
	}

	smart_fps( const ml::frame_type_ptr &frame )
	{
		frame->get_fps( num, den );
	}

	int num;
	int den;
};

struct smart_geometry
{
	smart_geometry( const std::wstring spec )
	: x( 0.0 ), y( 0.0 ), w( 0.0 ), h( 0.0 )
	{
		std::wistringstream iss( spec );
		wchar_t comma;
		iss >> x >> comma >> y >> comma >> w >> comma >> h;
		if ( h == 0.0 ) h = w;
	}

	smart_geometry( const double x_ = 0.0, const double y_ = 0.0, const double w_ = 1.0, const double h_ = 1.0 )
	: x( x_ ), y( y_ ), w( w_ ), h( h_ )
	{ }

	double x, y, w, h;
};

typedef std::map< std::wstring, std::wstring > image_lookup_type;

image_lookup_type image_lookup = boost::assign::map_list_of
	( L"2160p", L"3840x2160" )
	( L"1080i", L"1920x1080" )
	( L"1080p", L"1920x1080" )
	( L"720p", L"1280x720" )
	( L"pal", L"720x576@64:45" )
	( L"ntsc", L"720x480@40:33" )
;

struct smart_image
{
	smart_image( const std::wstring &input, bool strict = true )
	: width( 0 )
	, height( 0 )
	, sar_num( 1 )
	, sar_den( 1 )
#ifdef ML_INTERP_REV_2
	, interp( ml::image::INTERP_AUTO )
#else
	, interp( ml::image::BICUBIC_SAMPLING )
#endif
	, pf( L"yuv420p" )
	, fo( ml::image::progressive )
	, r( 0 )
	, g( 0 )
	, b( 0 )
	, a( 0 )
	{
		// Determine what we should parse
		const image_lookup_type::const_iterator iter = image_lookup.find( input );
		const std::wstring &image = iter != image_lookup.end( ) ? ( *iter ).second : input;

		// Parse the input
		wchar_t x, at, colon;
		std::wistringstream iss( image );

		iss >> width >> x >> height >> at >> sar_num >> colon >> sar_den >> colon >> pf;

		// Split the pf to check for field order (progressive, bff or tff)
		std::wstring order;
		auto location = pf.find( L":" );
		if ( location != std::string::npos )
		{
			order = pf.substr( location + 1 );
			pf = pf.substr( 0, location );
		}

		// Split the order to obtain the interp_name
		std::wstring interp_name;
		location = order.find( L":" );
		if ( location != std::string::npos )
		{
			interp_name = order.substr( location + 1 );
			order = order.substr( 0, location );
		}

		// Deal with field order
		if ( order == L"bff" ) fo = ml::image::bottom_field_first;
		else if ( order == L"tff" ) fo = ml::image::top_field_first;
		else fo = ml::image::progressive;

#ifdef ML_INTERP_REV_2
		if ( interp_name != L"" )
			interp = ml::image::get_interp_filter( interp_name );
#else
		if ( interp_name != L"" )
		{
			if ( interp_name == L"point" )
				interp = ml::image::POINT_SAMPLING;
			else if ( interp_name == L"bilinear" )
				interp = ml::image::BILINEAR_SAMPLING;
			else if ( interp_name == L"bicubic" )
				interp = ml::image::BICUBIC_SAMPLING;
			else
				ARENFORCE_MSG( false, "Unknown interpolation filter %s" )( interp_name );
		}
#endif

		// Sanity checks
		if ( strict )
		{
			ARENFORCE_MSG( iss.eof( ), "Invalid image specification '%s'" )( image );
			ARENFORCE_MSG( width != 0 && height != 0 && sar_num != 0 && sar_den != 0, "Invalid image of %dx%d @ %d:%d" )( width )( height )( sar_num )( sar_den );
			ARENFORCE_MSG( ml::image::string_to_MLPF( cls::to_t_string( pf ) ) != ml::image::ML_PIX_FMT_NONE, "Invalid picture format of %s" )( pf );
		}
	}

	smart_image( const ml::frame_type_ptr &frame )
	: r( 0 )
	, g( 0 )
	, b( 0 )
	, a( 0 )
	{
		bool image = frame && frame->has_image( );
		width = image ? frame->width( ) : 0;
		height = image ? frame->height( ) : 0;
		frame->get_sar( sar_num, sar_den );
#ifdef ML_INTERP_REV_2
		interp = ml::image::INTERP_AUTO;
#else
		interp = ml::image::BICUBIC_SAMPLING;
#endif
		pf = image ? cls::to_wstring( frame->pf( ) ) : L"yuv420p";
		fo = image ? frame->get_image( )->field_order( ) : ml::image::progressive;
	}

	int width;
	int height;
	int sar_num;
	int sar_den;
#ifdef ML_INTERP_REV_2
	ml::image::interp_filter interp;
#else
	int interp;
#endif
	std::wstring pf;
	ml::image::field_order_flags fo;
	int r, g, b, a;
};

ml::image::geometry convert( const smart_image &image )
{
	ml::image::geometry result;
	result.interp = image.interp;
	result.pf = ml::image::string_to_MLPF( cls::to_t_string( image.pf ) );
	result.field_order = image.fo;
	result.width = image.width;
	result.height = image.height;
	result.sar_num = image.sar_num;
	result.sar_den = image.sar_den;
	result.r = image.r;
	result.g = image.g;
	result.b = image.b;
	result.a = image.a;
	return result;
}

struct smart_time
{
	smart_time( const std::wstring &input, const smart_fps &fps )
	: num( fps.num )
	, den( fps.den )
	, value( evaluate( input ) )
	{ }

	smart_time( const std::wstring &input, int num_, int den_ )
	: num( num_ )
	, den( den_ )
	, value( evaluate( input ) )
	{ }

	int evaluate( const std::wstring &input ) const
	{
		int result = 0;

		std::wstring units;
		double time = 0;
		std::wistringstream iss( input );
		iss >> time >> units;

		ARENFORCE_MSG( iss.eof( ), "Invalid time specification '%s'" )( input );

		if ( units == L"ms" )
			result = int( time * num / ( den * 1000 ) );
		else if ( units == L"s" )
			result = int( time * num / den );
		else if ( units == L"" )
			result = int( time );
		else
			{ ARENFORCE_MSG( false, "Invalid time units '%s'" )( units ); }

		return result;
	}

	int num;
	int den;
	int value;
};

