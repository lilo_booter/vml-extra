// Smart Filters
//
// Copyright (C) 2018 Charles Yates
// Released under the LGPL.
//
// #filter:smart_alpha_halve
//
// Attempts to reverse the output of filter:smart_alpha_double.

class ML_PLUGIN_DECLSPEC filter_merge_alpha : public filter_simple
{
	public:
		virtual std::wstring get_uri( ) const { return L"smart_internal_merge_alpha"; }

		virtual ml::distributable_status is_distributable( ) { return ml::distributable_should; }

		virtual size_t slot_count( ) const { return 2; }

	protected:
		// The main access point to the filter
		ml::fetch_status do_fetch( ml::frame_type_ptr &result )
		{
			ml::frame_type_ptr overlay;
			AR_TRY_FETCH( fetch_from_slot( result, 0, false ) );
			AR_TRY_FETCH( fetch_from_slot( overlay, 1, false ) );
			result->set_alpha( overlay->get_image( )->shallow( ) );
			return ml::fetch_ok;
		}
};

class ML_PLUGIN_DECLSPEC filter_smart_alpha_halve : public filter_smart
{
	public:
		virtual std::wstring get_uri( ) const { return L"smart_alpha_halve"; }

	protected:
		virtual ml::input_type_ptr create_graph( const ml::frame_type_ptr &frame )
		{
			ml::input_type_ptr result;

			// Only proceed if the frame has an image
			if ( frame->has_image( ) )
			{
 				ml::stack stack;

				stack
				<< fetch_slot( )
				<< "filter:crop ry=0 rh=0.5"
				<< fetch_slot( )
				<< "filter:crop ry=0.5 rh=0.5"
				<< "filter:swscale pf=l8"
				<< ml::filter_type_ptr( new filter_merge_alpha )
				>> result
				;
			}
			else
			{
				SMART_DEBUG( SMART_ID << " has determined it has nothing to do" );
			}

			return result;
		}
};

