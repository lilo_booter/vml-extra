// Smart Filters
//
// Copyright (C) 2018 Charles Yates
// Released under the LGPL.
//
// #filter:smart_alpha_double
//
// Provides a simplified wrapper for converting an alpha carrying input to a 
// double height, non-alpha carrying output.

class ML_PLUGIN_DECLSPEC filter_smart_alpha_double : public filter_smart
{
	public:
		virtual std::wstring get_uri( ) const { return L"smart_alpha_double"; }

	protected:
		virtual ml::input_type_ptr create_graph( const ml::frame_type_ptr &frame )
		{
			ml::input_type_ptr result;

			// Only proceed if the frame has an image
			if ( frame->has_image( ) )
			{
				ml::stack stack;

				smart_fps fps( frame );
				smart_image image( frame );

				image.height *= 2;
				image.pf = std::wstring( L"yuv420p" );
				image.a = 255;

				stack

				// Create a colour: background as twice height of input and non-alpha carrying
				<< input::colour( image, fps )

				// Upper half of output - remove the alpha
				<< fetch_slot( )
				<< filter::colour_space( image )
				<< "filter:remove component=alpha"
				<< "filter:lerp @@y=0.0 @@h=0.5"

				// Lower half of output - convert alpha to luma
				<< fetch_slot( )
				<< "filter:extract_alpha force=1"
				<< "filter:remove component=audio"
				<< "filter:lerp @@y=0.5 @@h=0.5"

				// Composite all 3
				<< "filter:compositor slots=3"

				// Pop the output graph
				>> result
				;
			}
			else
			{
				SMART_DEBUG( SMART_ID << " has determined it has nothing to do" );
			}

			return result;
		}
};

