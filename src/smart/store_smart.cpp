// Smart Store
//
// Copyright (C) 2018 Charles Yates
// Released under the LGPL.
//
// #store:smart:
//
// Provides a wrapper for other stores and ensures that fps, audio and images
// are consistent before forwarding to the internal store.
//
// amlbatch <graph> -- preview: smart:decklink: 

class ML_PLUGIN_DECLSPEC store_smart : public ml::store_type
{
	public:
		typedef std::shared_ptr < ml::fps_regulate > fps_regulate_ptr;

		store_smart( const std::wstring &spec, const ml::frame_type_ptr &frame )
		: spec_( spec )
		, first_frame_( frame->shallow( ) )
		, prop_audio_( pl::pcos::key::from_string( "audio" ) )
		, prop_image_( pl::pcos::key::from_string( "image" ) )
		, prop_fps_( pl::pcos::key::from_string( "fps" ) )
		, audio_( frame )
		, image_( frame )
		, fps_( frame )
		{
			properties( ).append( prop_audio_ = std::wstring( L"" ) );
			properties( ).append( prop_image_ = std::wstring( L"" ) );
			properties( ).append( prop_fps_ = std::wstring( L"" ) );
		}

		// Initialise method
		virtual bool init( )
		{
			// Extract custom info from fixed properties if specified
			audio_ = parse< smart_audio >( prop_audio_.value< std::wstring >( ), audio_ );
			image_ = parse< smart_image >( prop_image_.value< std::wstring >( ), image_ );
			fps_ = parse< smart_fps >( prop_fps_.value< std::wstring >( ), fps_ );

			// Construct the regulator
			regulator_ = fps_regulate_ptr( new ml::fps_regulate( fps_.num, fps_.den ) );
			regulator_->set_shape( convert( image_ ) );
			regulator_->set_audio( audio_.frequency, audio_.channels, ml::audio::af_to_id( cls::to_t_string( audio_.af ) ) );

			// We need a valid sample created from the first frame to initialise the inner store
			first_frame_ = regulator_->prepare( first_frame_ );
			regulator_->push_audio( first_frame_ );
			regulator_->push_video( first_frame_ );

			// We're finished with the first frame now, so kill it
			first_frame_.reset( );

			// We can force the regulator to give us this one and intialise the inner store as required
			ml::frame_type_ptr sample = regulator_->pop( true );

			// Now construct the inner store
			inner_ = ml::create_store( spec_, sample );
			ARENFORCE_MSG( !!inner_, "Unable to create a store of %s" )( spec_ );

			// Pass any custom properties down to the inner store
			pass_properties( "@", properties( ), inner_->properties( ) );

			return inner_->init( );
		}

		// Push a frame to the store
		virtual bool push( ml::frame_type_ptr frame )
		{
			bool result = true;

			frame = regulator_->prepare( frame );
			regulator_->push_audio( frame );
			regulator_->push_video( frame );

			for( ml::frame_type_ptr queued = regulator_->pop( ); result && !!queued; queued = regulator_->pop( ) )
				result = inner_->push( boost::move( queued ) );

			return result;
		}

		// Tell the store to flush all pending frames, while returning the
		// one that was pending. The default implementation applies when
		// the store doesn't queue frames.
		virtual frame_type_ptr flush( )
		{
			regulator_->pop( true );
			return inner_->flush( );
		}

		// Playout all queued frames and return when done. The default
		// implementation applies when the store doesn't queue frames.
		virtual void complete( )
		{
			ml::frame_type_ptr frame = regulator_->pop( true );
			if ( !!frame ) inner_->push( boost::move( frame ) );
			inner_->complete( );
		}

	private:
		template < typename C >
		C parse( const std::wstring &input, const C &fallback )
		{
			return input != L"" ? C( input ) : fallback;
		}

		typedef pl::pcos::property_container container;
		typedef pl::pcos::property property;
		typedef pl::pcos::key_vector keys;

		void pass_properties( const std::string &prefix, const container &source_properties, const container &dest_properties )
		{
			const int ps = static_cast< int >( prefix.size( ) );
			const keys source_keys = source_properties.get_keys( );

			for( keys::const_iterator it = source_keys.begin( ); it != source_keys.end( ); ++ it )
			{
				const std::string source_prop_name( ( *it ).as_string( ) );

				if ( source_prop_name.find( prefix ) != 0 ) continue;

				const std::string dest_prop_name = source_prop_name.substr( ps );
				property dest_prop = dest_properties.get_property_with_string( dest_prop_name.c_str( ) );
				property source_prop = source_properties.get_property_with_string( source_prop_name.c_str( ) );

				ARENFORCE_MSG( dest_prop.valid( ), "Destination property '%s' is invalid" )( dest_prop_name.c_str( ) );
				ARENFORCE_MSG( source_prop.valid( ), "Source property '%s' is invalid" )( source_prop_name.c_str( ) );

				if ( dest_prop.type( ) == source_prop.type( ) )
				{
					dest_prop.set_from_property( source_prop );
				}
				else if ( source_prop.is_a< std::wstring >( ) )
				{
					dest_prop.set_from_string( source_prop.value< std::wstring >( ) );
				}
				else
				{
					std::ostringstream ostr;
					ostr << source_prop;
					dest_prop.set_from_string( cl::str_util::to_wstring( ostr.str( ) ) );
				}
			}
		}

		const std::wstring spec_;
		ml::frame_type_ptr first_frame_;

		pl::pcos::property prop_audio_;
		pl::pcos::property prop_image_;
		pl::pcos::property prop_fps_;

		smart_audio audio_;
		smart_image image_;
		smart_fps fps_;

		fps_regulate_ptr regulator_;
		ml::store_type_ptr inner_;
};

