// Smart Filters
//
// Copyright (C) 2020 Charles Yates
// Released under the LGPL.
//
// #filter:smart_sleep
//
// Attempts to regulate time between frames to the framerate of the input.

using namespace std::chrono;

class ML_PLUGIN_DECLSPEC filter_smart_sleep : public ml::filter_simple
{
	public:
		virtual std::wstring get_uri( ) const { return L"smart_sleep"; }

		ml::distributable_status is_distributable( ) { return ml::distributable_no; }

	protected:
		ml::fetch_status do_fetch( ml::frame_type_ptr &result )
		{
			if ( start_time_point_ > steady_clock::now( ) )
				std::this_thread::sleep_until( start_time_point_ );
			else
				start_time_point_ = steady_clock::now( );

			AR_TRY_FETCH( fetch_from_slot( result ) );
			start_time_point_ += microseconds( int64_t( 1000000.0 / result->fps( ) ) );

			return ml::fetch_ok;
		}

		steady_clock::time_point start_time_point_;
};
