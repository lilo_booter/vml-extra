// session.cpp - implementation of a client
//
// Copyright (C) 2016 Charles Yates
// Released under the LGPL.

// Daemon Headers
#include "session.hpp"
#include "dictionaries.hpp"

// Boost Headers
#include <boost/asio.hpp>
#include <boost/algorithm/string/replace.hpp>

namespace aml { namespace daemon {

namespace ml = olib::openmedialib::ml;
namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;

// An aml session object provides each connected client with their own aml_stack
// and thread instances. All communications with the underlying aml_stack: input
// are done via the various properties which it exposes.
class session_type_impl : public session_type
{
	public:
		typedef ml::fn_observer< session_type_impl > observer;
		typedef std::shared_ptr< pl::pcos::observer > observer_ptr;
		typedef std::deque< std::string > stdout_queue_type;
		typedef std::deque< std::string > command_queue_type;

		session_type_impl( boost::asio::io_service& io_service, player_type_ptr player, const dictionaries_type &dictionaries )
		: socket_( io_service )
		, player_( player )
		, dictionaries_( dictionaries )
		, prop_result_( stack_.property( "result" ) )
		, prop_stdout_( stack_.property( "stdout" ) )
		, obs_stdout_( new observer( const_cast< session_type_impl * >( this ), &session_type_impl::do_write_stdout ) )
		, prop_query_( stack_.property( "query" ) )
		, prop_handled_( stack_.property( "handled" ) )
		, obs_query_( new observer( const_cast< session_type_impl * >( this ), &session_type_impl::query_dictionary ) )
		, capturing_( false )
		, is_running_( true )
		, ended_( true )
		{
			// Ensure stdout is redirected to prop_stdout_
			stack_.property( "redirect" ) = 1;

			// We only want to process prop_stdout when it actually holds a different value
			prop_stdout_.attach( obs_stdout_ );

			// Each time query is assigned, that's an invitation for us to check our local dictionary
			prop_query_.set_always_notify( true );
			prop_query_.attach( obs_query_ );
		}

		// Clean up the session when the object is destroyed.
		virtual ~session_type_impl( )
		{
			player( )->unblock( );
			player( )->signal( );
			stop( );
			thread_.join( );
		}

		// Returns the socket associated to the session
		boost::asio::ip::tcp::socket &socket( )
		{
			return socket_;
		}

		// Start the sessions thread and sets up the async socket reading.
		void start( )
		{
			// Load a couple of grammars
			dictionaries_.apply( stack_ );

			// Start the thread which monitors the command queue
			thread_ = boost::thread( [ this ]( ) { thread( ); } );

			// Register socket reading with the socket
			do_read( );
		}

		// Writes data to the stdout queue and triggers the do_stdout method if the
		// queue was previously empty.
		void write( const std::string &data )
		{
			bool empty;
			{
				lock_type lock( mutex_ );
				empty = stdout_queue_.empty( );

				// Just remove all \r\n in the output
				std::string copy = data;
				boost::replace_all( copy, "\r\n", "\n" );

				// Tracks whether the closing status line needs a cr lf separator or not.
				if ( !capturing_ )
					ended_ = copy.length( ) >= 1 ? copy.substr( copy.length( ) - 1 ) == "\n" : false;

				// Push the data to the stdout queue
				stdout_queue_.push_back( copy );
			}

			// The first call to write will ensure that the asio starts writing now.
			if ( empty ) do_stdout( );
		}

		void flush( )
		{
			lock_type lock( mutex_ );
			while( is_running_ && player_->is_running( ) && !stdout_queue_.empty( ) )
				io_written_.wait( lock );
		}

		// Returns the stack associated to the session
		ml::stack &stack( )
		{
			return stack_;
		}

		// Returns the player associated to the session
		player_type_ptr player( )
		{
			return player_;
		}

		// Checks if the server is running and client is still connected
		bool is_active( ) const
		{
			lock_type lock( mutex_ );
			return is_running_ && player_->is_running( );
		}

		// Turns capture on and off. When capturing, the stdout of the executed
		// commands are added to the captured_ stringstream instead of being written
		// to the client. When it's turned off, the resultant data is added as a
		// string to the stack (thus bypassing parsing).
		void capture( int flag )
		{
			capturing_ = flag != 0;
			if ( !capturing_ )
			{
				// We need to introduce the capture to the stack safely such that it
				// doesn't get executed. This is done by introducing it as "$ <capture>".
				std::list< std::wstring > list;
				list.push_back( std::wstring( L"$" ) );

				// Since %s instances in the capture would be replaced by stack entries,
				// we need to escape them here by replacing % with %%.
				std::string result = capture_.str( );
				capture_.str( "" );
				boost::replace_all( result, "%", "%%" );

				// Finally add the correct result to the command list or an empty string
				// if necessary.
				if ( result.size( ) != 0 )
					list.push_back( cl::str_util::to_wstring( result ) );
				else
					list.push_back( std::wstring( L"\"\"" ) );

				// Push the command list and clear the capture
				stack_.push( list );
			}
		}

        // Acquire the clients dictionaries
        const dictionaries_type &dictionaries( ) const
		{
			return dictionaries_;
		}

	private:
		// This triggers an async read on the socket. When data is received, the data is
		// obtained by way of the run_read method below, split up by line feeds and
		// forwarded to the command queue.
		void do_read( )
		{
			#define AML_AREAD boost::system::error_code ec, size_t length
			#define AML_AREAD_ARGS ec, length
			auto self( shared_from_this( ) );
			boost::asio::async_read_until( socket_, data_, '\n', [ this, self ]( AML_AREAD ) { if ( run_read( AML_AREAD_ARGS ) ) do_read( ); } );
		}

		// Process data received by the socket.
		bool run_read( boost::system::error_code ec, size_t length )
		{
			bool result = !ec;
			CERR( "---> start read command" );
			if ( result )
			{
				std::istream is( &data_ );
				std::string command;
				std::getline( is, command );
				CERR( "--> received: " << command );
				queue( command );
			}
			else
			{
				CERR( "-> client disconnected with " << ec );
			}
			CERR( "---> end read command" );
			return result;
		}

		// Places a command in the queue.
		void queue( std::string &command )
		{
			lock_type lock( mutex_ );
			command_queue_.push_back( command );
			condition_.notify_all( );
		}

		// The session's thread implementation. Simply waits for a command or termination.
		// When a command is provided, extract it from the command queue and forward to the
		// run method below.
		void thread( )
		{
			// Process commands when they arrive
			while ( wait_for_command( ) )
			{
				std::string command;
				{
					lock_type lock( mutex_ );
					command = command_queue_[ 0 ];
					command_queue_.pop_front( );
				}
				run( command );
			}
		}

		// Blocks the thread until either a command has been received or the session
		// has been stopped.
		bool wait_for_command( )
		{
			lock_type lock( mutex_ );
			while( is_running_ && command_queue_.size( ) == 0 )
				condition_.wait( lock );
			return is_running_ && command_queue_.size( );
		}

		// Termination method for the session's thread
		void stop( )
		{
			lock_type lock( mutex_ );
			is_running_ = false;
			condition_.notify_all( );
		}

		// This method is run on the session's thread when commands are received.
		// Only one command can be run at a time on each session, and they should be
		// submitted via the command_queue.
		void run( const std::string &command )
		{
			try
			{
				stack_ << command;
			}
			catch( cl::base_exception &e )
			{
				if ( prop_result_.value< std::wstring >( ) == L"OK" )
				{
					olib::t_stringstream ss;
					e.pretty_print_one_line( ss, cl::print::output_default );
					prop_result_ = cl::str_util::to_wstring( ss.str( ) );
				}
				player_->unblock( );
			}
			catch( std::exception &e )
			{
				if ( prop_result_.value< std::wstring >( ) == L"OK" )
					prop_result_ = cl::str_util::to_wstring( e.what( ) );
				player_->unblock( );
			}

			// Obtain the result of the command now
			std::wstring error = prop_result_.value< std::wstring >( );

			// We need to ensure that capture is turned off at the end of the command,
			// otherwise the client will not receive the success/error status.
			if ( capturing_ ) capture( 0 );

			// Ugly, but will ensure that at the end of the command execution, the client
			// receives either or an ERROR: <message> string or just the output on success.
			//
			// The recommended approach to knowing when a command has completed is for the 
			// client to add an echo after the command, like:
			//
			// send( "any command you want" )
			// send( "aml.echo-a-random-ack-string" )
			// while ( read( data ) != a-random-ack-string )
			//    ... data is output from command ...
			//
			// Thus the client will simply block until a-random-ack-string is returned.
			// This aml.echo- use must be executed as a separate command.

			if ( error != L"OK" )
				error = L"ERROR: " + error;
			else if ( !is_active( ) )
				error = L"ERROR: server shutdown";
			else
				error = L"";

			if ( error != L"" )
				write( ( ended_ ? "" : "\n" ) + cl::str_util::to_string( error ) + "\n" );
			else if ( !ended_ )
				write( "\n" );

			ended_ = true;
		}

		// This method is invoked when data is written to the aml_stack's stdout property.
		// It is processed by the session's write command above (thus invoking do_stdout when
		// necessary).
		void do_write_stdout( )
		{
			std::string output = prop_stdout_.value< std::string >( );
			if ( output != "" )
			{
				write( output );
				prop_stdout_ = std::string( "" );
			}
		}

		// This method is invoked when stdout data is available for writing to the client.
		// The actual write to the socket may not happen immediately, and more data maybe
		// available by the time it is, so it will reschedule itself if necesssary. Note
		// that data isn't removed immediately from the stdout queue to avoid subsequent
		// additions to the stdout_queue starting another run at the same time.
		void do_stdout( )
		{
			#define AML_AWRITE boost::system::error_code ec, size_t length
			#define AML_AWRITE_ARGS ec, length
			try
			{
				auto self( shared_from_this( ) );
				std::string output;
				{
					lock_type lock( mutex_ );
					output = stdout_queue_.front( );
				}

				if ( !capturing_ )
				{
					boost::asio::async_write( socket_, boost::asio::buffer( output.data( ), output.length( ) ), [ this, self ]( AML_AWRITE ) { if ( clean_stdout( AML_AWRITE_ARGS ) ) do_stdout( ); } );
				}
				else
				{
					capture_ << output;
					boost::system::error_code ec;
					clean_stdout( ec, output.size( ) );
				}
			}
			catch( ... )
			{
			}
		}

		// Remove the last entry from the stdout queue and return an indication if there's more
		// pending data to process. Note that it's possible that not all the data at the front
		// of the queue has been written, so we should only remove what has been written here.
		bool clean_stdout( boost::system::error_code ec, std::size_t length )
		{
			bool result = !ec;

			if ( result )
			{
				lock_type lock( mutex_ );
				std::string output = stdout_queue_.front( );
				if ( output.size( ) == length )
				{
					stdout_queue_.pop_front( );
					result = !stdout_queue_.empty( );
				}
				else
				{
					stdout_queue_[ 0 ] = output.substr( length );
				}
			}

			io_written_.notify_all( );

			return result;
		}

		// aml_stack allows the hosting application to handle words - if the word is to be taken
		// by the application, it should run the command then set handled to 1. Any words it
		// chooses to ignore are left for the stack parser to process or reject.
		void query_dictionary( )
		{
			std::string token = cl::str_util::to_string( prop_query_.value< std::wstring >( ) );
			auto word = dictionaries_.find( token );
			if ( word != 0 )
			{
				CERR( "---> processing query: " << token );
				word( *this );
				prop_handled_ = 1;
			}
		}

		boost::asio::ip::tcp::socket socket_;
		player_type_ptr player_;
		const dictionaries_type &dictionaries_;
		boost::asio::streambuf data_;
		ml::stack stack_;
		pl::pcos::property prop_result_;
		pl::pcos::property prop_stdout_;
		observer_ptr obs_stdout_;
		pl::pcos::property prop_query_;
		pl::pcos::property prop_handled_;
		observer_ptr obs_query_;
		bool capturing_;
		std::stringstream capture_;

		boost::thread thread_;
		mutable boost::mutex mutex_;
		boost::condition_variable condition_;
		boost::condition_variable io_written_;
		stdout_queue_type stdout_queue_;
		command_queue_type command_queue_;
		bool is_running_;
		bool ended_;
};

session_type_ptr create_session( boost::asio::io_service& io_service, player_type_ptr player, const dictionaries_type &dictionaries )
{
	return session_type_ptr( new session_type_impl( io_service, player, dictionaries ) );
}

} }
