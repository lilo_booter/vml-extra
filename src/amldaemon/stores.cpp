// stores.hpp - provides a means to collect a group of stores
//
// Copyright (C) 2016 Charles Yates
// Released under the LGPL.

// Daemon Headers
#include "stores.hpp"

namespace aml { namespace daemon {

namespace ml = olib::openmedialib::ml;
namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;

// The stores class parses the provided command line arguments via the add
// method. When a valid graph has been detected, the first frame from the graph
// can be used to convert the arguments to physical aml stores. Finally, the
// push method will present each pushed frame for playout.
class stores_type_impl : public stores_type
{
	public:
		typedef std::vector< std::string > store_tokens_type;
		typedef std::map< std::string, store_tokens_type > store_list_type;
		typedef std::map< std::string, ml::store_type_ptr > stores_map;

		stores_type_impl( )
		: deferrable_( false )
		{
		}

		// Indicates if the stores have been created yet
		bool created( ) const
		{
			lock_type lock( mutex_ );
			return stores_.size( ) > 0;
		}

		// Add a store store to the stores class for use by the init method.
		bool add( const std::string &token )
		{
			lock_type lock( mutex_ );
			// The tokens can contain 1 or more stores - the tokens we obtain here
			// must be parsed and are of the form:
			//
			// [ <store> [ <property> ]* ]+

			// Tokens can only be added before the stores are constructed
			bool result = stores_.size( ) == 0;

			if ( result )
			{
				if ( current_ != "" && ml::is_property( token ) )
					list_[ current_ ].push_back( token );
				else if ( ml::is_store( token ) )
					list_[ current_ = token ] = store_tokens_type( );
				else
					result = false;
			}

			return result;
		}

		// Create the stores with the sample frame
		bool create( const ml::frame_type_ptr &frame )
		{
			lock_type lock( mutex_ );
			bool result = frame && stores_.size( ) == 0;

			if ( result )
			{
				bool is_deferred = true;
				property_list keydowns;

				if ( list_.size( ) == 0 )
					list_[ "preview:" ] = store_tokens_type( );

				for ( auto iter : list_ )
				{
					CERR( "--> creating store: " << iter.first );

					ml::store_type_ptr store = ml::create_store( iter.first, frame );

					if ( initialise( store, iter.first, iter.second ) )
					{
						// Check for an assigned deferrable property on all stores
						pl::pcos::property deferrable = store->property( "deferrable" );
						is_deferred &= deferrable.valid( ) && deferrable.value< int >( );

						// Check for the keydown property and add it to the list if found
						pl::pcos::property keydown = store->property( "keydown" );
						if ( keydown.valid( ) )
							keydowns.push_back( keydown );

						// Save the initialised store
						stores_[ iter.first ] = store;
					}
					else
					{
						CERR( "> ERROR: failed to create store " << iter.first );
						result = false;
						break;
					}
				}

				if ( result )
				{
					deferrable_ = is_deferred;
					keydown_list_ = keydowns;
				}
			}

			return result;
		}

		// Push the frame to the stores
		bool push( const ml::frame_type_ptr &frame )
		{
			lock_type lock( mutex_ );
			bool result = stores_.size( ) > 0;

			if ( result )
			{
				for ( auto iter : stores_ )
				{
					ARENFORCE_MSG( iter.second->push( !!frame ? frame->shallow( ) : ml::frame_type_ptr( ) ), "Unable to push frame to store %s" )( iter.first );
				}
			}

			return result;
		}

		// Indicates if the stores allow deferred compositing
		bool is_deferrable( ) const
		{
			lock_type lock( mutex_ );
			return deferrable_;
		}

		// Provides the list of keydown properties in case the app wants to monitor them
		property_list keydown( ) const
		{
			lock_type lock( mutex_ );
			return keydown_list_;
		}

		// Courtesy report
		std::ostream &report( std::ostream &os )
		{
			lock_type lock( mutex_ );
			if ( stores_.size( ) > 0 )
			{
				os << "Stores initialised" << ( deferrable_ ? " as deferrable" : "" ) << ( keydown_list_.size( ) > 0 ? " with keydown" : "" ) << '\n';
				for ( auto iter : stores_ )
					os << iter.first << ' ' << iter.second->properties( ) << '\n';
			}
			else
			{
				os << "Stores not initialised" << '\n';
				for ( auto iter : list_ )
					os << iter.first << ' ' << iter.second << '\n';
			}

			return os;
		}

		// Modify store settings before or after creation
		bool modify( const std::string &store, const std::string &token )
		{
			lock_type lock( mutex_ );
			bool result = false;
			if ( stores_.size( ) > 0 )
			{
				auto iter = stores_.find( store );
				result = iter != stores_.end( ) && ml::is_property( token );
				if ( result )
					result = ml::assign( iter->second, token );
			}
			else
			{
				auto iter = list_.find( store );
				result = iter != list_.end( ) && ml::is_property( token );
				if ( result )
					iter->second.push_back( token );
			}

			return result;
		}

	private:
		bool initialise( const ml::store_type_ptr &store, const std::string &name, const store_tokens_type &tokens )
		{
			bool result = !!store;

			if ( result )
			{
				for ( auto iter : tokens )
				{
					result = assign( store, iter );
					if ( !result )
					{
						CERR( "> ERROR: failed to assign " << iter << " to store " << name );
						break;
					}
				}
			}

			return result && store->init( );
		}

		mutable boost::mutex mutex_;
		store_list_type list_;
		std::string current_;
		stores_map stores_;
		bool deferrable_;
		property_list keydown_list_;
};

stores_type_ptr create_stores( )
{
	return stores_type_ptr( new stores_type_impl );
}

} }
