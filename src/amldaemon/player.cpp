// player.cpp - the player class for the amldaemon
//
// Copyright (C) 2016 Charles Yates
// Released under the LGPL.

// Daemon Headers
#include "player.hpp"
#include <boost/atomic.hpp>

namespace aml { namespace daemon {

namespace ml = olib::openmedialib::ml;
namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;

// Media playing class
class player_type_impl : public player_type
{
	public:
		typedef std::map< std::string, ml::input_type_ptr > normaliser_type;
		typedef boost::recursive_mutex::scoped_lock lock_type;

		player_type_impl( stores_type_ptr stores )
		: stores_( stores )
		, shutdown_( false )
		, speed_( 1 )
		, position_( 0 )
		, generation_( 0 )
		, pitch_( 1.0 )
		, volume_( 1.0 )
		, capacity_( 1000 )
		, auto_cue_( true )
		{
			// Minimalist stack grammar since this stack isn't used much - this is
			// configurable via the set_normalise method.
			stack_
			<< ": normalise"
			<< "	: dim width=1920 height=1080 ;"
			<< "	: sar sar_num=1 sar_den=1 ;"
			<< "	: res dim sar ;"
			<< "	filter:threader @name=threader queue=50 active=1"
			<< "	filter:avvideo_yadif @name=deinterlace"
			<< "	filter:resampler @name=resampler channels=2 frequency=48000"
			<< "	filter:volume @name=volume"
			<< "	filter:pitch @name=pitch"
			<< "	filter:visualise @name=visualise type=1 hold=0 res"
			<< "	filter:conform @name=conform"
			<< ";"
			;
		}

		// Introduces a new normalise word if required. Only takes effect if there
		// has been no attempt to play media.
		bool set_normalise( std::string &script )
		{
			lock_type lock( mutex_ );
			bool result = script.find( ": normalise" ) == 0 && !stores_->created( );
			if ( result )
				stack_ << script;
			return result;
		}

		// Play input immediately
		bool play( const ml::input_type_ptr &input )
		{
			lock_type lock( mutex_ );
			bool result = test_graph( input, lock );
			if ( result )
			{
				save( input_ );
				futures_.clear( );
				input_ = input;
				speed_ = 1;
				position_ = 0;
				pitch_ = 1.0;
				generation_ ++;
				requested_.notify_all( );
				actioned_.wait( lock );
			}
			return result;
		}

		// Add the input to the queue
		bool add( const ml::input_type_ptr &input )
		{
			lock_type lock( mutex_ );
			bool result = test_graph( input, lock );
			if ( result )
			{
				generation_ ++;
				if ( !input_ )
					input_ = input;
				else
					futures_.push_back( serialise( input ) );
				requested_.notify_all( );
				actioned_.wait( lock );
			}
			return result;
		}

		// Remove the specified playlist item
		void remove( int position )
		{
			lock_type lock( mutex_ );
			remove_item( position );
			requested_.notify_all( );
		}

		// Move to the selected item in the playlist
		void select( int position )
		{
			lock_type lock( mutex_ );
			select_item( position );
			requested_.notify_all( );
		}

		// Obtain a serialisation of the item in the playlist
		std::string obtain( int position )
		{
			lock_type lock( mutex_ );
			return obtain_item( position );
		}

		void after( int position, const ml::input_type_ptr &input )
		{
			lock_type lock( mutex_ );
			after_item( position, input );
			requested_.notify_all( );
		}

		void overwrite( int position, const ml::input_type_ptr &input )
		{
			lock_type lock( mutex_ );
			overwrite_item( position, input );
			requested_.notify_all( );
		}

		void wipe_history( )
		{
			lock_type lock( mutex_ );
			history_.clear( );
			generation_ ++;
			requested_.notify_all( );
		}

		void wipe_futures( )
		{
			lock_type lock( mutex_ );
			futures_.clear( );
			generation_ ++;
			requested_.notify_all( );
		}

		void wipe( )
		{
			lock_type lock( mutex_ );
			history_.clear( );
			futures_.clear( );
			generation_ ++;
			requested_.notify_all( );
		}

		// Move to the next input in the queue
		void next( )
		{
			lock_type lock( mutex_ );
			if ( futures_.size( ) > 0 )
			{
				discard( );
				requested_.notify_all( );
			}
		}

		void prev( )
		{
			lock_type lock( mutex_ );
			if ( history_.size( ) > 0 )
			{
				// Push the current playing item into the future
				futures_.insert( futures_.begin( ), serialise( input_ ) );

				// Pull the last item from the history
				const auto graph = history_.back( );
				history_.pop_back( );

				// Play the historical item
				ml::input_type_ptr input;
				stack_ << graph >> input;
				input_ = input;

				// Yick
				if ( auto_cue_ )
				{
					speed_ = 1;
					position_ = 0;
					pitch_ = 1.0;
				}
				generation_ ++;
				requested_.notify_all( );
			}
		}

		// Return a copy of the current playlist
		playlist_type playlist( ) const
		{
			lock_type lock( mutex_ );
			return futures_;
		}

		// Return a copy of the current history
		playlist_type history( ) const
		{
			lock_type lock( mutex_ );
			return history_;
		}

		// Determine whether auto cue is on or off
		bool auto_cue( ) const
		{
			lock_type lock( mutex_ );
			return auto_cue_;
		}

		// Set auto cue on or off
		void set_auto_cue( bool cue )
		{
			lock_type lock( mutex_ );
			auto_cue_ = cue;
		}

		// Replace the current playing input with the one specified
		bool replace( const ml::input_type_ptr &input, bool _save )
		{
			lock_type lock( mutex_ );
			bool result = test_graph( input, lock );
			if ( result )
			{
				if ( _save ) save( input_ );
				input_ = input;
				attach( true );
				position_ = std::min( input->get_frames( ) - 1, position_ );
				generation_ ++;
				requested_.notify_all( );
				actioned_.wait( lock );
			}
			return result;
		}

		// Obtain the current playing input
		ml::input_type_ptr current( ) const
		{
			lock_type lock( mutex_ );
			return input_;
		}

		// Obtain the current speed
		int speed( ) const
		{
			lock_type lock( mutex_ );
			return speed_;
		}

		// Set the speed
		void set_speed( int speed )
		{
			lock_type lock( mutex_ );
			speed_ = speed;
			requested_.notify_all( );
			if ( !!input_ )
				actioned_.wait( lock );
		}

		// Obtain the current pitch
		double pitch( ) const
		{
			lock_type lock( mutex_ );
			return pitch_;
		}

		// Set the pitch
		void set_pitch( double pitch )
		{
			lock_type lock( mutex_ );
			pitch_ = pitch;
		}

		// Obtain the current volume
		double volume( ) const
		{
			lock_type lock( mutex_ );
			return volume_;
		}

		// Set the volume
		void set_volume( double volume )
		{
			lock_type lock( mutex_ );
			volume_ = volume;
		}

		// Obtain the current position
		int position( ) const
		{
			lock_type lock( mutex_ );
			return position_;
		}

		// Seek to the requested position
		void seek( int position )
		{
			lock_type lock( mutex_ );
			ml::input_type_ptr graph = attach( );
			if ( !!graph )
			{
				int frames = graph->get_frames( );
				position = position < 0 ? frames + position : position;
				position_ = position < 0 ? 0 : position >= frames ? frames - 1 : position;
				requested_.notify_all( );
				actioned_.wait( lock );
			}
		}

		// The generation is incremented each time the input changes
		int generation( ) const
		{
			lock_type lock( mutex_ );
			return generation_;
		}

		// Return the current play status
		status_type status( ) const
		{
			lock_type lock( mutex_ );
			return status_type( position_, !!input_ ? input_->get_frames( ) : -1, speed_, generation_ );
		}

		// Shutdown the player
		void shutdown( )
		{
			lock_type lock( mutex_ );
			do_shutdown( );
			actioned_.wait( lock );
		}

		bool is_running( ) const
		{
			lock_type lock( mutex_ );
			return !shutdown_;
		}

		stores_type_ptr stores( ) const
		{
			lock_type lock( mutex_ );
			return stores_;
		}

		bool is_initialised( ) const
		{
			lock_type lock( mutex_ );
			return stores_->created( );
		}

		void block( )
		{
			mutex_.lock( );
			atom_id_.store( boost::this_thread::get_id( ), boost::memory_order_release );
			filter_assign( "threader", "active", 0 );
		}

		void unblock( )
		{
			if ( atom_id_.load( boost::memory_order_acquire ) == boost::this_thread::get_id( ) )
			{
				atom_id_.store( boost::thread::id( ), boost::memory_order_release );
				mutex_.unlock( );
			}
		}

		// Sync
		void sync( )
		{
			lock_type lock( mutex_ );
			ml::input_type_ptr input = attach( );
			if ( !!input )
			{
				int before = input->get_frames( );
				input->sync( );
				CERR( "---> sync from " << before << " to " << input->get_frames( ) );
			}
		}

		int frames( )
		{
			lock_type lock( mutex_ );
			int result = 0;
			ml::input_type_ptr input = attach( );
			if ( !!input )
				result = input->get_frames( );
			return result;
		}

		// Play graphs until shutdown
		void run( )
		{
			while( wait_for_input( ) )
			{
				try
				{
					if ( initialise_stores( ) )
						play_graph( );
				}
				catch( ... )
				{
					lock_type lock( mutex_ );
					if ( !!input_ )
						discard( true );
				}
			}

			signal_action( );
		}

		// Courtesy report
		std::ostream &report( std::ostream &os )
		{
			lock_type lock( mutex_ );
			auto upstream = normalisers_.find( "upstream" );
			auto downstream = normalisers_.find( "downstream" );
			if ( upstream != normalisers_.end( ) && downstream != normalisers_.end( ) )
			{
				ml::input_type_ptr down = downstream->second;
				while( down != upstream->second->fetch_slot( ) )
				{
					std::wstring name = pl::pcos::value< std::wstring >( down->properties( ), pl::pcos::key::from_string( "@name" ), down->get_uri( ) );
					os << cl::str_util::to_string( name ) << ' ' << down->properties( ) << '\n';
					down = down->fetch_slot( );
				}
			}
			else
			{
				os << "Player has not been initialised." << "\n";
			}

			int depth;
			stack_ << "depth?" >> depth;
			os << "Number of items on stack: " << depth << "\n";

			return os;
		}

		// Modify normalisation filter settings after creation
		bool modify( const std::string &filter, const std::string &token )
		{
			lock_type lock( mutex_ );
			auto iter = normalisers_.find( filter );
			bool result = iter != normalisers_.end( ) && ml::is_property( token );
			if ( result )
				result = ml::assign( iter->second, token );
			return result;
		}

		void wait( )
		{
			lock_type lock( mutex_ );
			actioned_.wait( lock );
		}

		void signal( )
		{
			signal_action( );
		}

	private:
		void do_shutdown( )
		{
			shutdown_ = true;
			requested_.notify_all( );
		}

		// Block until provided with an input or shutdown
		bool wait_for_input( )
		{
			lock_type lock( mutex_ );
			CERR( "-> waiting for activity" );
			while( !shutdown_ && !input_ )
				requested_.wait( lock );
			CERR( ( !shutdown_ ? "-> player received graph" : "> shutdown request received" ) );
			return !shutdown_;
		}

		void signal_action( )
		{
			actioned_.notify_all( );
		}

		// Initialise the stores
		bool initialise_stores( )
		{
			lock_type lock( mutex_ );
			bool result = stores_->created( );

			// Only do this once
			if ( !result )
			{
				// To initialise the store, we need a sample frame from our input
				ml::frame_type_ptr sample;

				ml::input_type_ptr input = attach( );
				if ( !!input )
				{
					ml::fetch_status status = input->fetch( sample, 0 );
					result = status == ml::fetch_ok;
				}

				// It might be OK to fail gracefully?
				if ( !result ) return false;

				// Attempt to create the stores
				result = stores_->create( sample );

				// If neither succeeded, fail and shutdown
				if ( !result )
				{
					CERR( "> FATAL ERROR: Unable to create stores." );
					do_shutdown( );
				}
			}

			return result;
		}

		// Play graphs until shutdown (really should provide an option to exit when playlist depleted...)
		void play_graph( )
		{
			while( 1 )
			{
				// We need the player lock while obtaining the frame
				ml::frame_type_ptr frame;
				bool end_of_input;

				{
					lock_type lock( mutex_ );

					if ( shutdown_ ) break;

					bool error;
					frame = cue_position( end_of_input, error );

					if ( error )
					{
						discard( true );
						break;
					}

					if ( end_of_input && auto_cue_ )
						discard( );
				}

				// Once obtain, we're ok to push to the stores
				stores_->push( frame );

				// Inform the client that a frame has been presented
				signal_action( );
			}

			signal_action( );
		}

		void discard( bool force = false )
		{
			if ( force || !futures_.empty( ) )
			{
				save( input_ );
				if ( !futures_.empty( ) )
				{
					input_ = load( futures_.front( ) );
					futures_.pop_front( );
				}
				else
				{
					input_ = ml::input_type_ptr( );
				}

				if ( auto_cue_ )
				{
					speed_ = 1;
					position_ = 0;
					pitch_ = 1.0;
				}
				generation_ ++;
			}
		}

		// Implementation for the 'remove' method
		void remove_item( int position )
		{
			if ( position == 0 )
			{
				if ( futures_.size( ) )
				{
					input_ = load( futures_.front( ) );
					futures_.pop_front( );
					speed_ = 1;
					pitch_ = 1.0;
					position_ = 0;
					generation_ ++;
				}
			}
			else if ( position > 0 )
			{
				if ( position <= int( futures_.size( ) ) )
				{
					futures_.erase( futures_.begin( ) + position - 1 );
					generation_ ++;
				}
			}
			else if ( position < 0 )
			{
				if ( - position <= int( history_.size( ) ) )
				{
					history_.erase( history_.end( ) + position );
					generation_ ++;
				}
			}
		}

		// Implementation for the 'select' method
		void select_item( int position )
		{
			bool active = false;

			if ( position == 0 )
			{
				active = true;
			}
			else if ( position > 0 )
			{
				if ( position <= int( futures_.size( ) ) )
				{
					active = true;

					save( input_ );

					for ( ; position > 1; position -- )
					{
						history_.push_back( futures_.front( ) );
						if ( history_.size( ) > capacity_ )
							history_.pop_front( );
						futures_.pop_front( );
					}

					input_ = load( futures_.front( ) );
					futures_.pop_front( );

					generation_ ++;
				}
			}
			else if ( position < 0 )
			{
				if ( - position <= int( history_.size( ) ) )
				{
					active = true;

					save( input_ );

					for ( ; position < 0; position ++ )
					{
						futures_.push_front( history_.back( ) );
						history_.pop_back( );
					}

					input_ = load( history_.back( ) );
					history_.pop_back( );

					generation_ ++;
				}
			}

			if ( active )
			{
				speed_ = 1;
				pitch_ = 1.0;
				position_ = 0;
			}
		}

		std::string obtain_item( int position )
		{
			std::string result;

			if ( position == 0 )
			{
				result = serialise( input_ );
			}
			else if ( position > 0 )
			{
				if ( position <= int( futures_.size( ) ) )
					result = futures_[ position - 1 ];
			}
			else if ( position < 0 )
			{
				if ( - position <= int( history_.size( ) ) )
					result = history_[ history_.size( ) + position ];
			}

			return result;
		}

		void after_item( int position, const ml::input_type_ptr &input )
		{
			if ( position >= 0 )
			{
				playlist_type::const_iterator iter = size_t( position ) < futures_.size( ) ? futures_.begin( ) + position : futures_.end( );
				futures_.insert( iter, serialise( input ) );
			}
			else if ( position < 0 )
			{
				playlist_type::const_iterator iter = size_t( - position ) < history_.size( ) ? history_.begin( ) + history_.size( ) + position : history_.end( );
				history_.insert( iter, serialise( input ) );
			}

			generation_ ++;
		}

		void overwrite_item( int position, const ml::input_type_ptr &input )
		{
			if ( position == 0 )
			{
				input_ = input;
			}
			else if ( position > 0 )
			{
				if ( size_t( position ) <= futures_.size( ) )
					futures_[ position - 1 ] = serialise( input );
				else
					futures_.push_back( serialise( input ) );
			}
			else if ( position < 0 )
			{
				if ( size_t( - position ) <= history_.size( ) )
					history_[ history_.size( ) + position ] = serialise( input );
				else
					history_.push_front( serialise( input ) );
			}

			generation_ ++;
		}

		// Fetch the current frame and cue up the following one
		ml::frame_type_ptr cue_position( bool &end_of_input, bool &error )
		{
			// Attach normalisers
			ml::frame_type_ptr result;
			ml::input_type_ptr graph = attach( );

			if ( !!graph )
			{
				// Obtain frame count and requested position
				int frames = graph->get_frames( );
				int requested = position_ < 0 ? 0 : position_ >= frames ? frames - 1 : position_;

				// Calculate next position
				position_ = requested + speed_;

				// Obtain frame
				ml::fetch_status status = graph->fetch( result, requested );

				// Indicate if we're at the end of the input and ready for the next in the playlist
				end_of_input = status == ml::fetch_eof || position_ >= frames;
				error = status == ml::fetch_error || !result || result->in_error( );

				// Report state of frame if not ok
				if ( end_of_input || error )
					CERR( "---> obtained frame: " << requested << ( end_of_input ? " EOF" : "" ) << ( error ? " ERROR" : "" ) );
			}
			else
			{
				end_of_input = true;
				error = true;
			}

			return result;
		}

		// Attach the normalising filters (should be configurable)
		ml::input_type_ptr attach( bool force_reconnect = false )
		{
			// Ensure we have a graph to attach to
			if ( !input_ ) return ml::input_type_ptr( );

			// Grab the head of the queue
			ml::input_type_ptr graph = input_;

			// Used to hold upstream and downstream normalisers
			ml::input_type_ptr upstream;
			ml::input_type_ptr downstream;

			// Create the filters if this is the first time through
			if ( normalisers_.size( ) == 0 )
			{
				// Define the normalising filters
				stack_ << graph << "normalise" >> downstream;

				// Determine contents of normaliser filters
				ml::input_type_ptr temp = downstream;
				auto key = pl::pcos::key::from_string( "@name" );
				while( !!temp && temp != graph )
				{
					std::string name = cl::str_util::to_string( pl::pcos::value< std::wstring >( temp->properties( ), key, temp->get_uri( ) ) );
					if ( name != "" )
					{
						normalisers_[ name ] = temp;
						CERR( "--> registered normalisation filter: " << name );
					}
					upstream = temp;
					temp = temp->fetch_slot( );
				}

				// Hold upstream and downstream for future use
				normalisers_[ "upstream" ] = upstream;
				normalisers_[ "downstream" ] = downstream;
			}
			else
			{
				// Pull out filters marked as upstream and downstream
				upstream = normalisers_[ "upstream" ];
				downstream = normalisers_[ "downstream" ];
			}

			// If the attached graph has changed, then disable the threader (if present) and
			// reconnect each filter to clear out caches
			if ( force_reconnect || upstream->fetch_slot( ) != graph )
			{
				filter_assign( "threader", "active", 0 );
				reconnect( graph, upstream, downstream );
				filter_assign( "threader", "active", 1 );
			}

			// Update the pitch and volume filters (if present)
			filter_assign( "pitch", "speed", pitch_ );
			filter_assign( "volume", "volume", volume_ );

			return downstream;
		}

		// Ensure that the graph meets some basic requirements
		bool test_graph( const ml::input_type_ptr &graph, lock_type &lock )
		{
			bool result = false;

			// get the current graph
			auto current = attach( );

			// Just in case we're out of range
			if ( !!current && current->get_position( ) >= current->get_frames( ) )
				current->seek( current->get_frames( ) - 1 );
			if ( !!graph && graph->get_position( ) >= graph->get_frames( ) )
				graph->seek( graph->get_frames( ) - 1 );

			// If graph or the current playing graph have any inputs/filters in common,
			// then we must retain the lock while testing it, otherwise we can safely
			// unlock it. Note that retaining the lock means blocking playout. To avoid
			// it, it is best to use 'current clone ... replace' type of logic.
			if ( !graph || graph->get_frames( ) <= 0 )
			{
				result = false;
			}
			else if ( !!current && ml::contains( current, graph ) )
			{
				if ( current->get_position( ) < graph->get_frames( ) )
					graph->seek( current->get_position( ) );
				result = test_graph( graph );
			}
			else
			{
				lock.unlock( );
				result = test_graph( graph );
				lock.lock( );
			}

			return result;
		}

		bool test_graph( const ml::input_type_ptr &graph )
		{
			ml::fetch_status status;

			try
			{
				ml::frame_type_ptr frame;
				status = graph->fetch( frame );
			}
			catch( ... )
			{
				status = ml::fetch_error;
			}

			return status == ml::fetch_ok;
		}

		// Assign the value to the specified normalisation filter's named property
		template< typename T >
		void filter_assign( const std::string &filter, const std::string name, T value )
		{
			auto iter = normalisers_.find( filter );
			if ( iter != normalisers_.end( ) )
			{
				auto prop = iter->second->property( name.c_str( ) );
				if ( prop.valid( ) && prop.is_a< T >( ) )
					prop = value;
			}
		}

		// Depth first recursion which connects graph to upstream, and each filter to
		// their upstream filter (this clears caches associated to the previous graph)
		void reconnect( const ml::input_type_ptr &graph, const ml::input_type_ptr &upstream, const ml::input_type_ptr &downstream )
		{
			if ( upstream == downstream )
			{
				upstream->connect( graph );
			}
			else
			{
				reconnect( graph, upstream, downstream->fetch_slot( ) );
				downstream->connect( downstream->fetch_slot( ) );
			}
		}

		void save( const ml::input_type_ptr &input )
		{
			if ( !!input )
			{
				history_.push_back( serialise( input ) );
				if ( history_.size( ) > capacity_ )
					history_.pop_front( );
			}
		}

		std::string serialise( const ml::input_type_ptr &input )
		{
			std::string dump;
			if ( !!input )
				stack_ << input << "filter:aml filename=@ $ stdout prop_query 1 roll drop" >> dump;
			return dump;
		}

		ml::input_type_ptr load( const std::string &graph )
		{
			ml::input_type_ptr result;
			stack_ << graph >> result;
			return result;
		}

		// Stores
		stores_type_ptr stores_;

		// Following members are protected by mutex_
		mutable boost::recursive_mutex mutex_;
		boost::condition_variable_any requested_;
		boost::condition_variable_any actioned_;
		bool shutdown_;
		ml::input_type_ptr input_;
		playlist_type history_;
		playlist_type futures_;
		int speed_;
		int position_;
		int generation_;
		double pitch_;
		double volume_;
		size_t capacity_;
		bool auto_cue_;
		boost::atomic< boost::thread::id > atom_id_;
		normaliser_type normalisers_;
		ml::stack stack_;
};

// Defines a shared pointer for the player
typedef std::shared_ptr< player_type > player_type_ptr;

// Factory method for player
player_type_ptr create_player( stores_type_ptr stores )
{
	return player_type_ptr( new player_type_impl( stores ) );
}

} }
