// io_lock.hpp - lock utility functionality for amldaemon
//
// Copyright (C) 2016 Charles Yates
// Released under the LGPL.

// Standard C++ Headers
#include <iostream>

// Boost Headers
#include <boost/thread.hpp>

namespace aml { namespace daemon {

// General thread stuff
typedef boost::unique_lock< boost::mutex > lock_type;

// Threadsafe std::cerr/cout usage
extern boost::mutex io_mutex;
#define IO_LOCK aml::daemon::lock_type lock( aml::daemon::io_mutex )
#define CERR( ... ) do { IO_LOCK; std::cerr << __VA_ARGS__ << std::endl; } while( 0 )
#define COUT( ... ) do { IO_LOCK; std::cout << __VA_ARGS__ << std::endl; } while( 0 )

// Mechanism to print a vector of strings
std::ostream &operator <<( std::ostream &os, const std::vector< std::string > &list );

} }
