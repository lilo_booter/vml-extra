// client.hpp - provides the base client interface
//
// Copyright (C) 2016 Charles Yates
// Released under the LGPL.

#pragma once

// Daemon Headers
#include "player.hpp"

namespace aml { namespace daemon {

namespace ml = olib::openmedialib::ml;
namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;

// Forward declarations here
class dictionaries_type;

// Interface that the words can use.
//
// Note that the only current implementation of a client_type is provided via
// session.hpp and session.cpp.
class client_type
{
	public:
		// Obtain the client's stack
		virtual ml::stack &stack( ) = 0;

		// Obtain the client's player
		virtual player_type_ptr player( ) = 0;

		// Write data to the client
		virtual void write( const std::string &data ) = 0;

		// Wait until all data is written
		virtual void flush( ) = 0;

		// Indicates if the client is still active
		virtual bool is_active( ) const = 0;

		// Allows stdout from commands to be redirected to the stack
		virtual void capture( int flag ) = 0;

		// Acquire the clients dictionaries
		virtual const dictionaries_type &dictionaries( ) const = 0;
};

// Convenience operators for accessing the client's stack
template< typename T >
inline ml::stack &operator <<( client_type &client, const T &value )
{
	return client.stack( ) << value;
}

template< >
inline ml::stack &operator <<( client_type &client, const status_type &status )
{
	std::stringstream ss;
	ss << status.speed << '\n' << status.position << '\n' << status.frames << '\n' << status.generation << '\n';
	client.write( ss.str( ) );
	return client.stack( );
}

template< typename T >
inline ml::stack &operator >>( client_type &client, T &value )
{
	return client.stack( ) >> value;
}

template< typename T >
inline T pop( client_type &client )
{
	return pop< T >( client.stack( ) );
}

template< >
inline ml::input_type_ptr pop( client_type &client )
{
	return pop< ml::input_type_ptr >( client.stack( ) );
}

#define DO_OR_DIE( client, something, ERROR ) do { { if ( !( something ) ) { client << "throw" << #ERROR; } } } while( 0 )

} }
