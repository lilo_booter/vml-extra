// stores.hpp - provides a means to collect a group of stores
//
// Copyright (C) 2016 Charles Yates
// Released under the LGPL.

#pragma once

// Daemon Headers
#include "io_lock.hpp"

// AML Headers
#include <openmedialib/ml/frame.hpp>
#include <openmedialib/ml/store.hpp>
#include <openpluginlib/pl/openpluginlib.hpp>
#include <opencorelib/cl/base_exception.hpp>
#include <ml/tool_utilities.hpp>

// C++ Headers
#include <map>
#include <vector>

namespace aml { namespace daemon {

namespace ml = olib::openmedialib::ml;
namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;

// The stores class parses the provided command line arguments via the add
// method. When a valid graph has been detected, the first frame from the graph
// can be used to convert the arguments to physical aml stores. Finally, the
// push method will present each pushed frame for playout.
class stores_type
{
	public:
		typedef std::vector< pl::pcos::property > property_list;

		virtual ~stores_type( ) { }

		// Indicates if the stores have been created yet
		virtual bool created( ) const = 0;

		// Add a store store to the stores class for use by the init method.
		virtual bool add( const std::string &token ) = 0;

		// Create the stores with the sample frame
		virtual bool create( const ml::frame_type_ptr &frame ) = 0;

		// Push the frame to the stores
		virtual bool push( const ml::frame_type_ptr &frame ) = 0;

		// Indicates if the stores allow deferred compositing
		virtual bool is_deferrable( ) const = 0;

		// Provides the list of keydown properties in case the app wants to monitor them
		virtual property_list keydown( ) const = 0;

		// Courtesy report
		virtual std::ostream &report( std::ostream &os ) = 0;

		// Modify store settings before or after creation
		virtual bool modify( const std::string &store, const std::string &token ) = 0;
};

// Shared pointer wrapper for stores_type
typedef std::shared_ptr< stores_type > stores_type_ptr;

// Factory method for the creating the stores_type
stores_type_ptr create_stores( );

} }
