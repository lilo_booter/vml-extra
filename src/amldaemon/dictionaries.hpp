// dictionaries.hpp - holds a collection of dictionaries

#pragma once

#include <openmedialib/ml/input.hpp>
#include <openmedialib/ml/filter.hpp>
#include <openmedialib/ml/stack.hpp>
#include <ml/stack_operators.hpp>

#include <string>
#include <map>
#include <list>
#include <ostream>

namespace aml { namespace daemon {

namespace ml = olib::openmedialib::ml;

// Forward declarations
class client_type;

// Public typedefs
typedef void ( *word_type )( client_type & );
typedef void ( *stack_word )( ml::stack & );
typedef std::map< std::string, word_type > dictionary_type;

// Helper macros for creating a dictionary
#ifdef OLIB_ON_MAC
#define CREATE_DICTIONARY( name ) const dictionary_type name = boost::assign::map_list_of
#define ADD_STATIC( NAME, VALUE ) ( NAME, VALUE )
#define ADD_LAMBDA( NAME, VALUE ) ( NAME, [ ]( client_type &client ) { VALUE } )
#define END_DICTIONARY
#else
#define CREATE_DICTIONARY( name ) const dictionary_type name = {
#define ADD_STATIC( NAME, VALUE ) { NAME, VALUE },
#define ADD_LAMBDA( NAME, VALUE ) { NAME, [ ]( client_type &client ) { VALUE } },
#define END_DICTIONARY }
#endif

// Holds collections of dictionary objects
class dictionaries_type
{
	public:
		typedef std::pair< std::string, const dictionary_type > entry_type;
		typedef std::list< entry_type > entries_type;
		typedef std::list< stack_word > words_list;

		void add( const std::string &description, const dictionary_type &dictionary )
		{
			entries_.push_back( entry_type( description, dictionary ) );

			for( auto iter : dictionary )
				dictionary_[ iter.first ] = iter.second;
		}

		void add( stack_word word )
		{
			bootstrap_.push_back( word );
		}

		void apply( ml::stack &stack ) const
		{
			for ( auto iter : bootstrap_ )
				stack << iter;
		}

		word_type find( const std::string &word ) const
		{
			word_type result = 0;

			auto iter = dictionary_.find( word );
			if ( iter != dictionary_.end( ) )
				result = iter->second;

			return result;
		}

		std::ostream &help( std::ostream &os ) const
		{
			for( auto iter : entries_ )
			{
				os << iter.first << ":\n\n";
				for ( auto word : iter.second )
					os << word.first << ' ';
				os << '\n' << '\n';
			}
			return os;
		}

	private:
		entries_type entries_;
		dictionary_type dictionary_;
		words_list bootstrap_;
};

} }
