// session.hpp - implementation of a client
//
// Copyright (C) 2016 Charles Yates
// Released under the LGPL.

#pragma once

// Daemon Headers
#include "client.hpp"

// Boost Headers
#include <boost/asio.hpp>

namespace aml { namespace daemon {

namespace ml = olib::openmedialib::ml;
namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;

// An aml session object provides each connected client with their own aml_stack
// and thread instances. All communications with the underlying aml_stack: input
// are done via the various properties which it exposes.
class session_type : public client_type, public boost::enable_shared_from_this< session_type >
{
	public:
		// Returns the socket associated to the session
		virtual boost::asio::ip::tcp::socket &socket( ) = 0;

		// Start the sessions thread and sets up the async socket reading.
		virtual void start( ) = 0;

		// Writes data to the stdout queue and triggers the do_stdout method if the
		// queue was previously empty.
		virtual void write( const std::string &data ) = 0;

		// Waits until all data is written
		virtual void flush( ) = 0;

		// Returns the stack associated to the session
		virtual ml::stack &stack( ) = 0;

		// Returns the player associated to the session
		virtual player_type_ptr player( ) = 0;

		// Checks if the server is running and client is still connected
		virtual bool is_active( ) const = 0;

		// Turns capture on and off. When capturing, the stdout of the executed
		// commands are added to the captured_ stringstream instead of being written
		// to the client. When it's turned off, the resultant data is added as a
		// string to the stack (thus bypassing parsing).
		virtual void capture( int flag ) = 0;

        // Acquire the clients dictionaries
        virtual const dictionaries_type &dictionaries( ) const = 0;
};

// Shared pointer wrapper for the session
typedef std::shared_ptr< session_type > session_type_ptr;

// Factory method to create a session
session_type_ptr create_session( boost::asio::io_service& io_service, player_type_ptr player, const dictionaries_type &dictionaries );

} }
