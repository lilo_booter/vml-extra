// amldaemon - an aml based media server
//
// Copyright (C) 2016 Charles Yates
// Released under the LGPL.
//
// Experimental media server - designed to replace amlshell's bless functionality.
//
// Usage:
//
// amldaemon [ --port=55378 ] [ --log=0 ] [ store [ name=value ]* ]*
//
// Connect with an amlclient using:
//
// amlclient [ --server=server:port ] [ --include=<aml-file> ]* [ cmd ... ]
//
// The clients have complete access to their own stack and thread. Through those they
// can control the daemon's player using the daemon commands and associated words or
// run reports or manipulate graphs via the same mechanism.

// Daemon Headers
#include "dictionaries.hpp"
#include "server.hpp"

// AML Headers
#include <opencorelib/cl/base_exception.hpp>
#include <ml/stack_operators.hpp>
#include <ml/tool_utilities.hpp>
#include <ml/reporter.hpp>

// Boost Headers
#include <boost/asio.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string/replace.hpp>

namespace aml { namespace daemon {

namespace ml = olib::openmedialib::ml;
namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;

// Local word definitions

// Provides some standard FORTH words - uses only aml stack primitives
static void basic_grammar( ml::stack &stack )
{
	stack
	<< ": dup 0 pick ;"
	<< ": swap 1 roll ;"
	<< ": over 1 pick ;"
	<< ": 2dup over over ;"
	<< ": rot 2 roll ;"
	<< ": 2rot rot rot ;"
	<< ": nip swap drop ;"
	<< ": tuck swap over ;"
	<< ": nl $ '' render ;"
	<< ": clear depth? pack drop ;"
	<< ": min over over > roll drop ;"
	<< ": max over over < roll drop ;"
	<< ": [ 0 pack ;"
	<< ": , pack& ;"
	<< ": ] , ;"
	<< ": { [ begin parse } until ;"
	<< ": } dup $ } = if drop 1 else dup $ { = if drop { , else ] then 0 then ;"
	;
}

// Display the top of stack - either on stdout if it's a value or play media
//
// 1 2 3 * + . => outputs 7
// $ hello . => outputs hello
// file.mp4 . => plays file.mp4
static void word_dot( client_type &client )
{
	ml::input_type_ptr input = pop< ml::input_type_ptr >( client );
	if ( !!input && input->get_frames( ) < 0 )
		client << input << "render";
	else
		DO_OR_DIE( client, client.player( )->play( input ), "Error in played graph" );
}

// Replaces the top of stack with a clone
//
// current clone <= replaces current with a safely modifiable clone
static void word_clone( client_type &client )
{
	std::string graph;
	client << "filter:aml filename=@ player.stdout? nip" >> graph << graph;
}

// Lists all the currently defined words
static void word_help( client_type &client )
{
	std::stringstream ss;
	client.dictionaries( ).help( ss );
	client.write( ss.str( ) );
	client << "dict";
}

// Dictionary registration - one liners expressed as lambdas, longer words above.
CREATE_DICTIONARY( daemon_dictionary )
	ADD_STATIC( ".", word_dot )
	ADD_STATIC( "help", word_help )
	ADD_STATIC( "daemon.clone", word_clone )
	ADD_LAMBDA( "daemon.capture", client.capture( pop< int >( client ) ); )
	ADD_LAMBDA( "daemon.shutdown", client.player( )->shutdown( ); )
	ADD_LAMBDA( "daemon.print", client.write( pop< std::string >( client ) + "\r" ); )
	ADD_LAMBDA( "daemon.system", int result = system( pop< std::string >( client ).c_str( ) ); client << result; )
END_DICTIONARY;

// String manipulation stuff

// Example custom word - determines if string matches a regex
static void word_contains( client_type &client )
{
	// <string> <regex> -- <string> <bool>
	std::string match;
	std::string value;

	// Pops match, duplicates tos, and pops value
	client >> match << "dup" >> value;

	// Create the regex
	boost::regex match_expr( match );
	boost::smatch what;

	// Push the result
	client << int( boost::regex_search( value, what, match_expr ) );
}

// Example custom word - splits a string into tokens
static void word_split( client_type &client )
{
	// <string> <delimiter> -- <token> ... <n>
	std::wstring delimiter = pop< std::wstring >( client );
	std::wstring data = pop< std::wstring >( client );

	auto keys = ml::reporter::split( data, delimiter );

	std::list< std::wstring > result;
	for( auto iter : keys )
	{
		result.push_back( L"$" );
		result.push_back( L"\"" + iter + L"\"" );
	}
	client.stack( ).push( result );

	client << int( keys.size( ) );
}

// Example custom word - joins a number of strings
static void word_join( client_type &client )
{
	// <string> ... <n> <delimiter> -- <string>
	std::wstring delimiter = pop< std::wstring >( client );
	int count = pop< int >( client );

	// Pop all the items off the stack
	std::vector< std::wstring > strings;
	while( count -- )
		strings.push_back( pop< std::wstring >( client ) );

	// We'll build the result here by iterating through strings in reverse order
	std::wstringstream ss;
	for( auto iter = strings.rbegin( ); iter != strings.rend( ); ++ iter )
		ss << *iter << ( iter + 1 != strings.rend( ) ? delimiter : L"" );

	// Return result to the stack
	std::list< std::wstring > result;
	result.push_back( L"$" );
	result.push_back( ss.str( ) );
	client.stack( ).push( result );
}

CREATE_DICTIONARY( string_dictionary )
	ADD_STATIC( "string.contains", word_contains )
	ADD_STATIC( "string.split", word_split )
	ADD_STATIC( "string.join", word_join )
	ADD_LAMBDA( "string.execute", client << pop< std::string >( client ); )
END_DICTIONARY;

// Store manipulation grammar

// Report the current state of the stores
static void word_store_report( client_type &client )
{
	std::stringstream os;
	client.player( )->stores( )->report( os );
	client.write( os.str( ) );
}

// Modify the state of the stores
static void word_store_modify( client_type &client )
{
	std::string token = pop< std::string >( client );
	std::string store = pop< std::string >( client );
	DO_OR_DIE( client, client.player( )->stores( )->modify( store, token ), "Unable to modify store" );
}

CREATE_DICTIONARY( store_dictionary )
	ADD_STATIC( "store.report", word_store_report )
	ADD_STATIC( "store.modify", word_store_modify )
END_DICTIONARY;

// PLAYER GRAMMAR

// Report on players normalisation filters
static void word_player_report( client_type &client )
{
	std::stringstream os;
	client.player( )->report( os );
	client.write( os.str( ) );
}

// Allow modification of players normalisation filters
static void word_player_modify( client_type &client )
{
	std::string token = pop< std::string >( client );
	std::string filter = pop< std::string >( client );
	DO_OR_DIE( client, client.player( )->modify( filter, token ), "Unable to modify filter" );
}

// Outputs an aml dump of the current playing media.
static void word_playing( client_type &client )
{
	ml::input_type_ptr current = client.player( )->current( );
	if ( !!current )
		client << current << "describe drop nl";
}

// Places the current playing graph on the stack. 'clone' can be used to make
// a safe copy to manipulate.
//
// current => current media graph is at the top of the stack
static void word_current( client_type &client )
{
	ml::input_type_ptr current = client.player( )->current( );
	if ( current )
		client << current;
	else
		client << "throw" << "\"there is currently nothing playing\"";
}

// Replace the current playing graph with top of stack. This is typically
// used in conjunction with current and clone.
//
// current clone filter:chroma replace <= clones and adds a chroma filter
static void word_replace( client_type &client )
{
	ml::input_type_ptr input = pop< ml::input_type_ptr >( client );
	if ( input->get_frames( ) >= 0 )
		DO_OR_DIE( client, client.player( )->replace( input, true ), "Error in replaced graph" );
	else
		client << input << "render";
}

// Replace the current playing graph with top of stack. This is typically
// used in conjunction with current and clone.
//
// current clone filter:chroma quash <= clones and adds a chroma filter
//
// Unlike replace, this will not save the previous graph.
static void word_quash( client_type &client )
{
	ml::input_type_ptr input = pop< ml::input_type_ptr >( client );
	if ( input->get_frames( ) >= 0 )
		DO_OR_DIE( client, client.player( )->replace( input, false ), "Error in replaced graph" );
	else
		client << input << "render";
}

// Sets the requested normaliser
static void word_set_normalise( client_type &client )
{
	std::string normalise = pop< std::string >( client );
	DO_OR_DIE( client, client.player( )->set_normalise( normalise ), "Attempt to set normaliser failed" );
}

static void word_player_info( client_type &client )
{
	auto player = client.player( );
	status_type status = player->status( );
	std::stringstream stream;
	std::string last;

	while( true )
	{
		stream << status.position << "," << status.frames << "," << status.speed << "," << status.generation << "," << player->volume( ) << "," << player->pitch( ) << "\n";
		if ( last != stream.str( ) )
		{
			client.write( stream.str( ) );
			last = stream.str( );
			client.flush( );
		}
		stream.str( "" );

		if ( client.is_active( ) )
		{
			player->wait( );
			status = player->status( );
		}
		else
		{
			break;
		}
	}
}

static void word_player_mq( client_type &client )
{
	auto player = client.player( );
	status_type status = player->status( );
	status_type old_status = status;
	std::stringstream stream;
	std::string last;
	bool first = true;

	while( true )
	{
		status.volume = player->volume( );
		status.pitch = player->pitch( );

		int count = 0;

		#define SEND( name ) if ( first || status.name != old_status.name ) { stream << #name << "=" << status.name << " "; count ++; }

		SEND( position );
		SEND( frames );
		SEND( speed );
		SEND( generation );
		SEND( volume );
		SEND( pitch );

		if ( count ) stream << "\n";

		#undef SEND

		first = false;
		old_status = status;

		if ( last != stream.str( ) )
		{
			client.write( stream.str( ) );
			last = stream.str( );
			client.flush( );
		}
		stream.str( "" );

		if ( client.is_active( ) )
		{
			player->wait( );
			status = player->status( );
		}
		else
		{
			break;
		}
	}
}

CREATE_DICTIONARY( player_dictionary )
	ADD_STATIC( "player.current", word_current )
	ADD_STATIC( "player.playing", word_playing )
	ADD_STATIC( "player.replace", word_replace )
	ADD_STATIC( "player.quash", word_quash )
	ADD_STATIC( "player.report", word_player_report )
	ADD_STATIC( "player.modify", word_player_modify )
	ADD_STATIC( "player.set_normalise", word_set_normalise )
	ADD_STATIC( "player.info", word_player_info )
	ADD_STATIC( "player.mq", word_player_mq )
	ADD_LAMBDA( "player.frames?", client << client.player( )->frames( ); )
	ADD_LAMBDA( "player.pitch?", client << client.player( )->pitch( ); )
	ADD_LAMBDA( "player.pitch", client.player( )->set_pitch( pop< double >( client ) ); )
	ADD_LAMBDA( "player.position?", client << client.player( )->position( ); )
	ADD_LAMBDA( "player.seek", client.player( )->seek( pop< int >( client ) ); )
	ADD_LAMBDA( "player.speed?", client << client.player( )->speed( ); )
	ADD_LAMBDA( "player.speed", client.player( )->set_speed( pop< int >( client ) ); )
	ADD_LAMBDA( "player.status?", client << client.player( )->status( ); )
	ADD_LAMBDA( "player.sync", client.player( )->sync( ); )
	ADD_LAMBDA( "player.volume?", client << client.player( )->volume( ); )
	ADD_LAMBDA( "player.volume", client.player( )->set_volume( pop< double >( client ) ); )
	ADD_LAMBDA( "player.block", client.player( )->block( ); )
	ADD_LAMBDA( "player.unblock", client.player( )->unblock( ); )
END_DICTIONARY;

// PLAYLIST GRAMMAR

// Adds media to the player's playlist or displays values immediately.
static void word_add( client_type &client )
{
	ml::input_type_ptr input = pop< ml::input_type_ptr >( client );
	if ( input->get_frames( ) < 0 )
		client << input << "render";
	else
		DO_OR_DIE( client, client.player( )->add( input ), "Error in added graph" );
}

// Lists all pending graphs as aml dumps
static void word_playlist( client_type &client )
{
	auto list = client.player( )->playlist( );
	for ( auto iter : list )
	{
		client.write( iter );
		client.write( "\n" );
	}
}

static void word_history( client_type &client )
{
	auto list = client.player( )->history( );
	for ( auto iter : list )
	{
		client.write( iter );
		client.write( "\n" );
	}
}

static void word_obtain( client_type &client )
{
	int position; 
	client >> position; 
	std::string graph = client.player( )->obtain( position ); 
	ARENFORCE_MSG( graph != "", "Unable to obtain graph for %d" )( position );
	client << graph;
}

static void word_after( client_type &client )
{
	int position;
	ml::input_type_ptr input;
	client >> position >> input; 
	client.player( )->after( position, input );
}

// Dictionary for the playlist
CREATE_DICTIONARY( playlist_dictionary )
	ADD_STATIC( "playlist.add", word_add )
	ADD_STATIC( "playlist.pending", word_playlist )
	ADD_STATIC( "playlist.history", word_history )
	ADD_STATIC( "playlist.obtain", word_obtain )
	ADD_STATIC( "playlist.after", word_after )
	ADD_LAMBDA( "playlist.next", client.player( )->next( ); )
	ADD_LAMBDA( "playlist.prev", client.player( )->prev( ); )
	ADD_LAMBDA( "playlist.auto_cue?", client << int( client.player( )->auto_cue( ) ); )
	ADD_LAMBDA( "playlist.auto_cue", int cue; client >> cue; client.player( )->set_auto_cue( bool( cue ) ); )
	ADD_LAMBDA( "playlist.select", int position; client >> position; client.player( )->select( position ); )
	ADD_LAMBDA( "playlist.remove", int position; client >> position; client.player( )->remove( position ); )
	ADD_LAMBDA( "playlist.overwrite", int position; ml::input_type_ptr input; client >> position >> input; client.player( )->overwrite( position, input ); )
	ADD_LAMBDA( "playlist.wipe", client.player( )->wipe( ); )
	ADD_LAMBDA( "playlist.wipe_history", client.player( )->wipe_history( ); )
	ADD_LAMBDA( "playlist.wipe_futures", client.player( )->wipe_futures( ); )
END_DICTIONARY;

static ml::input_type_ptr editor_find( ml::input_type_ptr &parent, int &slot, const ml::input_type_ptr &graph, const std::wstring &name )
{
	ml::input_type_ptr result;

	if ( !!graph && graph->get_uri( ) == name )
		result = graph;

	if ( !result && !!graph && graph->slot_count( ) > 0 )
	{
		for ( int index = 0; !result && index < int( graph->slot_count( ) ); index ++ )
		{
			result = editor_find( parent, slot, graph->fetch_slot( size_t( index ) ), name );
			if ( !!result && !parent )
			{
				parent = graph;
				slot = index;
			}
		}
	}

	return result;
}

static void word_editor_find( client_type &client )
{
	client.player( )->block( );

	std::wstring name = pop< std::wstring >( client );
	ml::input_type_ptr graph = pop< ml::input_type_ptr >( client );

	int slot = -1;
	ml::input_type_ptr parent;
	ml::input_type_ptr target = editor_find( parent, slot, graph, name );

	if ( !target )
		client << graph << "$" << name << "$" << "filter:%s" << "string.execute" << "dup" >> graph >> target;

	if ( !!parent )
		client << parent << ">r";
	else
		slot = -1;

	client << slot << ">r";
	client << graph << ">r";
	client << target;
}

static void word_editor_apply( client_type &client )
{
	ml::input_type_ptr graph;
	int slot;
	ml::input_type_ptr parent;
	ml::input_type_ptr target;

	client << "r>" >> graph;
	client << "r>" >> slot;
	if ( slot != -1 )
		client << "r>" >> parent;
	client >> target;

	if ( !!parent )
		parent->connect( target, size_t( slot ) );
	else
		graph = target;

	client << graph;

	client.player( )->unblock( );
}

typedef std::set< ml::input_type_ptr > annotated_type;

static ml::input_type_ptr graph_annotate( const ml::input_type_ptr &graph, const std::wstring &base, annotated_type &annotated )
{
	if ( graph && annotated.find( graph ) == annotated.end( ) )
	{
		pl::pcos::assign< std::wstring >( graph->properties( ), pl::pcos::key::from_string( "@label" ), base );
		annotated.insert( graph );

		const std::wstring common = base + ( base != L"" ? L" " : L"" );

		for ( size_t i = 0; i < graph->slot_count( ); i ++ )
		{
			const std::wstring label = common + boost::lexical_cast< std::wstring >( i );
			graph_annotate( graph->fetch_slot( i ), label, annotated );
		}
	}

	return graph;
}

typedef std::pair< ml::input_type_ptr, size_t > reconnection_type;
typedef std::set< reconnection_type > affected_type;

static bool graph_affected( const ml::input_type_ptr &graph, const ml::input_type_ptr &node, affected_type &affected, annotated_type &visited )
{
	bool result = graph == node;
	if ( !result && visited.find( graph ) == visited.end( ) )
	{
		visited.insert( graph );
		for ( size_t i = 0; i < graph->slot_count( ); i ++ )
		{
			if ( graph_affected( graph->fetch_slot( i ), node, affected, visited ) )
			{
				affected.insert( reconnection_type( graph, i ) );
				result = true;
			}
		}
	}
	return result;
}

static void word_editor_annotate( client_type &client )
{
	try
	{
		client.player( )->block( );
		ml::input_type_ptr graph;
		client >> graph;
		annotated_type annotated;
		client << graph_annotate( graph, L"", annotated );
		client.player( )->unblock( );
	}
	catch( ... )
	{
		client.player( )->unblock( );
		throw;
	}
}

static void word_editor_query( client_type &client )
{
	ml::input_type_ptr graph;
	std::string label;

	client 
	>> label 
	>> graph
	<< graph
	;

	size_t parent_slot = 0;
	ml::input_type_ptr parent;
	ml::input_type_ptr target = graph;

	std::istringstream input( label );

	while( target && input.good( ) )
	{
		size_t slot = 0xffff;
		input >> slot;
		if ( !input.fail( ) )
		{
			parent_slot = slot;
			parent = target;
			target = target->fetch_slot( slot );
		}
	}

	ARENFORCE_MSG( input.eof( ) && !!target, "Unable to locate %s in the graph" )( label );

	if ( !!parent )
		client << parent_slot << parent << target;
	else
		client << 0 << 0 << target;
}

static void word_editor_attach( client_type &client )
{
	try
	{
		client.player( )->block( );

		ml::input_type_ptr target;
		ml::input_type_ptr parent;
		size_t parent_slot;
		ml::input_type_ptr graph;

		client
		>> target
		>> parent
		>> parent_slot
		>> graph
		;

		if ( parent->get_frames( ) <= 0 )
			graph = target;
		else
			parent->connect( target, parent_slot );

		affected_type affected;
		annotated_type visited;

		if ( graph_affected( graph, target, affected, visited ) )
		{
			for ( auto iter = affected.begin( ); iter != affected.end( ); iter ++ )
			{
				const auto node = ( *iter ).first;
				const auto slot = ( *iter ).second;
				const auto child = node->fetch_slot( slot );
				node->connect( child, slot );
			}
		}

		annotated_type annotated;

		client
		<< graph_annotate( graph, L"", annotated )
		;

		client.player( )->unblock( );
	}
	catch( ... )
	{
		client.player( )->unblock( );
		throw;
	}
}

CREATE_DICTIONARY( editor_dictionary )
	ADD_STATIC( "editor.find", word_editor_find )
	ADD_STATIC( "editor.apply", word_editor_apply )
	ADD_STATIC( "editor.annotate", word_editor_annotate )
	ADD_STATIC( "editor.query", word_editor_query )
	ADD_STATIC( "editor.attach", word_editor_attach )
END_DICTIONARY;

// Input reporting grammar

static void replace_split( const std::string &replace, std::string &replace_pattern, std::string &replace_text )
{
	if ( replace != "" )
	{
		char divider = replace[ 0 ];
		size_t split = replace.find( divider, 1 );
		size_t end = replace.rfind( divider );
		if ( split != end && split != std::string::npos && end == replace.length( ) - 1 )
		{
			replace_pattern = replace.substr( 1, split - 1 );
			replace_text = replace.substr( split + 1, end - split - 1 );
			CERR( "--> replace split: " << replace << " becomes '" << replace_pattern << "' with '" << replace_text << "'" );
		}
	}
}

static bool replace_action( const boost::regex &pattern, const std::string &replace, std::string &text )
{
	text = boost::regex_replace( text, pattern, replace );
	return true;
}

static void report_run( client_type &client, const std::string &delimiter = ",", const std::string &lf = "\n" )
{
	static pl::pcos::key key_match( pl::pcos::key::from_string( "@match" ) );
	static pl::pcos::key key_replace( pl::pcos::key::from_string( "@replace" ) );

	ml::input_type_ptr report = pop< ml::input_type_ptr >( client );
	std::string spec = ml::convert< std::string >( report );
	std::string match = cl::str_util::to_string( pl::pcos::value< std::wstring >( report->properties( ), key_match, std::wstring( L"" ) ) );
	boost::regex match_expr( match );

	std::string replace = cl::str_util::to_string( pl::pcos::value< std::wstring >( report->properties( ), key_replace, std::wstring( L"" ) ) );
	std::string replace_pattern;
	std::string replace_text;
	replace_split( replace, replace_pattern, replace_text );
	boost::regex replace_expr( replace_pattern );

	ml::input_type_ptr input = pop< ml::input_type_ptr >( client );
	auto keys = ml::reporter::split( spec, delimiter );
	ml::frame_type_ptr frame;

	boost::smatch what;

	for( int i = 0; i < input->get_frames( ) && client.is_active( ); i ++ )
	{
		if ( input->fetch( frame, i ) != ml::fetch_ok ) break;
		std::string result = ml::reporter::report( keys, frame, delimiter, lf );
		if ( result.length( ) )
			if ( match == "" || boost::regex_search( result, what, match_expr ) )
				if ( replace == "" || replace_action( replace_expr, replace_text, result ) )
					client.write( result );
	}
}

static void word_report( client_type &client )
{
	report_run( client );
}

CREATE_DICTIONARY( report_dictionary )
	ADD_STATIC( "daemon.report", word_report )
	ADD_LAMBDA( "daemon.run", client << "$ position"; report_run( client, ",", "\r" ); client << "nl"; )
END_DICTIONARY;

// Provides some simple words for media playout using the various grammars
// defined above - these are generally more user friendly (well, sort of).
static void word_core_grammar( ml::stack &stack )
{
	stack
	<< ": player.stdout? $ stdout prop_query ;"
	<< ": current player.current ;"
	<< ": playing player.playing ;"
	<< ": replace player.replace ;"
	<< ": frames? player.frames? ;"
	<< ": pitch? player.pitch? ;"
	<< ": pitch player.pitch ;"
	<< ": position? player.position? ;"
	<< ": seek player.seek ;"
	<< ": speed? player.speed? ;"
	<< ": speed player.speed ;"
	<< ": shutdown daemon.shutdown ;"
	<< ": status? player.status? ;"
	<< ": sync player.sync ;"
	<< ": volume? player.volume? ;"
	<< ": volume player.volume ;"
	<< ": describe filter:aml filename=@ player.stdout? . decap ;"
	<< ": pause 0 player.speed ;"
	<< ": play 1 player.speed ;"
	<< ": ffwd 25 player.speed ;"
	<< ": rew -25 player.speed ;"
	<< ": reverse -1 player.speed ;"
	<< ": step position? + 0 max frames? min player.seek ;"
	<< ": toggle speed? if 0 else 1 then player.speed ;"
	<< ": mute 0 player.volume ;"
	<< ": unmute 1 player.volume ;"
	<< ": current_ratio player.position? player.frames? / ;"
	<< ": position% current_ratio 100 * ;"
	<< ": seek% 100 / player.frames? * floor player.seek ;"
	<< ": step% 100 / player.frames? * floor step ;"
	<< ": add playlist.add ;"
	<< ": playlist playlist.pending ;"
	<< ": next playlist.next ;"
	<< ": prev playlist.prev ;"
	<< ": add* pack iter_start playlist.add iter_slots drop ;"
	<< ": add$ depth? add* ;"
	<< ": [[ 1 daemon.capture ;"
	<< ": ]] 0 daemon.capture ;"
	<< ": execute$ string.execute ;"
	;
}

// We only support switches of the form --name=value at this point
template< typename T >
bool consume( const std::string &token, const std::string &pattern, T &var )
{
	size_t match = token.find( pattern );
	if ( match == 0 )
		var = boost::lexical_cast< T >( token.substr( pattern.size( ) ) );
	return match == 0;
}

// Exit during setup if necessary
void exit_on_fail( bool success, const std::string &message, int error = 2 )
{
	if ( !success )
	{
		CERR( "ERROR: " << message );
		exit( error );
	}
}

// Clean up when program is shutdown
void shutdown( )
{
	ml::uninit( );
	pl::uninit( );
}

void start( int log = 0 )
{
	// Initialise the logging
	pl::init_log( );
	pl::set_log_level( log );

	// Initialise openpluginlib now
	exit_on_fail( pl::init( ), "Failed to initialise opl", 1 );

	// Start up aml and register clean up on close
	exit_on_fail( ml::init( ), "Failed to initialise aml", 1 );

	// Clean up on exit
	atexit( shutdown );
}

int run( int argc, char **argv )
{
	int port = 55378;
	int log = 0;

	// These tokens will be used to construct the player's stores
	stores_type_ptr stores = create_stores( );

	// Parse the incoming arguments
	for( int i = 1; i < argc; i ++ )
	{
		std::string token( argv[ i ] );
		if ( consume( token, "--port=", port ) ) { }
		else if ( consume( token, "--log=", log ) ) { }
		else if ( stores->add( token ) ) { }
		else exit_on_fail( false, "Unidentified token: " + token );
	}

	// Initialise the aml system
	CERR( "> amldaemon starting on port " << port );
	start( log );

	// Create the asio service context
	boost::asio::io_service io_service;

	// Create the player instance
	player_type_ptr player( create_player( stores ) );

	// Register dictionaries
	dictionaries_type dictionaries;
	dictionaries.add( basic_grammar );
	dictionaries.add( "Daemon Words", daemon_dictionary );
	dictionaries.add( "Store Words", store_dictionary );
	dictionaries.add( "Player Words", player_dictionary );
	dictionaries.add( "Playlist Words", playlist_dictionary );
	dictionaries.add( "Editor Words", editor_dictionary );
	dictionaries.add( "String Words", string_dictionary );
	dictionaries.add( "Reporting Words", report_dictionary );
	dictionaries.add( word_core_grammar );

	// Create the asio tcp endpoint and register a server instance with the asio service
	// Note that even though server object isn't used here, it needs to remain in scope
	boost::asio::ip::tcp::endpoint endpoint( boost::asio::ip::tcp::v4( ), port );
	server_type server( io_service, endpoint, player, dictionaries );

	// Run as a background thread
	boost::thread service( [ &io_service ]( ) { io_service.run( ); } );

	// Run the player on the main thread (mostly for osx)
	player->run( );

	// Shutdown
	io_service.stop( );
	service.join( );

	CERR( "> amldaemon closing on port " << port );

	return 0;
}

} }

int main( int argc, char **argv )
{
	try
	{
		return aml::daemon::run( argc, argv );
	}
	catch( std::exception e )
	{
		std::cerr << "Exception: " << e.what( );
	}

	return 255;
}

