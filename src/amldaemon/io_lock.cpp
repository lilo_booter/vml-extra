// io_lock.cpp - lock utility functionality for amldaemon
//
// Copyright (C) 2016 Charles Yates
// Released under the LGPL.

// Daemon Headers
#include "io_lock.hpp"

#include <openpluginlib/pl/pcos/key.hpp>

namespace aml { namespace daemon {

// Threadsafe std::cerr/cout usage
boost::mutex io_mutex;

// Mechanism for print a vector of strings
std::ostream &operator <<( std::ostream &os, const std::vector< std::string > &list )
{
	for( auto iter : list )
		os << iter << " ";
	return os;
}

} }
