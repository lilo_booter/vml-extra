// player.hpp - the player class for the amldaemon
//
// Copyright (C) 2016 Charles Yates
// Released under the LGPL.

#pragma once

// Daemon Headers
#include "stores.hpp"

// AML Headers
#include <ml/stack_operators.hpp>

namespace aml { namespace daemon {

namespace ml = olib::openmedialib::ml;
namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;

// Provides current status info of playout
struct status_type
{
	status_type( int position_, int frames_, int speed_, int generation_ )
	: position( position_ )
	, frames( frames_ )
	, speed( speed_ )
	, generation( generation_ )
	, volume( 0.0 )
	, pitch( 0.0 )
	{
	}

	int position;
	int frames;
	int speed;
	int generation;
	float volume;
	float pitch;
};

// Media playing class
class player_type
{
	public:
		typedef std::deque< std::string > playlist_type;

		virtual ~player_type( ) { }

		// Introduces a new normalise word if required. Only takes effect if there
		// has been no attempt to play media.
		virtual bool set_normalise( std::string &script ) = 0;

		// Play input immediately
		virtual bool play( const ml::input_type_ptr &input ) = 0;

		// Add the input to the queue
		virtual bool add( const ml::input_type_ptr &input ) = 0;

		// Remove an item in the playlist - < 0 is a history item, 0 is current, > 0 is a future item
		virtual void remove( int position ) = 0;

		// Move in the playlist - < 0 is a history item, 0 is current, > 0 is a future item
		virtual void select( int position ) = 0;

		// Obtain specified item from the playlist 
		virtual std::string obtain( int position ) = 0;

		// Insert an item after the specified position
		virtual void after( int position, const ml::input_type_ptr &input ) = 0;

		// Overwrite the specified position with the graph
		virtual void overwrite( int position, const ml::input_type_ptr &input ) = 0;

		// Wipe the history and future playlist
		virtual void wipe( ) = 0;
		virtual void wipe_history( ) = 0;
		virtual void wipe_futures( ) = 0;

		// Move to the next input in the queue
		virtual void next( ) = 0;

		// Move to the prev input in the queue
		virtual void prev( ) = 0;

		// Return a copy of the current playlist
		virtual playlist_type playlist( ) const = 0;

		// Return a copy of the current history
		virtual playlist_type history( ) const = 0;

		// Determine whether auto cue is on or off
		virtual bool auto_cue( ) const = 0;

		// Set auto cue on or off
		virtual void set_auto_cue( bool cue ) = 0;

		// Replace the current playing input with the one specified
		virtual bool replace( const ml::input_type_ptr &input, bool save = true ) = 0;

		// Obtain the current playing input
		virtual ml::input_type_ptr current( ) const = 0;

		// Obtain the current speed
		virtual int speed( ) const = 0;

		// Set the speed
		virtual void set_speed( int speed ) = 0;

		// Obtain the current pitch
		virtual double pitch( ) const = 0;

		// Set the pitch
		virtual void set_pitch( double pitch ) = 0;

		// Obtain the current volume
		virtual double volume( ) const = 0;

		// Set the volume
		virtual void set_volume( double volume ) = 0;

		// Obtain the current position
		virtual int position( ) const = 0;

		// Seek to the requested position
		virtual void seek( int position ) = 0;

		// The generation is incremented each time the input changes
		virtual int generation( ) const = 0;

		// Return the current play status
		virtual status_type status( ) const = 0;

		// Shutdown the player
		virtual void shutdown( ) = 0;

		// Determines if the player is still running
		virtual bool is_running( ) const = 0;

		// Provide access to the stores collection
		virtual stores_type_ptr stores( ) const = 0;

		// Determines if the player and stores have been initialised
		virtual bool is_initialised( ) const = 0;

		// Sync
		virtual void sync( ) = 0;

		// Number of frames available to full graph
		virtual int frames( ) = 0;

		// Allows modification to the current playing graph
		virtual void block( ) = 0;

		// Release the lock
		virtual void unblock( ) = 0;

		// Play graphs until shutdown
		virtual void run( ) = 0;

		// Courtesy report
		virtual std::ostream &report( std::ostream &os ) = 0;

		// Modify normalisation filter settings after creation
		virtual bool modify( const std::string &filter, const std::string &token ) = 0;

		// Wait for an action to occur
		virtual void wait( ) = 0;

		// Force a signal to occur
		virtual void signal( ) = 0;
};

// Defines a shared pointer for the player
typedef std::shared_ptr< player_type > player_type_ptr;

// Factory method for player
player_type_ptr create_player( stores_type_ptr stores );

} }
