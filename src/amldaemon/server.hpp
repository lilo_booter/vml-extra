// server.hpp - server component for amldaemon
//
// Copyright (C) 2016 Charles Yates
// Released under the LGPL.

#pragma once

// Daemon Headers
#include "dictionaries.hpp"
#include "session.hpp"

// Boost Headers
#include <boost/asio.hpp>

namespace aml { namespace daemon {

namespace ml = olib::openmedialib::ml;
namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;
namespace asio = boost::asio;

// The aml server class simply accepts new requests and creates an amlsession for each
class server_type
{
	public:
		server_type( asio::io_service &io_service, const asio::ip::tcp::endpoint& endpoint, player_type_ptr player, const dictionaries_type &dictionaries )
		: io_service_( io_service )
		, acceptor_( io_service, endpoint )
		, player_( player )
		, dictionaries_( dictionaries )
		{
			do_accept( );
		}

	private:
		// Sets up the asynchronous accept mechanism
		void do_accept( )
		{
			#define AML_ACCEPT boost::system::error_code ec
			#define AML_ACCEPT_ARGS ec
			auto session = create_session( io_service_, player_, dictionaries_ );
			acceptor_.async_accept( session->socket( ), [ this, session ]( AML_ACCEPT ) { if ( run_accept( AML_ACCEPT_ARGS, session ) ) do_accept( ); } );
		}

		// Executed when a new connection is presented
		bool run_accept( boost::system::error_code ec, session_type_ptr session )
		{
			bool result = !ec;
			CERR( "-> start accept connection" );
			if ( result )
				session->start( );
			else
				CERR( "-> connection attempt failed with " << ec );
			CERR( "--> end accept connection" );
			return result;
		}

		boost::asio::io_service &io_service_;
		boost::asio::ip::tcp::acceptor acceptor_;
		player_type_ptr player_;
		const dictionaries_type &dictionaries_;
};

} }
