#ifdef WIN32
    #pragma warning(push)
    #pragma warning (disable : 4312 )
    #pragma warning (disable : 4355 ) // warning C4355: 'this' : used in base member initializer list
#endif

#include <openmedialib/ml/openmedialib_plugin.hpp>
#include <openpluginlib/pl/pcos/isubject.hpp>
#include <openpluginlib/pl/pcos/observer.hpp>

extern "C" {
#include <goom/goom.h>
}

#include <iostream>

#ifdef WIN32
    #include <windows.h>
    #include <winbase.h>
#endif // WIN32

namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;
namespace pcos = olib::openpluginlib::pcos;
namespace ml = olib::openmedialib::ml;

namespace olib { namespace openmedialib { namespace ml { 

// pcos property observer
template < typename C > class fn_observer : public pcos::observer
{
	public:
		fn_observer( C *instance, void ( C::*fn )( ) )
			: instance_( instance )
			, fn_( fn )
		{ }

		virtual void updated( pcos::isubject * )
		{ ( instance_->*fn_ )( ); }

	private:
		C *instance_;
		void ( C::*fn_ )( );
};

#define const_goom_filter const_cast< goom_filter * >

class goom_filter : public filter_type
{
	public:
		typedef fn_observer< goom_filter > observer;

		goom_filter( )
		: prop_enable_( pcos::key::from_string( "enable" ) )
		, prop_force_( pcos::key::from_string( "force" ) )
		, prop_width_( pcos::key::from_string( "width" ) )
		, prop_height_( pcos::key::from_string( "height" ) )
		, prop_effect_( pcos::key::from_string( "effect" ) )
		, prop_sar_num_( pcos::key::from_string( "sar_num" ) )
		, prop_sar_den_( pcos::key::from_string( "sar_den" ) )
		, prop_type_( pcos::key::from_string( "type" ) )
		, prop_pf_( pcos::key::from_string( "colourspace" ) )
		, goom_( 0 )
		, obs_effect_( new observer( const_goom_filter( this ), &goom_filter::effect ) )
		, prev0_( 0 )
		, ro_ ( ml::rescale_object_ptr( new ml::image::rescale_object( ) ) )
		{
			properties( ).append( prop_enable_ = 1 );
			properties( ).append( prop_force_ = 0 );
			properties( ).append( prop_width_ = 640 );
			properties( ).append( prop_height_ = 360 );
			properties( ).append( prop_effect_ = 6 );
			properties( ).append( prop_sar_num_ = 1 );
			properties( ).append( prop_sar_den_ = 1 );
			properties( ).append( prop_type_ = 0 );
			properties( ).append( prop_pf_ = std::wstring( L"yuv420p" ) );
			prop_effect_.attach( obs_effect_ );
		}

		virtual ~goom_filter( )
		{
			if ( goom_ )
				goom_close( goom_ );
		}

		virtual std::wstring get_uri( ) const { return L"goom"; }

		virtual distributable_status is_distributable( ) { return distributable_no; }

	protected:
		ml::fetch_status do_fetch( frame_type_ptr &result )
		{
			AR_TRY_FETCH( fetch_from_slot( result ) );
			if ( result && prop_enable_.value< int >( ) )
				if ( ( prop_force_.value< int >( ) || !result->has_image( ) ) && result->has_audio( ) )
					goom_it( result );
			return ml::fetch_ok;
		}

	private:
		void effect( )
		{
			if ( goom_ != 0 )
			{
				goom_close( goom_ );
				goom_ = 0;
				prev0_ = 0;
			}
		}

		void goom_it( frame_type_ptr &result )
		{
			int width = prop_width_.value< int >( );
			int height = prop_height_.value< int >( );
			int effect = 0;

			result = result->shallow( );

			if ( goom_ == 0 )
			{
				goom_ = goom_init( width, height );
				image_ = ml::image::allocate( image::ML_PIX_FMT_B8G8R8A8, width, height );
				memset( image_->writable_ptr( 0 ), 0, image_->total_bytes( ) );
				goom_set_screenbuffer( goom_, image_->writable_ptr( 0 ) );
				effect = prop_effect_.value< int >( );
			}
			else
			{
				width = image_->width( );
				height = image_->height( );
			}

			audio_type_ptr aud = audio::pitch( result->get_audio( ), 512 );
			aud = ml::audio::coerce( audio::pcm16_id, aud );

			gint16 array[ 2 ][ 512 ];
			short *ptr = ( short * )aud->pointer( );

			if ( array[ 0 ][ 0 ] == prev0_ )
			{
				for ( int i = 0; i < 1024; i ++, ptr ++ )
					array[ i % 2 ][ i / 2 ] = ( ( *ptr < 0 ? - *ptr : *ptr ) * 31 ) >> 5;
			}
			else
			{
				for ( int i = 0; i < 1024; i ++, ptr ++ )
					array[ i % 2 ][ i / 2 ] = *ptr;
			}
			prev0_ = array[ 0 ][ 0 ];

			const std::string song = pl::pcos::value< std::string >( result->properties( ), pl::pcos::key::from_string( "format.title" ), "" );
			const std::string artist = pl::pcos::value< std::string >( result->properties( ), pl::pcos::key::from_string( "format.artist" ), "" );

			const std::string title = song != "" && artist != "" ? song + "\n" + artist + "\n" : "";

			char *str = title != last_title_ ? ( char * )title.c_str( ) : 0;
			last_title_ = title;

			guint32 *img = goom_update( goom_, array, effect, 0.0, 0, str );

			ml::image_type_ptr copy = ml::image::allocate( image::ML_PIX_FMT_B8G8R8A8, width, height );
			memcpy( copy->writable_ptr( 0 ), img, image_->total_bytes( ) );

			result->set_image( std::move( copy ) );
			result->set_sar( prop_sar_num_.value< int >( ), prop_sar_den_.value< int >( ) );

			result = ml::frame_convert( ro_, result, cl::str_util::to_t_string( prop_pf_.value< std::wstring >( ) ) );
			result->set_alpha( ml::image_type_ptr( ) );
		}

	private:
		pcos::property prop_enable_;
		pcos::property prop_force_;
		pcos::property prop_width_;
		pcos::property prop_height_;
		pcos::property prop_effect_;
		pcos::property prop_sar_num_;
		pcos::property prop_sar_den_;
		pcos::property prop_type_;
		pcos::property prop_pf_;
		PluginInfo *goom_;
		ml::image_type_ptr image_;
		std::shared_ptr< pcos::observer > obs_effect_;
		short prev0_;
		ml::rescale_object_ptr ro_;
		std::string last_title_;
};

//
// Plugin object
//

class ML_PLUGIN_DECLSPEC goom_plugin : public openmedialib_plugin
{
public:
	virtual input_type_ptr input( const io::request & )
	{
		return ml::input_type_ptr( );
	}

	virtual store_type_ptr store( const io::request &, const frame_type_ptr & )
	{
		return ml::store_type_ptr( );
	}

	virtual filter_type_ptr filter( const std::wstring & )
	{
		return filter_type_ptr( new goom_filter( ) );
	}
};

} } }

//
// Access methods for openpluginlib
//

extern "C"
{
	ML_PLUGIN_DECLSPEC bool openplugin_init( void )
	{
		return true;
	}

	ML_PLUGIN_DECLSPEC bool openplugin_uninit( void )
	{
		return true;
	}
	
	ML_PLUGIN_DECLSPEC bool openplugin_create_plugin( const char*, pl::openplugin** plug )
	{
		*plug = new ml::goom_plugin;
		return true;
	}
	
	ML_PLUGIN_DECLSPEC void openplugin_destroy_plugin( pl::openplugin* plug )
	{ 
		delete static_cast< ml::goom_plugin * >( plug ); 
	}
}

#ifdef WIN32 
    #pragma warning(pop)
#endif
