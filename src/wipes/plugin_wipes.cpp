// wipes - A wipes plugin to ml.
//
// Copyright (C) 2009 Charles Yates
// Released under the LGPL.
//
// #input:wipe_:
//
// Produces a luma wipe. The input actually looks for:
//
// wipe_<spec>:
//
// where spec is either one of the precanned definitions - loosely based on the
// SMPTE spec - or a custom specification.
//
// Some simple SMPTE examples being:
//
// amlbatch wipe_001:
// amlbatch wipe_002:
// etc
//
// There's also an RPN based custom wipe generator which is ... weird. Not
// going to explain it here, but to trigger it, you need to follow the
// vague rules of:
//
// wipe_custom_<shape>[_modifier]*:
//
// For example:
//
// amlbatch wipe_custom_circle_fold_fold:
//
// On its own, this is a fundamentally useless bit of functionality. To make
// any use of it, you need to use filter:wipe_:
//
// #filter:wipe_
//
// Applies a luma wipe to an input. As with the wipe_ input, it takes a spec to
// select the wipe which is interpreted by input:wipe_ to generate the correct 
// wipe.
//
// As an example, this will show a blue 6 pointed star on a red background:
//
// colour: r=255
// colour: b=255 out=50
// filter:wipe_star6 mix=0.4 softness=0.1
// filter:compositor
//
// The amount of mixing is typically controlled by an upstream filter via a
// filter:lerp or filter:bezier or similar.
//
// For example:
//
// colour: r=255
// colour: b=255 out=100
// filter:lerp @@wipe=0:1:0:25
// filter:wipe_001 @mix=@@wipe:1
// filter:compositor
// filter:repeat count=10 type=pingpong

#define _USE_MATH_DEFINES
#include <cmath>

#include <openmedialib/ml/openmedialib_plugin.hpp>
#include <openpluginlib/pl/pcos/observer.hpp>
#include <opencorelib/cl/str_util.hpp>
#include <opencorelib/cl/enforce_defines.hpp>

#include <boost/algorithm/string.hpp>

#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
#include <sstream>

namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;
namespace ml = olib::openmedialib::ml;
namespace il = olib::openmedialib::ml::image;
namespace pcos = olib::openpluginlib::pcos;

namespace olib { namespace openmedialib { namespace ml { namespace wipes {

static inline float modf( float value, bool integral = false )
{
	float dummy;
	float result = fabs( std::modf( value, &dummy ) );
	return integral ? dummy : result;
}

static inline void swap( float &a, float &b )
{
	float t = a;
	a = b;
	b = t;
}

namespace modifiers
{
	typedef enum
	{
		face,
		facex,
		facey,
		flip,
		flop,
		fracture,
		fracture_x,
		fracture_y,
		mirror,
		mirror_x,
		mirror_y,
		swap,
		zoom,
		zoom_x,
		zoom_y,
	}
	pixel;

	typedef enum
	{
		invert,
		fold,
		floor,
		mult,
		multx,
		multy,
		mult_max,
		mult_min,
		mult_foldx,
		mult_foldy,
		minusx,
		minusy,
		minusxy,
		arctan,
		arcsin,
		arccos,
		radius,
		band4,
		band8,
		band16,
		test,
		sinx,
		polygon3,
		polygon4,
		polygon5,
		polygon6,
		polygon7,
		polygon8,
		polygon9,
		sqrt,
		square,
		sqrtx,
		sqrty,
		sqrtxy,
	}
	sample;
}

class shape
{
	public:
		virtual ~shape( ) { }
		virtual float sample( float x, float y, float ar ) const = 0;
};

typedef std::shared_ptr< shape > shape_ptr;

class none : public shape
{
	public:
		float sample( float, float, float ) const
		{ return 1.0f; }
};

class half : public shape
{
	public:
		float sample( float, float, float ) const
		{ return 0.5f; }
};

class random : public shape
{
	public:
		float sample( float, float, float ) const
		{ return float( rand( ) ) / RAND_MAX; }
};

class circle : public shape
{
	public:
		float sample( float x, float y, float ar ) const
		{ return 1.0f - sqrt( pow( x - 0.5f, 2.0f ) + pow( y - 0.5f, 2.0f ) / ar ) / float( M_SQRT1_2 ); }
};

class oval : public shape
{
	public:
		float sample( float x, float y, float ) const
		{ return 1.0f - sqrt( pow( x - 0.5f, 2.0f ) + pow( y - 0.5f, 2.0f ) ) / float( M_SQRT1_2 ); }
};

class sweep : public shape
{
	public:
		float sample( float x, float y, float ) const
		{
			float result = fabs( atan( ( x - 0.5f ) / ( y - 0.5f ) ) / float( M_PI_2 ) );
			return result >= 0.0f && result <= 1.0f ? result : fmod( result, 1.0f );
		}
};

class nclock : public shape
{
	public:
		nclock( int sides )
		: sides_( sides )
		{ }

		float sample( float x, float y, float ) const
		{
			float angle = atan2( x - 0.5f, y - 0.5f );
			angle = angle + ( angle < 0.0f ? 2.0f * float( M_PI ) : 0.0f );
			float result = angle / ( 2.0f * float( M_PI ) / sides_ );
			return result >= 0.0f && result <= 1.0f ? result : fmod( result, 1.0f );
		}

	private:
		const int sides_;
};

class polygon : public shape
{
	public:
		polygon( int sides )
		: sides_( sides )
		, angle_side_( 2.0f * float( M_PI ) / sides_ )
		, max_( 0.0f )
		{ }

		float sample( float x, float y, float ar ) const
		{
			if ( max_ == 0.0f ) max_ = std::max< float >( calc( 0.0f, 0.0f, ar ), calc( 1.0f, 1.0f, ar ) );
			return calc( x, y, ar ) / max_;
		}

		float calc( float x, float y, float ar ) const
		{
			x -= 0.5f; y -= 0.5f;
			x *= ar;
			float angle = atan2( x, y );
			angle = angle + ( angle < 0.0f ? 2.0f * float( M_PI ) : 0.0f );
			int side = int( angle / angle_side_ );
			angle -= angle_side_ * side;
			float r = sqrt( x * x + y * y );
			float ratio = sin( float( M_PI_2 ) - angle + angle_side_ / 2.0f ) / sin( ( float( M_PI ) - angle_side_ ) / 2.0f );
			return fabs( ratio * r );
		}

	private:
		const int sides_;
		const float angle_side_;
		mutable float max_;
};

class snake : public shape
{
	public:
		snake( float bands )
		: bands_( bands )
		{
		}

		float sample( float a, float b, float ) const
		{
			float band = modf( b * bands_, true );
			int direction = int( band ) % 2 ? 1 : -1;
			float lower = ( 1.0f / bands_ ) * band;
			float upper = ( 1.0f / bands_ ) * ( band + 1.0f );
			if ( direction == -1 ) swap( lower, upper );
			return lower + a * ( upper - lower );
		}

	private:
		float bands_;
};

class star : public shape
{
	public:
		star( int sides )
		: tri_( sides )
		, max_( 0.0f )
		{
		}

		float sample( float x, float y, float ar ) const
		{
			if ( max_ == 0.0f ) max_ = std::max< float >( calculate( 0.0f, 0.0f, ar ), calculate( 0.0f, 0.5f, ar ) );
			return calculate( x, y, ar ) / max_;
		}

		float calculate( float x, float y, float ar ) const
		{
			return std::min< float >( tri_.sample( x, y, ar ), tri_.sample( x, 1 - y, ar ) );
		}

	private:
		polygon tri_;
		mutable float max_;
};

class board : public shape
{
	public:
		board( float sw, float sh, bool alt = false )
		: sw_( sw )
		, sh_( sh )
		, alt_( alt )
		{ }

		float sample( float x, float y, float ar ) const
		{
			float colw = 1.0f / ( sw_ * sh_ );
			float w = 1.0f / sw_;
			float h = 1.0f / sh_;
			int col = static_cast< int >( floor( x / w ) );
			int row = static_cast< int >( floor( y / h ) );
			if ( alt_ && row % 2 == 1 )
				col = static_cast< int >( floor( 1.0f / w ) - col );
			return row * h + col * colw;
		}

		float sw_;
		float sh_;
		bool alt_;
};

// Abstract shape class
class stack
{
	public:
		stack( shape_ptr shape )
		: shape_( shape )
		{ }

		void push( modifiers::pixel value )
		{ pixels_.push_back( value ); }

		void push( modifiers::sample value )
		{ samples_.push_back( value ); }

		// The main public method - generate an image of the requested width and height
		ml::image_type_ptr generate( int width, int height, int num, int den )
		{
			// Allocate a floating point image
			ml::image_type_ptr image = il::allocate( _CT( "l16le" ), width, height );
			int pitch = ( image->pitch( 0 ) - image->linesize( 0 ) ) / 2;
			boost::uint16_t *ptr = reinterpret_cast< boost::uint16_t * >( image->writable_ptr( 0 ) );
			float ar = float( width * num ) / float( height * den );

			// Populate the image
			for ( int y = 0; y < height; y ++, ptr += pitch )
				for ( int x = 0; x < width; x ++ )
					*ptr ++ = int( 65535.0 * calculate( x, y, width, height, ar ) );

			return image;
		}

	private:
		inline float calculate( int px, int py, int w, int h, float ar ) const
		{
			// Calculate default values
			float x = float( px ) / ( w - 1 );
			float y = float( py ) / ( h - 1 );

			// Deal with pixel position modifiers first
			for ( std::deque< modifiers::pixel >::const_iterator iter = pixels_.begin( ); iter != pixels_.end( ); iter ++ )
			{
				switch( *iter )
				{
					case modifiers::face:			y *= 2.0f; y = ( y >= 1.0f ? 2.0f - y : y );
					case modifiers::facex:			x *= 2.0f; x = ( x >= 1.0f ? 2.0f - x : x ); break;
					case modifiers::facey:			y *= 2.0f; y = ( y >= 1.0f ? 2.0f - y : y ); break;
					case modifiers::flip:			x = 1.0f - x; break;
					case modifiers::flop:			y = 1.0f - y; break;
					case modifiers::fracture:		y = modf( 2.0f * y );
					case modifiers::fracture_x:		x = modf( 2.0f * x ); break;
					case modifiers::fracture_y:		y = modf( 2.0f * y ); break;
					case modifiers::mirror:			y = y < 0.5f ? y : 1.0f - y;
					case modifiers::mirror_x:		x = x < 0.5f ? x : 1.0f - x; break;
					case modifiers::mirror_y:		y = y < 0.5f ? y : 1.0f - y; break;
					case modifiers::swap:			{ float t = x; x = y; y = t; ar = 1.0f / ar; } break;
					case modifiers::zoom:			y = y / 2.0f + 0.25f;
					case modifiers::zoom_x:			x = x / 2.0f + 0.25f; break;
					case modifiers::zoom_y:			y = y / 2.0f + 0.25f; break;
				}
			}

			// Get the value
			float result = fabs( shape_->sample( x, y, ar ) );

			// Deal with result/order sensitive modifiers next
			for ( std::deque< modifiers::sample >::const_iterator iter = samples_.begin( ); iter != samples_.end( ); iter ++ )
			{
				switch( *iter )
				{
					case modifiers::invert:		result = 1.0f - result; break;
					case modifiers::fold:		result = fabs( 2.0f * result - 1.0f ); break;
					case modifiers::floor:		result = modf( 2.0f * result ); break;
					case modifiers::mult:		result *= ( x + y ) / 2.0f; break;
					case modifiers::multx:		result *= x; break;
					case modifiers::multy:		result *= y; break;
					case modifiers::mult_max:	result *= x > y ? x : y; break;
					case modifiers::mult_min:	result *= x < y ? x : y; break;
					case modifiers::mult_foldx:	result *= modf( 2.0f * x ); break;
					case modifiers::mult_foldy:	result *= modf( 2.0f * y ); break;
					case modifiers::minusx:		result *= 1.0f - x; break;
					case modifiers::minusy:		result *= 1.0f - y; break;
					case modifiers::minusxy:	result *= 1.0f - ( ( x + y ) / 2.0f ); break;
					case modifiers::arctan:		result *= modf( fabs( atan2( x - 0.5f, y - 0.5f ) / float( M_PI_4 ) ) ); break;
					case modifiers::arcsin:		result *= fabs( asin( fabs( ( x - 0.5f ) / ( y - 0.5f ) ) / float( M_PI_2 ) ) ); break;
					case modifiers::arccos:		result *= fabs( acos( fabs( ( x - 0.5f ) / ( y - 0.5f ) ) / float( M_PI_2 ) ) ); break;
					case modifiers::radius:		result *= 1.0f - sqrt( pow( x - 0.5f, 2.0f ) + pow( y - 0.5f, 2.0f ) ) / float( M_SQRT1_2 ); break;
					case modifiers::band4:		result = floor( result * 4.0f ) / 4.0f; break;
					case modifiers::band8:		result = floor( result * 8.0f ) / 8.0f; break;
					case modifiers::band16:		result = floor( result * 16.0f ) / 16.0f; break;
					case modifiers::test:		result *= ( x * y ); break;
					case modifiers::sinx:		result = sin( result * float( M_PI_4 ) ); break;
					case modifiers::polygon3:	result *= polygon( 3 ).sample( x, y, ar ); break;
					case modifiers::polygon4:	result *= polygon( 4 ).sample( x, y, ar ); break;
					case modifiers::polygon5:	result *= polygon( 5 ).sample( x, y, ar ); break;
					case modifiers::polygon6:	result *= polygon( 6 ).sample( x, y, ar ); break;
					case modifiers::polygon7:	result *= polygon( 7 ).sample( x, y, ar ); break;
					case modifiers::polygon8:	result *= polygon( 8 ).sample( x, y, ar ); break;
					case modifiers::polygon9:	result *= polygon( 9 ).sample( x, y, ar ); break;
					case modifiers::sqrt:		result = pow( result, 0.5f ); break;
					case modifiers::square:		result *= result; break;
					case modifiers::sqrtx:		result *= pow( x, 0.5f ); break;
					case modifiers::sqrty:		result *= pow( y, 0.5f ); break;
					case modifiers::sqrtxy:		result *= pow( x * y, 0.5f ); break;
				}
			}

			return result;
		}

		shape_ptr shape_;
		std::deque< modifiers::pixel > pixels_;
		std::deque< modifiers::sample > samples_;
};

typedef std::shared_ptr< stack > stack_ptr;

stack_ptr create_stack( shape_ptr shape )
{
	return stack_ptr( new stack( shape ) );
}

static stack_ptr calculator( stack_ptr current, const std::wstring &command )
{
	if ( current == 0 )
	{
		shape *s = 0;

		if ( command == L"circle" ) 		s = new circle( );
		else if ( command == L"clock" ) 	s = new nclock( 1 );
		else if ( command == L"clock2" ) 	s = new nclock( 2 );
		else if ( command == L"clock3" ) 	s = new nclock( 3 );
		else if ( command == L"clock4" ) 	s = new nclock( 4 );
		else if ( command == L"clock5" ) 	s = new nclock( 5 );
		else if ( command == L"clock6" ) 	s = new nclock( 6 );
		else if ( command == L"clock7" ) 	s = new nclock( 7 );
		else if ( command == L"half" ) 		s = new half( );
		else if ( command == L"none" ) 		s = new none( );
		else if ( command == L"oval" ) 		s = new oval( );
		else if ( command == L"sweep" ) 	s = new sweep( );
		else if ( command == L"triangle" ) 	s = new polygon( 3 );
		else if ( command == L"square" ) 	s = new polygon( 4 );
		else if ( command == L"pentagon" ) 	s = new polygon( 5 );
		else if ( command == L"hexagon" ) 	s = new polygon( 6 );
		else if ( command == L"heptagon" ) 	s = new polygon( 7 );
		else if ( command == L"octagon" ) 	s = new polygon( 8 );
		else if ( command == L"random" ) 	s = new random( );
		else if ( command == L"unity" ) 	s = new none( );
		else if ( command == L"star6" )		s = new star( 3 );
		else if ( command == L"star10" ) 	s = new star( 5 );
		else if ( command == L"star14" ) 	s = new star( 7 );
		else if ( command == L"snake4" ) 	s = new snake( 4 );
		else if ( command == L"snake6" ) 	s = new snake( 6 );
		else if ( command == L"snake8" ) 	s = new snake( 8 );
		else if ( command == L"board4" ) 	s = new board( 4, 4 );
		else if ( command == L"board6" ) 	s = new board( 6, 6 );
		else if ( command == L"board8" ) 	s = new board( 8, 8 );
		else if ( command == L"aboard4" ) 	s = new board( 4, 4, true );
		else if ( command == L"aboard6" ) 	s = new board( 6, 6, true );
		else if ( command == L"aboard8" ) 	s = new board( 8, 8, true );

		if ( current == 0 && s == 0 )
		{
			std::cerr << "Unrecognised shape " << cl::str_util::to_string( command ) << " defaulting to none" << std::endl;
			s = new none( );
		}

		if ( s ) current = create_stack( shape_ptr( s ) );
	}
	else
	{
		if ( command == L"face" ) 			current->push( modifiers::face );
		else if ( command == L"facex" ) 	current->push( modifiers::facex );
		else if ( command == L"facey" ) 	current->push( modifiers::facey );
		else if ( command == L"flip" ) 		current->push( modifiers::flip );
		else if ( command == L"flop" ) 		current->push( modifiers::flop );
		else if ( command == L"fracture" ) 	current->push( modifiers::fracture );
		else if ( command == L"fracturex" ) current->push( modifiers::fracture_x );
		else if ( command == L"fracturey" ) current->push( modifiers::fracture_y );
		else if ( command == L"mirror" ) 	current->push( modifiers::mirror );
		else if ( command == L"mirrorx" ) 	current->push( modifiers::mirror_x );
		else if ( command == L"mirrory" ) 	current->push( modifiers::mirror_y );
		else if ( command == L"swap" ) 		current->push( modifiers::swap );
		else if ( command == L"zoom" ) 		current->push( modifiers::zoom );
		else if ( command == L"zoomx" )		current->push( modifiers::zoom_x );
		else if ( command == L"zoomy" ) 	current->push( modifiers::zoom_y );
		else if ( command == L"invert" ) 	current->push( modifiers::invert );
		else if ( command == L"fold" ) 		current->push( modifiers::fold );
		else if ( command == L"floor" ) 	current->push( modifiers::floor );
		else if ( command == L"mult" ) 		current->push( modifiers::mult );
		else if ( command == L"multmax" )	current->push( modifiers::mult_max );
		else if ( command == L"multmin" )	current->push( modifiers::mult_min );
		else if ( command == L"multx" ) 	current->push( modifiers::multx );
		else if ( command == L"multy" ) 	current->push( modifiers::multy );
		else if ( command == L"minusx" ) 	current->push( modifiers::minusx );
		else if ( command == L"minusy" ) 	current->push( modifiers::minusy );
		else if ( command == L"minusxy" ) 	current->push( modifiers::minusxy );
		else if ( command == L"multfoldx" )	current->push( modifiers::mult_foldx );
		else if ( command == L"multfoldy" )	current->push( modifiers::mult_foldy );
		else if ( command == L"arctan" )	current->push( modifiers::arctan );
		else if ( command == L"arcsin" )	current->push( modifiers::arcsin );
		else if ( command == L"arccos" )	current->push( modifiers::arccos );
		else if ( command == L"radius" )	current->push( modifiers::radius );
		else if ( command == L"band4" )		current->push( modifiers::band4 );
		else if ( command == L"band8" )		current->push( modifiers::band8 );
		else if ( command == L"band16" )	current->push( modifiers::band16 );
		else if ( command == L"test" )		current->push( modifiers::test );
		else if ( command == L"sinx" )		current->push( modifiers::sinx );
		else if ( command == L"polygon3" )	current->push( modifiers::polygon3 );
		else if ( command == L"polygon4" )	current->push( modifiers::polygon4 );
		else if ( command == L"polygon5" )	current->push( modifiers::polygon5 );
		else if ( command == L"polygon6" )	current->push( modifiers::polygon6 );
		else if ( command == L"polygon7" )	current->push( modifiers::polygon7 );
		else if ( command == L"polygon8" )	current->push( modifiers::polygon8 );
		else if ( command == L"polygon9" )	current->push( modifiers::polygon9 );
		else if ( command == L"sqrt" )		current->push( modifiers::sqrt );
		else if ( command == L"square" )	current->push( modifiers::square );
		else if ( command == L"sqrtx" )		current->push( modifiers::sqrtx );
		else if ( command == L"sqrty" )		current->push( modifiers::sqrty );
		else if ( command == L"sqrtxy" )	current->push( modifiers::sqrtxy );
		else std::cerr << "Unrecognised modifier " << cl::str_util::to_string( command ) << std::endl;
	}

	return current;
}

static stack_ptr parse( const std::wstring &command )
{
	stack_ptr result;
	size_t index = 0;

	while( index != std::wstring::npos )
	{
		size_t next = command.find( L"_", index );
		if ( next != std::wstring::npos )
		{
			result = calculator( result, command.substr( index, next - index ) );
			next ++;
		}
		else
		{
			result = calculator( result, command.substr( index ) );
		}
		index = next;
	}

	return result;
}

static stack_ptr parse( const std::string &command )
{
	return parse( cl::str_util::to_wstring( command ) );
}

std::map< std::wstring, stack_ptr > shapes_;

static void init( )
{
	shapes_[ L"" ] = parse( "none" );

	shapes_[ L"wipe_001:" ] = parse( "unity_multx" );
	shapes_[ L"wipe_002:" ] = parse( "unity_multy" );
	shapes_[ L"wipe_003:" ] = parse( "unity_multmax" );
	shapes_[ L"wipe_004:" ] = parse( "unity_multmax_flip" );
	shapes_[ L"wipe_005:" ] = parse( "unity_multmax_flop" );
	shapes_[ L"wipe_006:" ] = parse( "unity_multmax_flip_flop" );
	shapes_[ L"wipe_007:" ] = parse( "unity_multmax_face" );
	shapes_[ L"wipe_008:" ] = parse( "unity_multmin_face_face" );
	shapes_[ L"wipe_021:" ] = parse( "unity_multx_facex" );
	shapes_[ L"wipe_022:" ] = parse( "unity_multy_facey" );
	shapes_[ L"wipe_023:" ] = parse( "unity_multmin_facex_flop" );
	shapes_[ L"wipe_024:" ] = parse( "unity_multmin_facey" );
	shapes_[ L"wipe_025:" ] = parse( "unity_multmin_facex" );
	shapes_[ L"wipe_026:" ] = parse( "unity_multmin_facey_flip" );

	shapes_[ L"wipe_041:" ] = parse( "unity_mult" );
	shapes_[ L"wipe_042:" ] = parse( "unity_mult_flip" );
	shapes_[ L"wipe_043:" ] = parse( "unity_mult_face_flip" );
	shapes_[ L"wipe_044:" ] = parse( "unity_mult_face_flop" );
	shapes_[ L"wipe_045:" ] = parse( "unity_mult_fold_invert" );
	shapes_[ L"wipe_046:" ] = parse( "unity_mult_fold_invert_flip" );
	shapes_[ L"wipe_047:" ] = parse( "unity_mult_fold_invert_face_flip" );
	shapes_[ L"wipe_048:" ] = parse( "unity_mult_fold_invert_face" );
	shapes_[ L"wipe_061:" ] = parse( "unity_mult_facex_flop" );
	shapes_[ L"wipe_062:" ] = parse( "unity_mult_flip_facey_flop_invert" );
	shapes_[ L"wipe_063:" ] = parse( "unity_mult_facex" );
	shapes_[ L"wipe_064:" ] = parse( "unity_mult_flip_facey" );
	shapes_[ L"wipe_065:" ] = parse( "unity_mult_fold_invert_facex_flop" );
	shapes_[ L"wipe_066:" ] = parse( "unity_mult_fold_invert_facey_flop_flip" );
	shapes_[ L"wipe_067:" ] = parse( "unity_mult_fold_invert_facex" );
	shapes_[ L"wipe_068:" ] = parse( "unity_mult_fold_invert_facey_flop" );
	shapes_[ L"wipe_071:" ] = parse( "unity_mult_facey_facey_facey_flip" );
	shapes_[ L"wipe_072:" ] = parse( "unity_mult_facex_facex_facex_flop" );
	shapes_[ L"wipe_073:" ] = parse( "unity_mult_facex_facey_facey_facey_flip" );
	shapes_[ L"wipe_074:" ] = parse( "unity_mult_facey_facex_facex_facex_flop" );
	shapes_[ L"wipe_101:" ] = parse( "unity_multmin_face" );
	shapes_[ L"wipe_102:" ] = parse( "unity_mult_face" );

	shapes_[ L"wipe_box_1:" ] = parse( "unity_multmin_face_floor_floor" );
	shapes_[ L"wipe_box_2:" ] = parse( "unity_multmin_face_fold_fold" );

	shapes_[ L"wipe_circle:" ] = parse( "circle_invert" );
	shapes_[ L"wipe_circle_1:" ] = parse( "circle_invert_floor_floor" );
	shapes_[ L"wipe_circle_2:" ] = parse( "circle_invert_floor_floor_floor" );
	shapes_[ L"wipe_circle_3:" ] = parse( "circle_invert_fracture" );
	shapes_[ L"wipe_circle_4:" ] = parse( "circle_invert_floor_floor_fracture" );
	shapes_[ L"wipe_circle_5:" ] = parse( "circle_invert_fold" );
	shapes_[ L"wipe_circle_6:" ] = parse( "circle_invert_fold_fold" );
	shapes_[ L"wipe_circle_7:" ] = parse( "circle_invert_fold_fold_fold" );

	shapes_[ L"wipe_clock:" ] = parse( "clock" );
	shapes_[ L"wipe_clock_1:" ] = parse( "clock_flop" );
	shapes_[ L"wipe_clock_2:" ] = parse( "clock_flip" );
	shapes_[ L"wipe_clock_3:" ] = parse( "clock_flip_flop" );
	shapes_[ L"wipe_clock_4:" ] = parse( "clock_swap" );
	shapes_[ L"wipe_clock_5:" ] = parse( "clock_swap_flop" );
	shapes_[ L"wipe_clock_6:" ] = parse( "clock_floor" );
	shapes_[ L"wipe_clock_7:" ] = parse( "clock_floor_floor" );
	shapes_[ L"wipe_clock_8:" ] = parse( "clock_fold" );
	shapes_[ L"wipe_clock_9:" ] = parse( "clock_fold_flop" );
	shapes_[ L"wipe_clock_a:" ] = parse( "clock_fold_swap" );
	shapes_[ L"wipe_clock_b:" ] = parse( "clock_fold_swap_flop" );

	shapes_[ L"wipe_clock3:" ] = parse( "clock3" );
	shapes_[ L"wipe_clock3_1:" ] = parse( "clock3_fold" );

	shapes_[ L"wipe_clock5:" ] = parse( "clock5" );
	shapes_[ L"wipe_clock5_1:" ] = parse( "clock5_fold" );

	shapes_[ L"wipe_clock7:" ] = parse( "clock7" );
	shapes_[ L"wipe_clock7_1:" ] = parse( "clock7_fold" );

	shapes_[ L"wipe_oval:" ] = parse( "oval" );
	shapes_[ L"wipe_ovals:" ] = parse( "oval_floor_floor_floor" );
	shapes_[ L"wipe_ovals_floor:" ] = parse( "oval_floor_floor" );
	shapes_[ L"wipe_oval_quad:" ] = parse( "oval_fracture" );
	shapes_[ L"wipe_ovals_quad:" ] = parse( "oval_floor_floor_fracture" );

	shapes_[ L"wipe_rhombus_1:" ] = parse( "unity_mult_face_fold_fold_fold" );
	shapes_[ L"wipe_rhombus_2:" ] = parse( "unity_mult_face_fold_fold_fold_fracture" );

	shapes_[ L"wipe_sweep:" ] = parse( "sweep" );
	shapes_[ L"wipe_sweep_1:" ] = parse( "sweep_fold" );
	shapes_[ L"wipe_sweep_2:" ] = parse( "sweep_fold_fold" );
	shapes_[ L"wipe_sweep_3:" ] = parse( "sweep_swap" );
	shapes_[ L"wipe_sweep_4:" ] = parse( "sweep_floor" );
	shapes_[ L"wipe_sweep_5:" ] = parse( "sweep_floor_floor" );

	shapes_[ L"wipe_snake4:" ] = parse( "snake4" );
	shapes_[ L"wipe_snake6:" ] = parse( "snake6" );
	shapes_[ L"wipe_snake8:" ] = parse( "snake8" );

	shapes_[ L"wipe_board4:" ] = parse( "board4" );
	shapes_[ L"wipe_board6:" ] = parse( "board6" );
	shapes_[ L"wipe_board8:" ] = parse( "board8" );

	shapes_[ L"wipe_aboard4:" ] = parse( "aboard4" );
	shapes_[ L"wipe_aboard6:" ] = parse( "aboard6" );
	shapes_[ L"wipe_aboard8:" ] = parse( "aboard8" );

	shapes_[ L"wipe_star6:" ] = parse( "star6" );
	shapes_[ L"wipe_star10:" ] = parse( "star10" );
	shapes_[ L"wipe_star14:" ] = parse( "star14" );
}

// Input wrapper for source types
class ML_PLUGIN_DECLSPEC input_wipe : public input_type
{
	public:
		// Constructor and destructor
		input_wipe( const std::wstring &spec )
			: input_type( )
			, spec_( spec )
			, prop_out_( pcos::key::from_string( "out" ) )
			, prop_fps_num_( pcos::key::from_string( "fps_num" ) )
			, prop_fps_den_( pcos::key::from_string( "fps_den" ) )
			, prop_width_( pcos::key::from_string( "width" ) )
			, prop_height_( pcos::key::from_string( "height" ) )
			, prop_sar_num_( pcos::key::from_string( "sar_num" ) )
			, prop_sar_den_( pcos::key::from_string( "sar_den" ) )
		{
			properties( ).append( prop_out_ = 1 );
			properties( ).append( prop_fps_num_ = 25 );
			properties( ).append( prop_fps_den_ = 1 );
			properties( ).append( prop_width_ = 720 );
			properties( ).append( prop_height_ = 576 );
			properties( ).append( prop_sar_num_ = 118 );
			properties( ).append( prop_sar_den_ = 81 );
		}

		virtual ~input_wipe( )
		{
		}

		// Indicates if the input will enforce a packet decode
		virtual bool requires_image( ) const { return true; }

		// Basic information
		virtual std::wstring get_uri( ) const { return spec_; }
		virtual const std::wstring get_mime_type( ) const { return L""; }

		// Audio/Visual
		virtual int get_frames( ) const { return prop_out_.value< int >( ); }
		virtual bool is_seekable( ) const { return true; }
		virtual bool determine_complete( ) const { return true; }

		// Visual
		virtual void get_fps( int &num, int &den ) const { num = prop_fps_num_.value< int >( ); den = prop_fps_den_.value< int >( ); }
		virtual void get_sar( int &num, int &den ) const { num = prop_sar_num_.value< int >( ); den = prop_sar_den_.value< int >( ); }
		virtual int get_video_streams( ) const { return 1; }
		virtual int get_width( ) const { return prop_width_.value< int >( ); }
		virtual int get_height( ) const { return prop_height_.value< int >( ); }

		// Audio
		virtual int get_audio_streams( ) const { return 0; }

	protected:
		// Fetch method
		ml::fetch_status do_fetch( frame_type_ptr &result )
		{
			// Construct a frame and populate with basic information
			result = frame_type_ptr( new frame_type( ) );

			int num, den;

			get_fps( num, den );
			result->set_fps( num, den );

			result->set_position( get_position( ) );

			get_sar( num, den );
			result->set_sar( num, den );

			ml::image_type_ptr image;
			int width = prop_width_.value< int >( );
			int height = prop_height_.value< int >( );

			if ( spec_.find( L"wipe_custom_" ) == 0 )
				image = parse_custom( spec_, width, height );
			else if ( shapes_.find( spec_ ) != shapes_.end( ) )
				image = shapes_.find( spec_ )->second->generate( width, height, num, den );
			else
				image = shapes_.find( L"" )->second->generate( width, height, num, den );

			result->set_image( boost::move( image ) );

			pcos::assign< std::wstring >( result->properties( ), pcos::key::from_string( "title" ), spec_ );

			return ml::fetch_ok;
		}

		ml::image_type_ptr parse_custom( const std::wstring command, int width, int height )
		{
			int num, den;
			get_sar( num, den );

			if ( custom_ == 0 )
			{
				const std::wstring tag( L"wipe_custom_" );
				std::wstring chain = command.substr( tag.size( ), command.size( ) - tag.size( ) - 1 );
				custom_ = parse( chain );
			}

			return custom_ ? custom_->generate( width, height, num, den ) : ml::image_type_ptr( );
		}

	private:
		std::wstring spec_;
		pcos::property prop_out_;
		pcos::property prop_fps_num_;
		pcos::property prop_fps_den_;
		pcos::property prop_width_;
		pcos::property prop_height_;
		pcos::property prop_sar_num_;
		pcos::property prop_sar_den_;
		std::wstring custom_last_;
		stack_ptr custom_;
};

inline float smoothstep( const float e1, const float e2, const float a )
{
	if ( a < e1 ) return 0.0f;
	if ( a > e2 ) return 1.0f;
	float v = ( a - e1 ) / ( e2 - e1 );
	return ( v * v * ( 3.0f - 2.0f * v ) );
}

inline float lerp( const float e1, const float e2, const float a )
{
	return e1 * ( 1.0f - a ) + e2 * a;
}


class ML_PLUGIN_DECLSPEC filter_wipe : public filter_type
{
	public:
		filter_wipe( const std::wstring &spec )
			: filter_type( )
			, spec_( spec )
			, prop_enable_( pl::pcos::key::from_string( "enable" ) )
			, prop_mix_( pl::pcos::key::from_string( "mix" ) )
			, prop_softness_( pl::pcos::key::from_string( "softness" ) )
			, prop_invert_( pl::pcos::key::from_string( "invert" ) )
			, prop_show_alpha_( pl::pcos::key::from_string( "show_alpha" ) )
		{
			properties( ).append( prop_enable_ = 1 );
			properties( ).append( prop_mix_ = 1.0 );
			properties( ).append( prop_softness_ = 0.05 );
			properties( ).append( prop_invert_ = 0 );
			properties( ).append( prop_show_alpha_ = 0 );
		}

		~filter_wipe( )
		{
		}

		// Indicates if the input will enforce a packet decode
		virtual bool requires_image( ) const { return true; }

		virtual std::wstring get_uri( ) const { return spec_; }

		virtual size_t slot_count( ) const { return 1; }

		virtual ml::distributable_status is_distributable( ) { return ml::distributable_should; }

	protected:
		int get_alpha_depth( const ml::frame_type_ptr &frame ) const
		{
			int depth = 8;
			if ( frame->get_alpha( ) )
				depth = frame->get_alpha( )->bitdepth( );
			else if ( frame->get_image( ) )
				depth = frame->get_image( )->bitdepth( );
			return depth;
		}

		ml::fetch_status do_fetch( frame_type_ptr &result )
		{
			AR_TRY_FETCH( fetch_from_slot( result, 0 ) );

			if ( spec_ != L"wipe_none" && prop_enable_.value< int >( ) )
			{
				result = result->shallow( );

				if ( !mask_ && result->has_image( ) )
				{
					std::wstring temp = spec_ + L":";
					ml::input_type_ptr input = create_delayed_input( temp );
					ARENFORCE_MSG( input, "Failed to create wipe %s" )( temp );
					input->property( "fps_num" ) = result->get_fps_num( );
					input->property( "fps_den" ) = result->get_fps_den( );
					input->property( "sar_num" ) = result->get_sar_num( );
					input->property( "sar_den" ) = result->get_sar_den( );
					input->property( "width" ) = result->get_image( )->width( );
					input->property( "height" ) = result->get_image( )->height( );
					ARENFORCE_MSG( input->init( ), "Failed to initialise wipe %s" )( temp );
					ml::frame_type_ptr frame;
					AR_TRY_FETCH( input->fetch( frame ) );
					mask_ = frame->get_image( )->shallow( );
					ARENFORCE_MSG( mask_, "Unable to obtain an image from %s" )( temp );
				}

				ml::image_type_ptr alpha;
				int alpha_depth = get_alpha_depth( result );

				if ( alpha_depth == 8 )
					alpha = apply_mask< boost::uint8_t >( result, 255 );
				else if ( alpha_depth > 8 )
					alpha = apply_mask< boost::uint16_t >( result, 65535 );

				ARENFORCE_MSG( alpha, "No alpha created for alpha of %d bits" )( alpha_depth );

				if ( !prop_show_alpha_.value< int >( ) )
				{
					result->set_alpha( boost::move( alpha ) );
				}
				else
				{
					result->set_image( boost::move( alpha ) );
					result->set_alpha( ml::image_type_ptr( ) );
				}
			}

			return ml::fetch_ok;
		}

		template< typename T >
		ml::image_type_ptr apply_mask( ml::frame_type_ptr &result, const float div )
		{
			float mix = float( prop_mix_.value< double >( ) );
			float softness = float( prop_softness_.value< double >( ) );

			const boost::uint16_t *src = reinterpret_cast< const boost::uint16_t * >( mask_->ptr( 0 ) );
			int src_pitch = mask_->pitch( 0 );
			int src_linesize = mask_->linesize( 0 );
			int width = result->get_image( )->width( );
			int height = result->get_image( )->height( );
			int invert = prop_invert_.value< int >( );
			const int src_rem = ( src_pitch - src_linesize ) / 2;

			mix = mix * ( 1.0f + softness );

			float a = 1.0f;
			float b = 1.0f;

			if ( result->get_alpha( ) )
			{
				ml::image_type_ptr alpha = result->take_alpha( );
				ml::image::make_writable( alpha );
				T *dst = reinterpret_cast< T * >( alpha->writable_ptr( 0 ) );
				const int dst_pitch = alpha->pitch( 0 );
				const int dst_rem = ( dst_pitch - alpha->linesize( 0 ) ) / ( div == 255 ? 1 : 2 );

				while( height -- )
				{
					if ( invert )
					{
						for( int x = 0; x < width; x ++ )
						{
							a = 1.0f - float( *src ++ ) / 65535.0f;
							b = smoothstep( a, a + softness, mix );
							*dst = T( *dst * b );
							dst ++;
						}
					}
					else
					{
						for( int x = 0; x < width; x ++ )
						{
							a = float( *src ++ ) / 65535.0f;
							b = smoothstep( a, a + softness, mix );
							*dst = T( *dst * b );
							dst ++;
						}
					}
					src += src_rem;
					dst += dst_rem;
				}

				return boost::move( alpha );
			}
			else
			{
				ml::image_type_ptr alpha = il::allocate( div == 255.0f ? _CT( "l8" ) : _CT( "l16le" ), width, height );
				T *dst = ( T * )alpha->writable_ptr( 0 );
				const int dst_pitch = alpha->pitch( 0 );
				const int dst_rem = ( dst_pitch - alpha->linesize( 0 ) ) / ( div == 255 ? 1 : 2 );

				while( height -- )
				{
					if ( invert )
					{
						for( int x = 0; x < width; x ++ )
						{
							a = 1.0f - float( *src ++ ) / 65535.0f;
							b = smoothstep( a, a + softness, mix );
							*dst = T( b * div );
							dst ++;
						}
					}
					else
					{
						for( int x = 0; x < width; x ++ )
						{
							a = float( *src ++ ) / 65535.0f;
							b = smoothstep( a, a + softness, mix );
							*dst = T( b * div );
							dst ++;
						}
					}
					src += src_rem;
					dst += dst_rem;
				}
				return boost::move( alpha );
			}
		}

	private:
		std::wstring spec_;
		pcos::property prop_enable_;
		pcos::property prop_mix_;
		pcos::property prop_softness_;
		pcos::property prop_invert_;
		pcos::property prop_show_alpha_;
		ml::image_type_ptr mask_;
};

static ml::input_type_ptr all_wipes( )
{
	ml::input_type_ptr stack = ml::create_input( L"aml_stack:" );
	for ( std::map< std::wstring, stack_ptr >::const_iterator iter = shapes_.begin( ); iter != shapes_.end( ); iter ++ )
	{
		if ( iter->first != L"" )
		{
			stack->property( "command" ) = iter->first;
			stack->property( "command" ) = std::wstring( L"width=320" );
			stack->property( "command" ) = std::wstring( L"height=180" );
			stack->property( "command" ) = std::wstring( L"sar_num=1" );
			stack->property( "command" ) = std::wstring( L"sar_den=1" );
		}
	}
	stack->property( "command" ) = std::wstring( L"depth?" );
	stack->property( "command" ) = std::wstring( L"pack" );
	stack->property( "command" ) = std::wstring( L"." );
	return stack->fetch_slot( 0 );
}

// Aditional factory methods
ml::filter_type_ptr ML_PLUGIN_DECLSPEC create_blend( const std::wstring & );
ml::filter_type_ptr ML_PLUGIN_DECLSPEC create_show_alpha( const std::wstring & );
ml::filter_type_ptr ML_PLUGIN_DECLSPEC create_target( const std::wstring & );

//
// Plugin object
//

class ML_PLUGIN_DECLSPEC plugin : public openmedialib_plugin
{
public:
	virtual filter_type_ptr filter( const std::wstring &spec )
	{
		if ( spec == L"blend" )
			return create_blend( spec );
		else if ( spec == L"show_alpha" )
			return create_show_alpha( spec );
		else if ( spec == L"target" )
			return create_target( spec );
		return filter_type_ptr( new filter_wipe( spec ) );
	}

	virtual input_type_ptr input( const io::request &request )
	{
		std::wstring spec = cl::str_util::to_wstring( request.uri( ) );
		if ( spec == L"wipes:" )
			return all_wipes( );
		return input_type_ptr( new input_wipe( spec ) );
	}

	virtual store_type_ptr store( const io::request &, const frame_type_ptr & )
	{
		return store_type_ptr( );
	}
};

} } } }

//
// Access methods for openpluginlib
//

extern "C"
{
	ML_PLUGIN_DECLSPEC bool openplugin_init( void )
	{
		static bool init = false;
		if ( !init )
		{
			ml::wipes::init( );
			init = true;
		}
		return true;
	}

	ML_PLUGIN_DECLSPEC bool openplugin_uninit( void )
	{
		return true;
	}

	ML_PLUGIN_DECLSPEC bool openplugin_create_plugin( const char*, pl::openplugin** plug )
	{
		*plug = new ml::wipes::plugin;
		return true;
	}

	ML_PLUGIN_DECLSPEC void openplugin_destroy_plugin( pl::openplugin* plug )
	{
		delete static_cast< ml::wipes::plugin * >( plug );
	}
}
