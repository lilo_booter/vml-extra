// Blends the frame on to a background of its own dimensions or a predefined spec.
//
// Copyright (C) 2016 Vizrt
// Released under the terms of the LGPL.
//
// #filter:show_alpha
//
// Simplistic filter for showing the alpha mask when it exists.

#include <openmedialib/ml/openmedialib_plugin.hpp>

namespace olib { namespace openmedialib { namespace ml { namespace wipes {

class ML_PLUGIN_DECLSPEC filter_show_alpha : public ml::filter_simple
{
	public:
		filter_show_alpha( )
		: prop_enable_( pl::pcos::key::from_string( "enable" ) )
		{
			properties( ).append( prop_enable_ = 1 );
		}

		virtual std::wstring get_uri( ) const { return L"show_alpha"; }
		ml::distributable_status is_distributable( ) { return ml::distributable_should; }

	protected:
		ml::fetch_status do_fetch( ml::frame_type_ptr &result )
		{
			// Attempt to fetch result from the connected slot
			AR_TRY_FETCH( fetch_from_slot( result ) );

			if ( prop_enable_.value< int >( ) && !!result->get_alpha( ) )
			{
				result->set_image( result->get_alpha( )->shallow( ) );
				result->set_alpha( ml::image_type_ptr( ) );
			}

			return ml::fetch_ok;
		}

	private:
		pl::pcos::property prop_enable_;
};

ml::filter_type_ptr ML_PLUGIN_DECLSPEC create_show_alpha( const std::wstring &resource )
{
	return ml::filter_type_ptr( new filter_show_alpha( ) );
}

} } } }
