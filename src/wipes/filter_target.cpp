// Target
//
// Copyright (C) 2018 Charles Yates
// Released under the LGPL.
//
// #filter:target
//
// Allows filter chains to run on a specific region of a timeline or input.
//
// Examples of use:
//
// <input>
// filter:chroma
// filter:target in=50 out=100
//
// Would apply the chroma filter over frames 50 to 99 inclusive.
//
// <input>
// filter:chroma
// filter:charcoal
// filter:target in=50 out=100
//
// Would apply the charcoal filter over frames 50 to 99 inclusive. Chroma is 
// used throughout.
//
// <input>
// filter:chroma
// filter:charcoal
// filter:target name=chroma in=50 out=100
// 
// Would apply chroma and charcoal filters over frames 50 to 99 inclusive.
//
// <input>
// filter:chroma
// filter:charcoal
// filter:target index=0 in=50 out=100
// 
// Would apply the charcoal filter over frames 50 to 99 inclusive. Chroma is 
// used throughout.
//
// <input>
// filter:chroma
// filter:charcoal
// filter:target index=1 in=50 out=100
// 
// Would apply the chroma and charcoal filters over frames 50 to 99 inclusive.
//
// <input>
// filter:charcoal
// filter:wipe_001 mix=0.5
// filter:target index=1 in=50 out=100 composite=1
//
// Composites a circle charcoal effect on to the input.
//
// <input>
// filter:lerp @@wipe:0=0:1:0:25 @@wipe:1=1:0:-25:-1
// filter:charcoal
// filter:wipe_001 @mix=@@wipe:1
// filter:target index=2 in=50 out=150 composite=1
//
// Surprise?
//
// Rationale:
//
// I thought it was funny.

#include <opencorelib/cl/core.hpp>
#include <opencorelib/cl/enforce_defines.hpp>
#include <openmedialib/ml/openmedialib_plugin.hpp>
#include <openmedialib/ml/stack.hpp>

namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;
namespace ml = olib::openmedialib::ml;
namespace cls = olib::opencorelib::str_util;

namespace olib { namespace openmedialib { namespace ml { namespace wipes {

class ML_PLUGIN_DECLSPEC filter_target : public ml::filter_type
{
	public:
		filter_target( )
		: prop_in_( pl::pcos::key::from_string( "in" ) )
		, prop_out_( pl::pcos::key::from_string( "out" ) )
		, prop_name_( pl::pcos::key::from_string( "name" ) )
		, prop_index_( pl::pcos::key::from_string( "index" ) )
		, prop_composite_( pl::pcos::key::from_string( "composite" ) )
		{
			properties( ).append( prop_in_ = 0 );
			properties( ).append( prop_out_ = -1 );
			properties( ).append( prop_name_ = std::wstring( L"" ) );
			properties( ).append( prop_index_ = 0 );
			properties( ).append( prop_composite_ = 0 );
		}

		virtual std::wstring get_uri( ) const { return L"target"; }

		virtual ml::distributable_status is_distributable( ) { return ml::distributable_should; }

		virtual void on_slot_change( ml::input_type_ptr, int )
		{
			if ( debug_level( ) ) std::cerr << "reseting graphs" << std::endl;
			input_.reset( );
			output_.reset( );
		}

	protected:
		virtual ml::fetch_status do_fetch( ml::frame_type_ptr &result )
		{
			// Initialisation on first use
			if ( !input_ || !output_ ) setup_graphs( );

			// Sanity check
			ARENFORCE_MSG( !!input_ && !!output_, "Internal graphs not available for target" );

			// If we're in our clipping range, use output, otherwise input
			ml::input_type_ptr graph_to_use = in_range( get_position( ) ) ? output_ : input_;

			// Additional debug stuff
			if ( debug_level( ) > 2 ) std::cerr << "target: " << get_position( ) << " " << cls::to_string( graph_to_use->get_uri( ) ) << std::endl;

			// Just use whichever one we have
			return graph_to_use->fetch( result, get_position( ) );
		}

		void setup_graphs( )
		{
			// Obtain the upstream graph
			output_ = fetch_slot( );
			ARENFORCE_MSG( !!output_, "No input connected" );

			// Create a stack with input on it
			ml::stack stack;
			stack.push( output_ );

			// Extract properties here
			const std::wstring name = prop_name_.value< std::wstring >( );
			const int index = prop_index_.value< int >( );
			const int composite = prop_composite_.value< int >( );

			// Deal with name and index
			if ( name != L"" )
			{
				int found = to_int( stack.push( "$" ).push( name ).push( "find" ).pop_release( ) );
				ARENFORCE_MSG( index < found, "Filter '%s' at %d not found - total found %d" )( name )( index )( found );
				input_ = stack.push( boost::format( "%d pick" ) % index ).pop( )->fetch_slot( );
				stack.push( boost::format( "%d pack drop" ) % found );
			}
			else
			{
				input_ = output_;
				for ( int i = 0; !!input_ && i <= index; i ++ )
					input_ = input_->fetch_slot( );
				ARENFORCE_MSG( !!input_, "Unable to find a filter or input with index %d" )( index );
			}

			// Update the interpolators now
			update_interpolaters( );

			// Construct the compositor
			if ( composite )
				output_ = stack.push( input_ ).push( "filter:remove component=audio 1 roll filter:compositor" ).pop_release( );

			// Debug info
			dump( stack, "Input graph:", input_ );
			dump( stack, "Output graph:", output_ );
		}

		void update_interpolaters( )
		{
			int in, out;
			calculate_range( in, out );

			// Walk between the output and the input
			ml::input_type_ptr filter = output_;
			while ( !!filter && filter != input_ )
			{
				pl::pcos::property prop_in = filter->property( "in" );
				pl::pcos::property prop_out = filter->property( "out" );

				if ( prop_in.valid( ) && prop_out.valid( ) )
				{
					prop_in = in;
					prop_out = out;
				}

				filter = filter->fetch_slot( );
			}
		}

		bool in_range( int position )
		{
			int in, out;
			calculate_range( in, out );
			return position >= in && position < out;
		}

		inline void calculate_range( int &in, int &out )
		{
			int frames = get_frames( );
			in = prop_in_.value< int >( );
			out = prop_out_.value< int >( );

			if ( out < 0 )
				out = frames + out + 1;
			if ( in < 0 )
				in = frames + in;

			if ( out < in )
			{
				int t = in;
				in = out;
				out = t;
			}
		}

	private:
		int to_int( const ml::input_type_ptr &input )
		{
			return atoi( cls::to_string( input->get_uri( ) ).c_str( ) );
		}

		void dump( ml::stack &stack, const std::string &message, const ml::input_type_ptr &graph )
		{
			if ( debug_level( ) )
			{
				std::cerr << message << std::endl;
 				stack.push( graph ).push( "filter:aml filename=-" ).pop_release( );
			}
		}

		pl::pcos::property prop_in_;
		pl::pcos::property prop_out_;
		pl::pcos::property prop_name_;
		pl::pcos::property prop_index_;
		pl::pcos::property prop_composite_;
		ml::input_type_ptr input_;
		ml::input_type_ptr output_;
};

ml::filter_type_ptr create_target( const std::wstring & )
{
	return ml::filter_type_ptr( new filter_target( ) );
}

} } } }
