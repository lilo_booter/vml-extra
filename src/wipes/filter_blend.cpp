// Blends the frame on to a background of its own dimensions.
//
// Copyright (C) 2016 Vizrt
// Released under the terms of the LGPL.
//
// #filter:blend
//
// Simplistic filter for blending alpha and applying geometry.

#include <openmedialib/ml/openmedialib_plugin.hpp>

namespace olib { namespace openmedialib { namespace ml { namespace wipes {

struct dimensions
{
	dimensions( int width_, int height_, int sar_num_, int sar_den_, ml::image::field_order_flags field_order_ )
	: width( width_ )
	, height( height_ )
	, sar_num( sar_num_ )
	, sar_den( sar_den_ )
	, field_order( field_order_ )
	{ }

	int width;
	int height;
	int sar_num;
	int sar_den;
	ml::image::field_order_flags field_order;
};

typedef std::map< std::wstring, dimensions > resolution_map;

static const resolution_map map = boost::assign::map_list_of
	( L"1080p", dimensions( 1920, 1080, 1, 1, ml::image::progressive ) )
	( L"1080i", dimensions( 1920, 1080, 1, 1, ml::image::top_field_first ) )
	( L"720p", dimensions( 1280, 720, 1, 1, ml::image::progressive ) )
	( L"576i", dimensions( 720, 576, 64, 45, ml::image::bottom_field_first ) )
	( L"576p", dimensions( 720, 576, 64, 45, ml::image::progressive ) )
	( L"480i", dimensions( 720, 480, 40, 33, ml::image::bottom_field_first ) )
	( L"480p", dimensions( 720, 576, 40, 33, ml::image::progressive ) )
;

class ML_PLUGIN_DECLSPEC filter_blend : public ml::filter_simple
{
	public:
		filter_blend( )
		: prop_spec_( pl::pcos::key::from_string( "spec" ) )
		, prop_pf_( pl::pcos::key::from_string( "pf" ) )
		{
			properties( ).append( prop_spec_ = std::wstring( L"" ) );
			properties( ).append( prop_pf_ = std::wstring( L"yuv420p" ) );
		}

		virtual std::wstring get_uri( ) const { return L"blend"; }
		ml::distributable_status is_distributable( ) { return ml::distributable_should; }

	protected:
		ml::fetch_status do_fetch( ml::frame_type_ptr &result )
		{
			// Attempt to fetch result from the connected slot
			AR_TRY_FETCH( fetch_from_slot( result ) );

			// If there's no image here, then skip processing
			if ( result->has_image( ) )
				result = process( result );

			return ml::fetch_ok;
		}

		ml::frame_type_ptr process( const ml::frame_type_ptr &input )
		{
			ml::frame_type_ptr result;

			// Create our inner graph if required
			if ( !compositor_ )
			{
				background_ = ml::create_input( "colour:" );
				foreground_ = ml::create_input( "pusher:" );
				compositor_ = ml::create_filter( L"compositor" );
				compositor_->connect( background_, 0 );
				compositor_->connect( foreground_, 1 );
			}

			// Extract the current spec
			std::wstring spec = prop_spec_.value< std::wstring >( );

			// Update the background to be the same as spec if required
			if ( spec != L"" )
			{
				resolution_map::const_iterator iter = map.find( spec );
				ARENFORCE_MSG( iter != map.end( ), "Unable to determine a resolution for %s" )( spec );
				background_->property( "width" ) = iter->second.width;
				background_->property( "height" ) = iter->second.height;
				background_->property( "sar_num" ) = iter->second.sar_num;
				background_->property( "sar_den" ) = iter->second.sar_den;
				background_->property( "interlace" ) = int( iter->second.field_order );
			}
			else if ( spec == L"" )
			{
				background_->property( "width" ) = input->width( );
				background_->property( "height" ) = input->height( );
				background_->property( "sar_num" ) = input->get_sar_num( );
				background_->property( "sar_den" ) = input->get_sar_den( );
				background_->property( "interlace" ) = int( input->field_order( ) );
			}

			// We always take the frame rate from the incoming frame and pf from the property here
			background_->property( "colourspace" ) = prop_pf_.value< std::wstring >( );
			background_->property( "fps_num" ) = input->get_fps_num( );
			background_->property( "fps_den" ) = input->get_fps_den( );

			// Push input to foreground
			foreground_->push( input->shallow( ) );

			// Composite on to background
			ARENFORCE_MSG( compositor_->fetch( result, input->get_position( ) ) == ml::fetch_ok, "Unable to fetch blended frame" );

			return result;
		}

	private:
		pl::pcos::property prop_spec_;
		pl::pcos::property prop_pf_;
		ml::input_type_ptr background_;
		ml::input_type_ptr foreground_;
		ml::input_type_ptr compositor_;
		std::wstring used_spec_;
};

ml::filter_type_ptr ML_PLUGIN_DECLSPEC create_blend( const std::wstring &resource )
{
	return ml::filter_type_ptr( new filter_blend( ) );
}

} } } }
