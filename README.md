# VML Extra

This provides a general staging area for testing new VML plugins externally
from the VML and VMF projects.

While it is predominantly cmake oriented, it is compatible with scons builds
and this document will currently focus on the scons use for now.

## Building and Using

The simplest approach is to checkout a clean vml (and optionally, vmf) from
the current mercurial repo in a new directory like:

```
mkdir ~/Source/ardendo/default
cd ~/Source/ardendo/default
hg clone ssh://<user>@repos.ardendo.se//data/hg/repos/Comp/cpp/VML vml
cd vml
prod grab -t linux64 ardome-ml
scons prefix=~/Source/ardendo/default/build target=linux64 -j $( nproc ) install
```

The above will create a traditional unix like top level with bin, lib, share and
other common directories in the build subdirectory.

Then setup your environment:

```
export VML_PREFIX=~/Source/ardendo/default/build
export PATH=$VML_PREFIX/bin:$PATH
export LD_LIBRARY_PATH=$VML_PREFIX/bin:$LD_LIBRARY_PATH
export PYTHONPATH=$VML_PREFIX/lib/python2.7/site-packages
```

This should provide you with the basic aml build - amlbatch and a collection of
plugins should be availble for you in this shell.

Checkout vml-extra in the default directory:

```
cd ~/Source/ardendo/default
git clone git@gitlab.vizrt.com:cya/vml-extra.git
cd vml-extra
./build.sh
./build.sh -j $( nproc ) install
```

And your build will be supplemented with additional plugins and tools.

To obtain documentation, run the following:

```
cd ~/Source/ardendo/default
amldocs vml/src/openmedialib/plugins/ vml-extra/src/ > build/bin/amldocs.aml
amltutor --help
```
