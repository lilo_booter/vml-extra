// ml - A media library representation.

// Copyright (C) 2018 Charles Yates
// Released under the LGPL.

#pragma once

#ifdef WIN32
	#ifdef UTILS_EXPORTS
		#define UTILS_DECLSPEC __declspec( dllexport )
	#else
		#define UTILS_DECLSPEC __declspec( dllimport )
	#endif
#else
	#define UTILS_DECLSPEC
#endif
