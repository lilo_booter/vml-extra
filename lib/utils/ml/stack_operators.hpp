// stack_operators.hpp - provides a << and >> operators for stack access
//
// Copyright (C) 2016 Charles Yates
// Released under the LGPL.
//
// Its purpose is to provide << and >> operators for ml::stack's push and pop
// methods, and encapsulate the stack_convert.hpp.

#pragma once

// Daemon Headers
#include "stack_convert.hpp"

// AML Headers
#include <openmedialib/ml/filter.hpp>
#include <openmedialib/ml/stack.hpp>

namespace olib { namespace openmedialib { namespace ml {

namespace ml = olib::openmedialib::ml;

// ml::stack operators

// The following provides an alternative use of the ml::stack class.
//
// The default class is expected to be used like:
//
// ml::stack stack;
// ml::input_type_ptr input = stack.push( "file.mp4" ).pop( );
// int r1 = convert< int >( stack.push( "1" ).push( "2" ).push( "+" ).pop( ) );
// int r2 = convert< int >( stack.push( "2" ).push( "3" ).push( "*" ).pop( ) );
//
// Using the operators, we can express the same as:
//
// ml::input_type_ptr input;
// int r1, r2;
// STACK( stack )
// << "file.mp4" >> input
// << 1 << 2 << "+" >> r1
// << 2 << 3 << "*" >> r2
// ;
//
// IOW, it extends the use to ints, doubles and other serialisable types, and
// is bidirectional unlike std::cin and std::cout.
//
// There are also some additional templated functions for popping the top
// of the stack as various types.

// Experimental constructor
#define STACK( s ) olib::openmedialib::ml::stack s; s

// ml::stack pop functions

// NB: the stack's pop_release method is used instead of pop. This is just to
// avoid the stack object holding on to a reference to the popped result.

// Pop and convert the top of stack to the requested type
template< typename T >
inline T pop( ml::stack &stack )
{
	return convert< T >( stack.pop_release( ) );
}

// ml::stack pop operators

// Use the stack conversion template to convert the input pop'd to T
template< typename T >
inline ml::stack &operator >>( ml::stack &stack, T &output )
{
	output = pop< T >( stack );
	return stack;
}

// ml::stack push operators

// Attempt to convert to T by way of a lexical cast (on account of the stack not being
// overloaded with a push which accepts things other than strings or aml inputs).
template< typename T >
inline ml::stack &operator <<( ml::stack &stack, const T &value )
{
	return stack.push( boost::lexical_cast< std::string >( value ) );
}

template< >
inline ml::stack &operator <<( ml::stack &stack, const std::string &command )
{
	return stack.push( command );
}

template< >
inline ml::stack &operator <<( ml::stack &stack, const std::wstring &command )
{
	return stack.push( command );
}

template< >
inline ml::stack &operator <<( ml::stack &stack, const ml::input_type_ptr &input )
{
	return stack.push( input );
}

template< >
inline ml::stack &operator <<( ml::stack &stack, const ml::filter_type_ptr &filter )
{
	return stack.push( filter );
}

template< >
inline ml::stack &operator <<( ml::stack &stack, const boost::format &command )
{
	return stack.push( command );
}

template< >
inline ml::stack &operator <<( ml::stack &stack, const boost::wformat &command )
{
	return stack.push( command );
}

// We can also "push" pointers to functions though care should be taken
// with these as they are always immediate. As in, they can't be part of another
// word or stack based condition or loop.
typedef void ( *word_type )( ml::stack & );

inline ml::stack &operator <<( ml::stack &stack, word_type word )
{
	word( stack );
	return stack;
}

} } }
