// reporter.hpp - general mechanism for report csv reports from frame methods and properties
//
// Copyright (C) 2016 Charles Yates
// Released under the LGPL.
//
// This provides a general CSV reporting mechanism for frames and the various
// objects they have assigned to them, such as properties, video packets, audio
// blocks and so on.

#pragma once

// Include the declspec stuff
#include "config.hpp"

// AML Headers
#include <openmedialib/ml/ml.hpp>

namespace olib { namespace openmedialib { namespace ml { namespace reporter {

namespace ml = olib::openmedialib::ml;
namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;

// Type for holding a collection of keys as strings
typedef std::vector< std::string > keys_type;

// Splits a delimited string into a vector
UTILS_DECLSPEC keys_type split( const std::string &value, const std::string &delimiter );

// Type for holding a collection of keys as strings
typedef std::vector< std::wstring > wkeys_type;

// Splits a delimited string into a vector
UTILS_DECLSPEC wkeys_type split( const std::wstring &value, const std::wstring &delimiter );

// Provides a mechanism to access the contents of a properties object on a given
// object by strings.
template < typename T >
inline bool matches( std::ostream &os, const T &object, const std::string &pattern, const std::string &key )
{
	bool result = key.find( pattern ) == 0;
	if ( result && !!object )
	{
		pl::pcos::property property = object->properties( ).get_property_with_string( key.substr( pattern.length( ) ).c_str( ) );
		if ( property.valid( ) ) os << property;
	}
	return result;
}

// Extracts values from frames objects. General frame methods are accessed via
// the lookup map, and frame and video packet properties are accessed via
// "frame." and "packet." properties respectively.
UTILS_DECLSPEC std::ostream &value( std::ostream &os, const ml::frame_type_ptr &frame, const std::string &key );

// Generates a CSV report from the frame using the requested keys.
UTILS_DECLSPEC std::string report( const keys_type &keys, const ml::frame_type_ptr &frame, const std::string &delimiter, const std::string &lf );

} } } }
