// ml::fps_regulate - regulates any combination of frame rates to the request fps

// Copyright (C) 2018 Charles Yates
// Released under the LGPL.

#pragma once

#include <openmedialib/ml/types.hpp>
#include <openmedialib/ml/frame.hpp>
#include <openmedialib/ml/audio.hpp>
#include <openmedialib/ml/stack.hpp>
#include <opencorelib/cl/core.hpp>
#include <opencorelib/cl/enforce_defines.hpp>
#include <openmedialib/ml/utilities.hpp>
#include <openmedialib/ml/audio_reseat.hpp>
#include <string.h>

namespace olib { namespace openmedialib { namespace ml {

class fps_regulate
{
	public:
		typedef std::deque< frame_type_ptr > queue_type;
		typedef boost::rational< int > rational;

		fps_regulate( int num, int den, int debug = 0 )
		: num_( num )
		, den_( den )
		, debug_( debug )
		, position_( 0 )
		, reseat_( audio::create_reseat( ) )
		, offset_( 0 )
		, needs_audio_( true )
		, frequency_( 0 )
		, channels_( 0 )
		, id_( ml::audio::pcm16_id )
		, needs_video_( true )
		{
		}

		void set_shape( const image::geometry &shape )
		{
			shape_ = shape;
			needs_video_ = shape.width * shape.height > 0;
		}

		void set_audio( const int &frequency, const int &channels, const ml::audio::identity &id )
		{
			frequency_ = frequency;
			channels_ = channels;
			id_ = id;
			needs_audio_ = frequency * channels > 0;
		}

		// Before push_audio or push_image is invoked, the frame must be prepared
		ml::frame_type_ptr prepare( const ml::frame_type_ptr &frame )
		{
			// A null frame will be ignored here
			if ( frame == 0 ) return ml::frame_type_ptr( );

			// Keep a note of the number of frames prepared
			frames_in ++ ;

			// Validate the frame
			validate( frame );

			// Fill in any missing information
			ml::frame_type_ptr result = std::move( modify( frame ) );

			// Keep a note of the last input frame
			last_input_frame_ = frame->shallow( );

			return result;
		}

		void push_audio( frame_type_ptr &frame )
		{
			// Sanity check
			ARENFORCE_MSG( needs_audio_ && frame->has_audio( ), "Missing audio on frame - prepare must be called before push methods" );

			// There is a necessary one frame delay here, so deal with with previous
			// audio packet first
			if ( needs_audio_ && staging_ )
			{
				reseat_->append( staging_ );
				samples_in += staging_->samples( );
				staging_.reset( );
			}

			// Stage the audio for this frame now
			if ( needs_audio_ && frame->has_audio( ) )
				staging_ = frame->get_audio( );

			// We can now remove the audio from the frame
			frame->set_audio( ml::audio_type_ptr( ) );
		}

		void push_video( const frame_type_ptr &frame )
		{
			// Image regulation is provided by tracking 3 rationals:
			//
			// - the fixed target framerate
			// - the variable input framerate
			// - the class member offset which defaults to 0
			//
			// For each input frame, we need to display its associated image 0 or more times
			// to ensure that we output at the target rate.
			//
			// An image is displayed if the current offset is less than target. If displayed,
			// offset is increased by the input fps, and the process is repeated while the
			// resultant offset is less than target. The offset is finally reduced by the
			// target fps in preparation for the next frame.
			//
			// The following shows the state associated to a specified target (t), input (i)
			// and offset:
			//
			// INPUT:            DESCRIPTION:                 OUTPUT:
			// t=60,i=60,o=0     input is displayed once      o=0
			// t=60,i=180,o=0    input is displayed once      o=120
			// t=60,i=180,o=120  input is dropped             o=60
			// t=60,i=180,o=60   input is dropped             o=0
			// t=60,i=30,o=0     input is displayed twice     o=0
			// t=60,i=1,o=0      input is displayed 60 times  o=0
			//
			// Note that the frames which should repeat the previously displayed image are
			// actually output with null images to allow for downstream distributed rescaling.
			// See smart_pusher: and filter:smart_repair for more details.

			const rational target( num_, den_ );
			const rational input( frame->get_fps_num( ), frame->get_fps_den( ) );

			// We will only output unique images here
			bool shown = is_identical( last_shown_frame_, frame );

			// Cue images
			while( offset_ < target )
			{
				// Create our output frame
				ml::frame_type_ptr output = frame->shallow( );

				// If frame is a repeat, drop the image to avoid a downstream rescale
				if ( shown )
					output->set_image( ml::image_type_ptr( ) );
				else
					shown = true;

				// Push the repeated frame
				queue_.push_back( std::move( output ) );

				// Increment the offset by the input fps
				offset_ += input;

				// Increment the number of frames produced
				frames_out ++;
			}

			// Decrement the offset by the target fps
			offset_ -= target;

			// Just in case we need to reuse the last submitted image
			if ( shown && frame->get_image( ) )
				last_shown_frame_ = frame->shallow( );
		}

		// Indicates if the presented frame matches the type of the frame in the input queue
		bool is_matching( const ml::frame_type_ptr &f0, const ml::frame_type_ptr &f1 ) const
		{
			bool result = f0 != 0 && f1 != 0;
			if ( result )
			{
				result =
					f0->width( ) == f1->width( ) &&
					f0->height( ) == f1->height( ) &&
					f0->pf( ) == f1->pf( ) &&
					f0->field_order( ) == f1->field_order( );
			}
			return result;
		}

		// Attempts to determine if the presented frame is a repeat of the frame in the input queue
		bool is_identical( const ml::frame_type_ptr &f0, const ml::frame_type_ptr &f1 ) const
		{
			bool result = is_matching( f0, f1 );
			if ( result )
			{
				result = f0->get_position( ) == f1->get_position( );

				// We will have to force a decode to double check this .. unless we have a packet
				if ( result && f0->get_video_packet( ) && f1->get_video_packet( ) )
					result = f0->get_video_packet( )->bytes( ) == f1->get_video_packet( )->bytes( );
				else if ( result )
					result = f0->get_image( )->ptr( 0 ) == f1->get_image( )->ptr( 0 );
			}
			return result;
		}

		bool is_continuous( const ml::frame_type_ptr &f0, const ml::frame_type_ptr &f1 ) const
		{
			// Basic sanity check
			ARENFORCE_MSG( f1 != 0, "Invalid call to to is_continuous" );

			// If this is the first frame (ie: f0 is null), then return true
			if ( f0 == 0 ) return true;

			// If the difference in position is exactly 1 or -1, we're continuous
			bool result = std::abs( f0->get_position( ) - f1->get_position( ) ) == 1;

			// The frame rates also have to match (so pitch speed changes aren't continuous)
			result = result && ( f0->fps( ) == f1->fps( ) );

			// If there are video packets involved, both should have them or not have them
			result = result && ( ( f0->get_video_packet( ) != 0 ) == ( f1->get_video_packet( ) != 0 ) );

			// If there are video packets, they should be consecutive
			if ( result && f0->get_video_packet( ) != 0 )
				result = std::abs( f0->get_video_packet( )->position( ) - f1->get_video_packet( )->position( ) ) == 1;

			return result;
		}

		frame_type_ptr pop( bool force = false )
		{
			frame_type_ptr result;

			// Calculate the number of samples for the current frame position
			const int samples = needs_audio_ ? audio::samples_for_frame( position_, frequency_, num_, den_ ) : 0;

			// Ensure the last staged audio packet is cued when force is specified
			if ( force && staging_ )
			{
				reseat_->append( staging_ );
				staging_.reset( );
			}

			// We will only obtain a frame if we're forced to do so, or we have enough samples
			if ( queue_.size( ) && ( force || reseat_->has( samples ) ) )
			{
				// Retrieve the specified number of samples or pad if forced
				audio_type_ptr audio;
				if ( needs_audio_ )
				{
					audio = reseat_->retrieve( samples, force );
					if ( audio != 0 ) audio->set_position( position_ );
					samples_out += samples;
				}

				// Obtain the next frame
				result = boost::move( queue_.front( ) );
				queue_.pop_front( );

				// Update the frame with the new audio, position and frame rate
				result->set_audio( audio );
				result->set_position( position_ );
				result->set_fps( num_, den_ );

				// Prepare for next frame request
				position_ ++;
			}

			// Clear the cache and reseater if we're in forced mode
			if ( force && queue_.size( ) == 0 )
			{
				reseat_->clear( );
				queue_.clear( );
				offset_ = 0;
			}

			if ( ( debug_ & 1 ) && needs_audio_ )
				std::cerr
				<< "frames in/pushed/out: " << frames_in << "/" << frames_out << "/" << position_
				<< " pending: " << ( frames_out - position_ )
				<< " samples in/wanted/out: " << samples_in << "/" << samples_wanted << "/" << samples_out
				<< " discrepancy: " << ( samples_in + ( staging_ != 0 ? staging_->samples( ) : 0 ) - samples_wanted ) << "/" << ( samples_in - samples_out )
				<< std::endl;

			return result;
		}

		bool has_frame( )
		{
			const int samples = needs_audio_ ? audio::samples_for_frame( position_, frequency_, num_, den_ ) : 0;
			return queue_.size( ) && reseat_->has( samples );
		}

		size_t pending( ) const
		{
			return queue_.size( );
		}

	protected:
		void validate( const ml::frame_type_ptr &frame )
		{
			ARENFORCE_MSG( !!frame, "Null frame passed to fps regulator - abandoning" );

			if ( needs_audio_ && ( frequency_ == 0 || channels_ == 0 ) )
			{
				if ( frame->has_audio( ) )
				{
					frequency_ = frame->frequency( );
					channels_ = frame->channels( );
					id_ = frame->get_audio( )->id( );
				}
				else
				{
					frequency_ = 48000;
					channels_ = 2;
				}
			}
			else if ( needs_audio_ && frame->has_audio( ) )
			{
				ARENFORCE_MSG( frequency_ == frame->frequency( ), "Audio frequency changed from %d to %d" )( frequency_ )( frame->frequency( ) );
			}
		}

		ml::frame_type_ptr modify( const ml::frame_type_ptr &frame )
		{
			// Make a safe copy so we can modify it
			ml::frame_type_ptr result = frame->shallow( );

			// We hold back audio by one frame
			const int samples_staged = staging_ != 0 ? staging_->samples( ) : 0;

			// This is the discrepancy between the number of samples we've received and the number we've wanted
			const int discrepancy = static_cast< int >( samples_in + samples_staged - samples_wanted );

			// Determine if there is a discontinuity between this frame and last
			//const bool continuous = is_continuous( last_input_frame_, frame );

			// If not contiguous and discrepancy exists, samples can be removed from the end of staging

			// Calculate the number of samples for the current frame position
			const int samples_for_frame = audio::samples_for_frame( frames_in, frequency_, result->get_fps_num( ), result->get_fps_den( ) );

			// If we are to generate an audio type (for pause or missing), and we've already had more than we wanted
			// the number of samples we need to create can be fewer than the samples_for_frame
			const int samples = std::max< int >( samples_for_frame - discrepancy, 2 );

			// This is the total number of samples that we should ideally have received before the next frame is pushed
			samples_wanted += samples_for_frame;

			// Turn off audio scrubbing noise
			if ( needs_audio_ && samples_staged != 0 && last_input_frame_ != 0 && std::abs( frame->get_position( ) - last_input_frame_->get_position( ) ) != 1 )
				staging_ = ml::audio::allocate( id_, frequency_, channels_, samples_staged, true );

			// Repeat last input image if available and no other image exists
			if ( needs_video_ && !frame->has_image( ) && last_input_frame_ && last_input_frame_->has_image( ) )
				result->set_image( last_input_frame_->get_image( )->shallow( ) );

			// New silent audio if last frame is repeated
			if ( needs_audio_ && is_identical( frame, last_input_frame_ ) )
				result->set_audio( ml::audio::allocate( id_, frequency_, channels_, samples, true ) );

			// Generate audio if missing
			if ( needs_audio_ && !frame->has_audio( ) )
				result->set_audio( ml::audio::allocate( id_, frequency_, channels_, samples, true ) );

			// Visualise if no image exists
			if ( needs_video_ && !frame->has_image( ) )
				result = visualise( result );

			// Rearrange audio channels if necessary
			if ( needs_audio_ && result->has_audio( ) && result->channels( ) != channels_ )
				result->set_audio( channel_convert( result->get_audio( ), channels_ ) );

			// Convert to the same audio type
			if ( needs_audio_ && result->has_audio( ) && result->get_audio( )->id( ) != id_ )
				result->set_audio( ml::audio::coerce( id_, result->get_audio( ) ) );

			// FIXME: We cannot convert timecodes correctly here since it's possible
			// that a pitch filter has already changed the fps on the frame from the
			// original so all we can do at this point is remove the info to avoid
			// errors occuring in a downstream muxer. This does not effect the stats
			// filter's timecode since that is there to reflect the input anyway.
			for ( auto name : { "source_timecode", "material_start_timecode", "source_start_timecode", "source_timecode_dropframe", "audio_reversed" } )
				if ( result->property( name ).valid( ) )
					result->properties( ).remove( result->property( name ) );

			return result;
		}

		ml::frame_type_ptr visualise( ml::frame_type_ptr &frame )
		{
			if ( !pusher_ )
			{
				visualise_ = ml::stack( )
				.push( "pusher:" )
				.push( filter_visualise( ) )
				.pop( )
				;

				pusher_ = visualise_->fetch_slot( );
			}

			int position = frame->get_position( );
			pusher_->push( std::move( frame ) );
			ARENFORCE_MSG( visualise_->fetch( frame, position ) == ml::fetch_ok, "Unable to visualise" );

			return boost::move( frame );
		}

		boost::format filter_visualise( )
		{
			return boost::format( "filter:visualise width=%d height=%d sar_num=%d sar_den=%d colourspace=%s type=1" )
				   % shape_.width % shape_.height % shape_.sar_num % shape_.sar_den % olib::opencorelib::str_util::to_string( ml::image::MLPF_to_string( shape_.pf ) );
		}

	private:
		const int num_;
		const int den_;
		const int debug_;

		// Current state
		int position_;
		ml::audio_type_ptr staging_;
		audio::reseat_ptr reseat_;
		queue_type queue_;
		rational offset_;
		int last_frame_;

		// We track last input and shown frames
		ml::frame_type_ptr last_input_frame_;
		ml::frame_type_ptr last_shown_frame_;

		// Audio normalisation settings
		bool needs_audio_;
		int frequency_;
		int channels_;
		ml::audio::identity id_;

		// Video normalisation settings
		bool needs_video_;
		image::geometry shape_;

		// Visualisation subgraph
		ml::input_type_ptr pusher_;
		ml::input_type_ptr visualise_;

		// Audio related stats
		std::int64_t samples_in = 0;
		std::int64_t samples_wanted = 0;
		std::int64_t samples_out = 0;
		int frames_in = 0;
		int frames_out = 0;
};

} } }

