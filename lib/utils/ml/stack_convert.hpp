// stack_convert - General functionality for converting popped results from an aml stack
//
// Copyright (C) 2016 Charles Yates
// Released under the LGPL.
//
// Its purpose is to recover values (such as strings, ints and doubles) which
// are placed on the stack and become wrapped in input_type_ptr objects.

#pragma once

#include "config.hpp"

// AML Headers
#include <openmedialib/ml/input.hpp>
#include <opencorelib/cl/str_util.hpp>

// Boost headers
#include <boost/lexical_cast.hpp>

namespace olib { namespace openmedialib { namespace ml {

namespace ml = olib::openmedialib::ml;
namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;

// General conversions from std::wstring
template< typename T >
inline T convert( const std::wstring &value )
{
	return boost::lexical_cast< T >( value );
}

template< >
inline std::string convert( const std::wstring &value )
{
	return cl::str_util::to_string( value );
}

template< >
inline std::wstring convert( const std::wstring &value )
{
	return value;
}

// Conversion mechanism for primitive input_type_ptr's (created by aml_stack:)

// aml_stack's conversion of type to input_type_ptr involves creating a minimalist
// input_type and assigning to a property of this name.
extern UTILS_DECLSPEC pl::pcos::key key_aml_value_;

// Obtains or converts the type of the value of this input
// -> When the .aml_value is set and it's type matches T, use that
// -> otherwise, convert the input's uri to the requested type when possible
template< typename T >
inline T convert( const ml::input_type_ptr &input )
{
	T result;
	pl::pcos::property prop = input->property_with_key( key_aml_value_ );
	if ( prop.valid( ) && prop.is_a< T >( ) )
		result = prop.value< T >( );
	else
		result = convert< T >( input->get_uri( ) );
	return result;
}

template< >
inline ml::input_type_ptr convert( const ml::input_type_ptr &input )
{
	return input;
}

} } }
