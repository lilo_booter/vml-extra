// tool_utilities.hpp - small collection of common functionality for aml tools
//
// Copyright (C) 2016 Charles Yates
// Released under the LGPL.
//
// This file provides some general purpose functionality for identifying command
// line arguments and setting up aml_stack wrappers.

#pragma once

// Include the declspec stuff
#include "config.hpp"

// AML Headers
#include <openmedialib/ml/ml.hpp>
#include <openpluginlib/pl/pcos/observer.hpp>

// Standard c++ headers
#include <set>

namespace olib { namespace openmedialib { namespace ml {

namespace ml = olib::openmedialib::ml;
namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;

// AML stack parsing identifiers

// The functions here are an attempt to formalise the command line and stack
// parsing

UTILS_DECLSPEC bool is_property( const std::string &token );

// Determines if the token is a possible input
UTILS_DECLSPEC bool is_input( const std::string &token );

// Determines if the token is a possible store
UTILS_DECLSPEC bool is_store( const std::string &token );

// Property assignment from strings
UTILS_DECLSPEC bool assign( pl::pcos::property_container &props, const std::string &name, const std::wstring &value );

// Assign a property from a name=value string
UTILS_DECLSPEC bool assign( pl::pcos::property_container &props, const std::string &token );

// Interpret token - should work for inputs, filters, stores, frames and packets.

// This method attempts to assign the property to the properties carrying object
template< typename T >
bool assign( const T &object, const std::string &token )
{
	// Verify that we have an object and the token represents a propertty first
	bool result = !!object && is_property( token );

	if ( result )
	{
		pl::pcos::property_container props = object->properties( );
		result = assign( props, token );
	}

	return result;
}

// Attempt to create a store from the token and sample frame given.
UTILS_DECLSPEC bool create( ml::store_type_ptr &object, const std::string &token, const ml::frame_type_ptr &sample );

// Property observer (could be mem_fn?)
template < typename C >
class fn_observer : public pl::pcos::observer
{
	public:
		fn_observer( C *instance, void ( C::*fn )( ) )
		: instance_( instance )
		, fn_( fn )
		{ }

		virtual void updated( pl::pcos::isubject * )
		{ ( instance_->*fn_ )( ); }

	private:
		C *instance_;
		void ( C::*fn_ )( );
};

typedef std::set< ml::input_type_ptr > inputs_type;

// Converts a graph to the set of individual input/filter objects it contains.
UTILS_DECLSPEC void flatten( inputs_type &inputs, const ml::input_type_ptr &haystack );

// Checks if there's an overlap between the set of inputs and the inputs contained
// in the specified graph.
UTILS_DECLSPEC bool intersects( const inputs_type &inputs, const ml::input_type_ptr &needle );

// This function checks if the two graphs have any input objects in common. The reason
// being that if they do, then any use of the "needles" graph must not be concurrent with
// use of "haystack".
UTILS_DECLSPEC bool contains( const ml::input_type_ptr &haystack, const ml::input_type_ptr &needles );

} } }
