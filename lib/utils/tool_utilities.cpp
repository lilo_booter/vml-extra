// tool_utilities.hpp - small collection of common functionality for aml tools
//
// Copyright (C) 2016 Charles Yates
// Released under the LGPL.
//
// This file provides some general purpose functionality for identifying command
// line arguments and setting up aml_stack wrappers.

#include "ml/tool_utilities.hpp"

namespace olib { namespace openmedialib { namespace ml {

namespace ml = olib::openmedialib::ml;
namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;

// AML stack parsing identifiers

// The functions here are an attempt to formalise the command line and stack
// parsing

// A property is specified when the token contains =, doesn't have a preceding :, or
// is prefixed by an @.
//
// IOW, it's expected that properties specified here do not contain :'s unless they're
// prefixed by an @.
//
// eg: avformat:http://server?name=value is not a property
//     name=http://server?name=value is a property.
//     @name:0=value is a property

UTILS_DECLSPEC bool is_property( const std::string &token )
{
	const auto pos_equal = token.find( "=" );
	const auto pos_colon = token.find( ":" );
	const auto pos_at = token.find( "@" );
	return pos_equal != std::string::npos && ( pos_at == 0 || pos_colon == std::string::npos || pos_colon > pos_equal );
}

// Determines if the token is a possible input
UTILS_DECLSPEC bool is_input( const std::string &token )
{
	return token != "" && !is_property( token );
}

// Determines if the token is a possible store
UTILS_DECLSPEC bool is_store( const std::string &token )
{
	return token != "" && !is_property( token );
}

// Property assignment from strings
UTILS_DECLSPEC bool assign( pl::pcos::property_container &props, const std::string &name, const std::wstring &value )
{
	bool result = true;

	// Obtain the property
	pl::pcos::property property = props.get_property_with_string( name.c_str( ) );

	// Assign, create or fail
	if ( property.valid( ) )
		property.set_from_string( value );
	else if ( name.find( "@" ) == 0 )
		props.append( pl::pcos::property( pl::pcos::key::from_string( name.c_str( ) ) ) = value );
	else if ( name.find( "?" ) != 0 )
		result = false;

	return result;
}

// Assign a property from a name=value string
UTILS_DECLSPEC bool assign( pl::pcos::property_container &props, const std::string &token )
{
	// Verify that we have an object and the token represents a propertty first
	bool result = is_property( token );

	if ( result )
	{
		// Split the token
		size_t pos_equal = token.find( "=" );
		std::string name = token.substr( 0, pos_equal );
		std::wstring value = cl::str_util::to_wstring( token.substr( pos_equal + 1 ) );

		// Assign the property
		result = assign( props, name, value );
	}

	return result;
}

// Attempt to create a store from the token and sample frame given.
UTILS_DECLSPEC bool create( ml::store_type_ptr &object, const std::string &token, const ml::frame_type_ptr &sample )
{
	bool result = is_store( token );

	if ( result )
	{
		object = ml::create_store( token, sample );
		result = !!object;
	}

	return result;
}

// Converts a graph to the set of individual input/filter objects it contains.
UTILS_DECLSPEC void flatten( inputs_type &inputs, const ml::input_type_ptr &haystack )
{
	if ( haystack && inputs.find( haystack ) == inputs.end( ) )
	{
		inputs.insert( haystack );
		for( size_t i = 0; i < haystack->slot_count( ); i ++ )
			flatten( inputs, haystack->fetch_slot( i ) );
	}
}

// Checks if there's an overlap between the set of inputs and the inputs contained
// in the specified graph.
UTILS_DECLSPEC bool intersects( const inputs_type &inputs, const ml::input_type_ptr &needle )
{
	bool result = inputs.find( needle ) != inputs.end( );
	for( size_t i = 0; !result && i < needle->slot_count( ); i ++ )
		result = intersects( inputs, needle->fetch_slot( i ) );
	return result;
}

// This function checks if the two graphs have any input objects in common. The reason
// being that if they do, then any use of the "needles" graph must not be concurrent with
// use of "haystack".
UTILS_DECLSPEC bool contains( const ml::input_type_ptr &haystack, const ml::input_type_ptr &needles )
{
	inputs_type inputs;
	flatten( inputs, haystack );
	return intersects( inputs, needles );
}

} } }
