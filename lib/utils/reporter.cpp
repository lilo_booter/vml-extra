// reporter.cpp - general mechanism for report csv reports from frame methods and properties
//
// Copyright (C) 2016 Charles Yates
// Released under the LGPL.
//
// This provides a general CSV reporting mechanism for frames and the various
// objects they have assigned to them, such as properties, video packets, audio
// blocks and so on.

// AML Headers
#include "ml/reporter.hpp"

namespace olib { namespace openmedialib { namespace ml { namespace reporter {

namespace ml = olib::openmedialib::ml;
namespace cl = olib::opencorelib;
namespace pl = olib::openpluginlib;

// Splits a delimited string into a vector
UTILS_DECLSPEC keys_type split( const std::string &value, const std::string &split_by )
{
	keys_type result;
	size_t last = 0;
	size_t next = 0;
	const size_t total = value.length( );
	const std::string delimiter = split_by != "\\n" ? split_by : "\r\n";

	while ( last != std::string::npos )
	{
		if ( delimiter != "" )
			next = value.find( delimiter, last );
		else
			next = last < total - 1 ? last + 1 : std::string::npos;
		result.push_back( value.substr( last, next - last ) );
		if ( next == std::string::npos ) break;
		last = next + delimiter.length( );
	}

	return result;
}

UTILS_DECLSPEC wkeys_type split( const std::wstring &value, const std::wstring &split_by )
{
	wkeys_type result;
	size_t last = 0;
	size_t next = 0;
	const size_t total = value.length( );
	const std::wstring delimiter = split_by != L"\\n" ? split_by : L"\r\n";

	while ( last != std::wstring::npos )
	{
		if ( !delimiter.empty( ) )
			next = value.find( delimiter, last );
		else
			next = last < total - 1 ? last + 1 : std::wstring::npos;
		result.push_back( value.substr( last, next - last ) );
		if ( next == std::wstring::npos ) break;
		last = next + delimiter.length( );
	}

	return result;
}

// Defines a lookup table for evaluating frame values
typedef void( *frame_lookup_type )( std::ostream &os, const ml::frame_type_ptr & );
typedef std::map< std::string, frame_lookup_type > values_type;

// Temporary work around for problems with the std c++11 library on the mac
#ifdef OLIB_ON_MAC

static void bug_fix( std::ostream &os, const ml::frame_type_ptr & )
{
	os << "this is necessary to work around a problem with map_list_of and lambdas";
}

#define CREATE_LOOKUP( name ) const values_type name = boost::assign::map_list_of ( "bug_fix", bug_fix )
#define ADD_LOOKUP( NAME, VALUE ) ( NAME, [ ]( std::ostream &os, const ml::frame_type_ptr &frame ) { os << VALUE; } )
#define END_LOOKUP
#else
#define CREATE_LOOKUP( name ) const values_type name = {
#define ADD_LOOKUP( NAME, VALUE ) { NAME, [ ]( std::ostream &os, const ml::frame_type_ptr &frame ) { os << VALUE; } },
#define END_LOOKUP }
#endif

// Provides a mechanism to access the methods on the frame by strings
CREATE_LOOKUP( lookup )
	ADD_LOOKUP( "position", frame->get_position( ) )
	ADD_LOOKUP( "fps_num", frame->get_fps_num( ) )
	ADD_LOOKUP( "fps_den", frame->get_fps_den( ) )
	ADD_LOOKUP( "fps", frame->get_fps_num( ) << ":" << frame->get_fps_den( ) )
	ADD_LOOKUP( "width", frame->width( ) )
	ADD_LOOKUP( "height", frame->height( ) )
	ADD_LOOKUP( "has_image", frame->has_image( ) )
	ADD_LOOKUP( "has_alpha", frame->has_alpha( ) )
	ADD_LOOKUP( "has_audio", frame->has_audio( ) )
	ADD_LOOKUP( "has_audio_block", !!frame->audio_block( ) )
	ADD_LOOKUP( "has_video_packet", !!frame->get_video_packet( ) )
	ADD_LOOKUP( "sar_num", frame->get_sar_num( ) )
	ADD_LOOKUP( "sar_den", frame->get_sar_den( ) )
	ADD_LOOKUP( "sar", frame->get_sar_num( ) << ":" << frame->get_sar_den( ) )
	ADD_LOOKUP( "pf", cl::str_util::to_string( frame->pf( ) ) )
	ADD_LOOKUP( "frequency", frame->frequency( ) )
	ADD_LOOKUP( "channels", frame->channels( ) )
	ADD_LOOKUP( "samples", frame->samples( ) )
END_LOOKUP;

// Extracts values from frames objects. General frame methods are accessed via
// the lookup map, and frame and video packet properties are accessed via
// "frame." and "packet." properties respectively.
UTILS_DECLSPEC std::ostream &value( std::ostream &os, const ml::frame_type_ptr &frame, const std::string &key )
{
	auto iter = lookup.find( key );
	if ( iter != lookup.end( ) ) iter->second( os, frame );
	else if ( matches( os, frame, "frame.", key ) ) { }
	else if ( matches( os, frame->get_video_packet( ), "packet.", key ) ) { }
	return os;
}

// Generates a CSV report from the frame using the requested keys.
UTILS_DECLSPEC std::string report( const keys_type &keys, const ml::frame_type_ptr &frame, const std::string &delimiter, const std::string &lf )
{
	std::stringstream os;
	auto last = -- keys.end( );
	for ( auto iter = keys.begin( ); iter != keys.end( ); iter ++ )
		value( os, frame, *iter ) << ( iter != last ? delimiter : lf );
	return os.str( );
}

} } } }
