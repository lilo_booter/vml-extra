
#include "ml/config.hpp"
#include <openpluginlib/pl/pcos/key.hpp>

namespace olib { namespace openmedialib { namespace ml {

namespace pl = olib::openpluginlib;

// Key for aml_stack stuff
UTILS_DECLSPEC pl::pcos::key key_aml_value_ = pl::pcos::key::from_string( ".aml_value" );

} } }

