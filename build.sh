#!/usr/bin/env bash

die() {
	echo >&2 "ERROR: $@"
	exit 1
}

# Make sure we only run this in the directory containing the script
bash_source=$( readlink -f $BASH_SOURCE )
source_directory=$( dirname "$bash_source" )
cd $( readlink -f "$source_directory" ) || die "Unable to change to $source_directory"

# Create and change to build directory if necessary
mkdir -p release || die "Unable to create release directory"
cd release || die "Unable to go to the release directory"

# Set up the rules
declare cmd=""
declare install=""

if [[ -f "Makefile" ]] ; then
	cmd="make -j $( nproc )"
	install="install"
elif [[ -f "CMakeCache.txt" ]] ; then
	cmd="cmake --build ."
	install="--target INSTALL"
fi

declare execute=""
if [[ "$#" == 1 && "$cmd" != "" ]] ; then
	case "$1" in
	clean ) execute=$1 ; shift ;;
	install ) cmd="$cmd $install" ; shift ;;
	esac
fi

if [[ "$execute" == "clean" ]] ; then
	rm -rf *
elif [[ "$cmd" != "" ]] ; then
	$cmd "$@"
else
	[[ -d "$VMB_PREFIX" ]] || die "$VMB_PREFIX not found"
	if [[ "$VMB_GENERATOR" != "" ]] ; then
		VMB_PREFIX=$( cygpath -m "$VMB_PREFIX" )
		cmake -G "$VMB_GENERATOR" -DCMAKE_INSTALL_PREFIX="$VMB_PREFIX" -DIMPORT_PATTERN="$VMB_PREFIX/lib/cmake" -DCMAKE_MODULE_PATH="$VMB_PREFIX/share/cmake/Modules;$VMB_PREFIX/lib/cmake" ..
	else
		cmake -DCMAKE_INSTALL_PREFIX="$VMB_PREFIX" -DIMPORT_PATTERN="$VMB_PREFIX/lib/cmake" -DCMAKE_MODULE_PATH="$VMB_PREFIX/lib/cmake;$VMB_PREFIX/share/cmake;$VMB_PREFIX/share/cmake/Modules" -DCMAKE_CXX_FLAGS="-std=c++11 -fext-numeric-literals -O2" -DCMAKE_CXX_STANDARD=11 ..
	fi
fi

